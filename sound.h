//**************************************************
//
// sound.h
// Author  : katsuki mizuki
//
//**************************************************
#ifndef _SOUND_H_	//このマクロ定義がされてなかったら
#define _SOUND_H_	//２重インクルード防止のマクロ定義

//==================================================
// インクルード
//==================================================
#include <d3dx9.h>
#include <xaudio2.h>

//==================================================
// 定義
//==================================================
class CSound
{
	/* ↓定義↓ */
public:
	enum ELabel
	{
		LABEL_NONE = -1,
		LABEL_BGM_TITLE,		// タイトル
		LABEL_BGM_CHARSELECT,	// キャラ選択
		LABEL_BGM_GAME1,		// ゲーム1
		LABEL_BGM_GAME2,		// ゲーム2
		LABEL_BGM_GAME3,		// ゲーム3
		LABEL_BGM_GAME4,		// ゲーム4
		LABEL_BGM_GAME5,		// ゲーム5
		LABEL_BGM_GAME6,		// ゲーム6
		LABEL_BGM_RANKING,		// ランキング
		LABEL_BGM_TUTORIAL,		// チュートリアル
		LABEL_BGM_RESULT,		// リザルト
		LABEL_BGM_MAP,			// マップセレクト
		LABEL_SE_ENTER,			// 決定音
		LABEL_SE_SELECT,		// 選択
		LABEL_SE_CURVE,			// 曲がるとき
		LABEL_SE_GET_ITEM,		// アイテム取得
		LABEL_SE_ITEM,			// アイテム
		LABEL_SE_SPLASH,		// 死んだとき爆発
		LABEL_SE_REX,			// お邪魔恐竜
		LABEL_SE_BROKEN,		// 壊れるブロック
		LABEL_SE_DASH,			// 走る音
		LABEL_SE_DAMAGE1,		// ダメージ1
		LABEL_SE_DAMAGE2,		// ダメージ2
		LABEL_SE_BULLET,		// 弾発射
		LABEL_SE_START,			// スタート
		LABEL_SE_FINISH,		// フィニッシュ
		LABEL_SE_STAN,			// スタン
		LABEL_SE_GAYA,			// ガヤ
		LABEL_SE_JUMP,			// ジャンプ
		LABEL_SE_LANDING,		// 着地
		LABEL_SE_PUNCH,			// パンチ
		LABEL_SE_ENEMYDAMAGE,	// 敵からのダメ―ジ
		LABEL_SE_ENTER2,		// エンター
		LABEL_SE_WARNING,		// 敵警告
		LABEL_MAX,
	};

	struct SParam
	{
		char *pFileName;	// ファイル名
		int loop;			// ループ
	};

	static const SParam PARAM[];	// パラメータの情報

	/* ↓メンバ関数↓ */
public:
	CSound();	// デフォルトコンストラクタ
	~CSound();	// デストラクタ

public:
	HRESULT Init(HWND hWnd);
	void Uninit();
	HRESULT Play(ELabel sound);
	void Stop(ELabel label);
	void Stop();

private:
	HRESULT CheckChunk(HANDLE hFile, DWORD format, DWORD* pChunkSize, DWORD* pChunkDataPosition);	// チャンクのチェック
	HRESULT LoadChunkData(HANDLE hFile, void* pBuffer, DWORD dwBuffersize, DWORD dwBufferoffset);	// チャンクデータの読み込み

	/* ↓メンバ変数↓ */
private: 
	IXAudio2* m_pXAudio2;							// XAudio2オブジェクトへのインターフェイス
	IXAudio2MasteringVoice* m_pMasteringVoice;		// マスターボイス
	IXAudio2SourceVoice* m_pSourceVoice[LABEL_MAX];	// ソースボイス
	BYTE* m_pDataAudio[LABEL_MAX];					// オーディオデータ
	DWORD m_sizeAudio[LABEL_MAX];					// オーディオデータサイズ
};

#endif // !_SOUND_H_
