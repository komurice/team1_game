//==================================================
// player_select.h
// Author: Buriya Kota
//==================================================
#ifndef _PLAYER_SELECT_H_
#define _PLAYER_SELECT_H_

//**************************************************
// インクルード
//**************************************************
#include "game_mode.h"

//**************************************************
// 前方宣言
//**************************************************
class CObject2D;
class CObject3D;
class CPlayer;
//**************************************************
// クラス
//**************************************************
class CPlayerSelect : public CGameMode
{
public:

	enum FRUIT
	{
		FRUIT_APPLE = 0,
		FRUIT_GRAPE,
		FRUIT_ORANGE,
		FRUIT_STRAWBERRY,
		FRUIT_CHERRY,
		FRUIT_MAX
	};




	struct List
	{
		bool IsApple;
		bool IsGrape;
		bool IsOrange;
		bool IsStrawberry;
		bool IsCherry;
	};


	static const int PlayerMax = 4;

	static const int SleepTime = 5;
	CPlayerSelect();
	~CPlayerSelect() override;

	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw(DRAW_MODE /*drawMode*/) override {}

	static CPlayerSelect *Create();

	// プレイヤーの情報の取得
	static CPlayerSelect* GetPlayerSelect() { return m_pPlayerSelect; }

	void SetList();
	int SelectSort(const int Isnumber, const bool isUp);

private:
	void CharacterSelect();
	void NumberSelect();
	// 自身のポインタ
	static CPlayerSelect* m_pPlayerSelect;

	CPlayer *m_Player[PlayerMax];
	CObject2D *BG;
	CObject2D *m_Stage;
	CObject2D *m_PlaySelectUi[PlayerMax];
	CObject2D *m_CharacterSelectUi[PlayerMax];
	CObject2D *m_ResultUi[PlayerMax];
	CObject3D *m_CharacterSelectBg[PlayerMax];
	bool m_IsPlayerselect;
	bool m_IsOK[PlayerMax];
	bool m_Change;
	bool m_Select;
	int m_SleepTime[PlayerMax];
	int m_ReadyCheck;
	int m_PlayPlayer;

	int m_Last;
	int m_Top;
	std::vector<int> m_CharacterList;
	float m_Move;
	float m_BgMove;
	
	List m_List;
};

#endif	// _PLAYER_SELECT_H_