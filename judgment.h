//============================
//
// 背景
// Author:hamada ryuuga
//
//============================
#ifndef _JUDGMENT_H_
#define _JUDGMENT_H_

//**************************************************
// インクルード
//**************************************************
#include "object3D.h"
#include "model_data.h"
class CObjectX;
class CModelData;
//**************************************************
// クラス
//**************************************************
class CJudgment : public CObject
{
public:

	enum MOVE_STATE
	{
		MOVE_NONE = 0,
		MOVE_WORLD,
		MOVE_SKY,
		MOVE_STATE_MAX
	};
	const float Timer = 0.001f;
	const float VecMove = 0.1f;
	explicit CJudgment(int nPriority = PRIORITY_EFFECT);
	~CJudgment();

	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw(DRAW_MODE drawMode) override;

	static CJudgment *CJudgment::Create(bool IsResult);

	void SetResult(bool IsResult) { m_IsResult = IsResult; }
	void SetWin(CPlayer*win) { m_win = win; }
private:
	CObject3D *m_clown;
	CObject3D *m_Bgblender;
	D3DXVECTOR3 m_size;
	int m_count;
	bool m_is;
	bool m_IsResult;
	CPlayer  * m_win;
};

#endif

