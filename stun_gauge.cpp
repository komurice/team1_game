//==================================================
// stun_gauge.cpp
// Author: Buriya Kota
//==================================================

//**************************************************
// include
//**************************************************
#include "stun_gauge.h"

#include "manager.h"
#include "object2D.h"

//**************************************************
// マクロ定義
//**************************************************
#define STUN_GAUGE_POS_X			(70.0f)
#define STUN_GAUGE_POS_Y			(57.0f)

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CStunGauge::CStunGauge(int nPriority) : CObject(nPriority)
{
	m_pStunBG = nullptr;
	m_pStunGauge = nullptr;
	m_pStunChar = nullptr;

	m_nNumStun = 0;
	m_nAnyPlayer = 0;
	m_isStun = false;
	m_nFrame = 0;

	m_nPlayerNum = 0;
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CStunGauge::~CStunGauge()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CStunGauge::Init()
{
	return S_OK;
}

//--------------------------------------------------
// 終了
//--------------------------------------------------
HRESULT CStunGauge::Init(const int& playerNum)
{
	int playPlayer = CManager::GetManager()->GetPlayerNumber();

	float pos = (float)CManager::SCREEN_WIDTH / (playPlayer);
	D3DXVECTOR3 screenPos = D3DXVECTOR3((pos * playerNum) + (pos / 2), 50.0f, 0.0f);

	m_pStunBG = CObject2D::Create(
		D3DXVECTOR3(screenPos.x - STUN_GAUGE_POS_X, screenPos.y + STUN_GAUGE_POS_Y, screenPos.z),
		D3DXVECTOR3(145.0f, 45.0f, 0.0f), PRIORITY_UI2D);
	m_pStunBG->SetTexture(CTexture::TEXTURE_STUN_UI_1);
	m_pStunBG->SetCol(D3DXCOLOR(0.0f, 0.0f, 0.0f, 0.0f));

	m_pStunGauge = CObject2D::Create(
		D3DXVECTOR3(screenPos.x - STUN_GAUGE_POS_X, screenPos.y + STUN_GAUGE_POS_Y, screenPos.z),
		D3DXVECTOR3(145.0f, 45.0f, 0.0f), PRIORITY_UI2D);
	m_pStunGauge->SetTexture(CTexture::TEXTURE_STUN_UI_2);
	m_pStunGauge->SetTexPoint(3, 2, 0, 0);
	m_pStunGauge->SetCol(D3DXCOLOR(0.0f, 0.0f, 0.0f, 0.0f));

	m_pStunChar = CObject2D::Create(
		D3DXVECTOR3(screenPos.x + 10.0f, screenPos.y + 70.0f, screenPos.z),
		D3DXVECTOR3(60.0f, 30.0f, 0.0f), PRIORITY_UI2D);
	m_pStunChar->SetTexture(CTexture::TEXTURE_STUN_UI_3);
	m_pStunChar->SetCol(D3DXCOLOR(0.0f, 0.0f, 0.0f, 0.0f));

	return S_OK;
}

//--------------------------------------------------
// 終了
//--------------------------------------------------
void CStunGauge::Uninit()
{
	CObject::DeletedObj();
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CStunGauge::Update()
{
	if (CManager::GetGameMode() == CManager::MODE_GAME ||
		CManager::GetGameMode() == CManager::MODE_TUTORIAL)
	{
		m_pStunBG->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
		m_pStunGauge->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
	}

	Flashing_();
	StunGauge_();
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CStunGauge *CStunGauge::Create(const int& playerNum)
{
	CStunGauge *pStnGauge;
	pStnGauge = new CStunGauge;

	if (pStnGauge != nullptr)
	{
		pStnGauge->Init(playerNum);
	}
	else
	{
		assert(false);
	}

	return pStnGauge;
}

//--------------------------------------------------
// 点滅
//--------------------------------------------------
void CStunGauge::Flashing_()
{
	if (m_isStun)
	{
		m_nFrame++;

		m_pStunChar->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
		m_pStunChar->AnimTexture(m_nFrame * static_cast<int>(0.3f), 3);
	}
	else
	{
		m_pStunChar->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.0f));
	}
}

//--------------------------------------------------a
// スタンゲージ
//--------------------------------------------------
void CStunGauge::StunGauge_()
{
	if(m_nNumStun >= 5)
	{
		m_pStunGauge->SetTexPoint(5, 2, m_nNumStun - 5, 1);
	}
	else
	{
		m_pStunGauge->SetTexPoint(5, 2, m_nNumStun, 0);
	}
}

//--------------------------------------------------
// スタンゲージのテクスチャ調整
//--------------------------------------------------
void CStunGauge::SetStunTex(int Point,int Max,int /*Player*/)
{
	D3DXCOLOR Data(0.0f, ((1.0f / Max)*Point), 0.0f, 1.0f);

	m_pStunGauge->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
	m_pStunGauge->SetTex(Data);
}
