//============================
//
// マップチップ
// Author:hamada ryuuga
//
//============================

//**************************************************
// include
//**************************************************
#include <assert.h>
#include "manager.h"
#include "count.h"
#include "objectX.h"
#include "object2D.h"
#include "sound.h"


//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CCount::CCount(int nPriority /* =0 */) : CObject3D(nPriority)
{
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CCount::~CCount()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CCount::Init()
{
	
	CObject3D::Init();
	SetTex(PRIORITY_UI3D);
	CObject3D::SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.0f));
	CObject3D::SetSize(D3DXVECTOR3(0.0f, 0.0f, 0.0f));

	m_Bgblender[0] = CObject3D::Create(D3DXVECTOR3(0.0f, 0.0f,0.0f),
		D3DXVECTOR3(CManager::SCREEN_WIDTH*0.5f, CManager::SCREEN_HEIGHT*0.6f, 0.0f), GetTex());
	m_Bgblender[0]->SetTexture(CTexture::TEXTURE_BG1);
	m_Bgblender[0]->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));

	m_Bgblender[1] = CObject3D::Create(D3DXVECTOR3(0.0f, 0.0f, 0.0f),
		D3DXVECTOR3(CManager::SCREEN_WIDTH*0.5f, CManager::SCREEN_HEIGHT*0.6f, 0.0f), GetTex());
	m_Bgblender[1]->SetTexture(CTexture::TEXTURE_BG2);
	m_Bgblender[1]->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.0f));

	m_Bgblender[2] = CObject3D::Create(D3DXVECTOR3(0.0f, 0.0f, 100.0f),
		D3DXVECTOR3(CManager::SCREEN_WIDTH*0.5f, CManager::SCREEN_HEIGHT*0.6f, 0.0f), PRIORITY_UI3D);
	m_Bgblender[2]->SetTexture(CTexture::TEXTURE_NONE);
	m_Bgblender[2]->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.0f));
	m_blender = 0;
	m_Noudraw = 0;
	m_Nextdraw = 1;
	m_MoveVec = false;
	m_bgChange = false;
	m_EventEnd = false;
	m_sizeEnd = false;
	//BlenderTex(CTexture::TEXTURE_BG1, CTexture::TEXTURE_BG2, CTexture::TEXTURE_BG3);
	m_speed = 0.001f;
	return S_OK;
}

//--------------------------------------------------
// 終了
//--------------------------------------------------
void CCount::Uninit()
{
	for (int i = 0; i < 3; i++)
	{
		m_Bgblender[i]->Uninit();
	}
	
	CObject3D::Uninit();

}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CCount::Update()
{
	CObject3D::Update();

	if (m_List.size() == 0)
	{
		return;
	}
	m_blender += 1;
	if (m_blender >= 60&& !m_sizeEnd)
	{
		m_blender = 0;
		m_Noudraw++;
		m_Nextdraw++;
		int Size = m_List.size();
		if (m_Nextdraw >= Size)
		{
			m_Nextdraw = 0;
			m_sizeEnd = true;
			m_List.clear();
		}
		if (m_Noudraw >= Size)
		{
			m_MoveVec = !m_MoveVec;
			m_Noudraw = 0;
			m_sizeEnd = true;
			m_List.clear();
		}

	}
	else
	{
		m_Bgblender[0]->SetTexture(m_List[m_Noudraw]);
		m_Bgblender[1]->SetTexture(m_List[m_Nextdraw]);
		float dta = (float)m_blender / 60.0f;
		m_Bgblender[0]->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f - dta));
		m_Bgblender[1]->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, dta));
	}


}

//--------------------------------------------------
// 描画
//--------------------------------------------------
void CCount::Draw(DRAW_MODE drawMode)
{
	CObject3D::Draw(drawMode);
}

//--------------------------------------------------
// 生成関数
//--------------------------------------------------
CCount * CCount::Create(D3DXVECTOR3 /*pos*/, D3DXVECTOR3 /*size*/, int nPriority)
{
	CCount *pObject2D;
	pObject2D = new CCount(nPriority);

	if (pObject2D != nullptr)
	{
		pObject2D->SetTex(nPriority);
		pObject2D->Init();

	}
	else
	{
		assert(false);
	}

	return pObject2D;
}

//--------------------------------------------------
// 設定
//--------------------------------------------------
void CCount::SetSize(D3DXVECTOR3 size)
{
	m_Bgblender[0]->SetSize(size);
	m_Bgblender[1]->SetSize(size);
}

//--------------------------------------------------
// 設定
//--------------------------------------------------
void CCount::SetPos(D3DXVECTOR3 pos)
{
	m_Bgblender[0]->SetPos(pos);
	m_Bgblender[1]->SetPos(pos);
}

//--------------------------------------------------
// ストリートビュー設定
//--------------------------------------------------
void CCount::BlenderTex(CTexture::TEXTURE Fast, CTexture::TEXTURE Second, CTexture::TEXTURE Third)
{

	m_List.clear();
	m_List.push_back(Fast);
	m_List.push_back(Second);
	m_List.push_back(Third);
	m_Bgblender[0]->SetTexture(Fast);
	m_Bgblender[1]->SetTexture(Second);

	//m_Bgblender[2]->SetTexture(Third);
}

//--------------------------------------------------
// ストリートビュー設定
//--------------------------------------------------
void CCount::AddTex(CTexture::TEXTURE add)
{
	m_List.push_back(add);
}

//--------------------------------------------------
// ストリートビュー設定
//--------------------------------------------------
void CCount::ChangeTex(CTexture::TEXTURE Fast, CTexture::TEXTURE Second, CTexture::TEXTURE Third)
{
	//CCount::Init();
	m_Log.clear();
	m_bgChange = true;
	m_Log.push_back(Fast);
	m_Log.push_back(Second);
	m_Log.push_back(Third);
	m_List = m_Log;
}

//--------------------------------------------------
// ストリートビュー設定
//--------------------------------------------------
void CCount::ChangeTex()
{
	//CCount::Init();
	//m_Log.clear();
	m_bgChange = true;
	m_EventEnd = false;
	m_blender = 0.0f;
}

