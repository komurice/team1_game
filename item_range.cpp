﻿//==================================================
// item_range.cpp
// Author: Huang Qiyue
//==================================================

//**************************************************
// include
//**************************************************
#include "item_range.h"

#include "model_data.h"
#include "fade.h"
#include "game.h"
#include "manager.h"
#include "player.h"
#include "bullet.h"
#include "stage_ui.h"
//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CItemRange::CItemRange(int nPriority) : CItem(nPriority)
{
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CItemRange::~CItemRange()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CItemRange::Init()
{

	CObjectX::SetModelData(CModelData::MODEL_ITEM_RANGE);
	CItem::Init();

	return S_OK;
}

//--------------------------------------------------
// 終了
//--------------------------------------------------
void CItemRange::Uninit()
{
	CItem::Uninit();
}


//--------------------------------------------------
// 更新
//--------------------------------------------------
void CItemRange::Update()
{
	CItem::Update();
}

//--------------------------------------------------
// 描画
//--------------------------------------------------
void CItemRange::Draw(DRAW_MODE drawMode)
{

	CItem::Draw(drawMode);
	
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CItemRange* CItemRange::Create(D3DXVECTOR3 pos, int Model)
{
	CItemRange* pPower = new CItemRange;

	if (pPower != nullptr)
	{
		pPower->Init();
		pPower->SetPos(pos);
		pPower->SetPosOriginPos(pos);
		pPower->SetModelType(Model);
	}
	else
	{
		assert(false);
	}
	return pPower;
}


//=============================================================================
// ヒット
//=============================================================================
bool CItemRange::hitevent(CPlayer* player)
{

	CModelData* Model = CManager::GetManager()->GetModelData();

	D3DXVECTOR3 Size = Model->GetModel(GetModelData()).size;
	D3DXVECTOR3 Pos = GetPos();
	D3DXVECTOR3 PlayerSize = D3DXVECTOR3(20.0f, 20.0f, 50.0f);
	D3DXVECTOR3 PlayerPos = player->GetPos();
	D3DXVECTOR3 PlayerPosOld = player->GetPosOld();

	bool IsHit = false;

	if (((Pos.y - Size.y) <= (PlayerPos.y + PlayerSize.y)) &&
		((Pos.y + Size.y) >= (PlayerPos.y - PlayerSize.y)) &&
		((Pos.x - Size.x) <= (PlayerPos.x + PlayerSize.x)) &&
		((Pos.x + Size.x) >= (PlayerPos.x - PlayerSize.x)))
	{
		player->GetUi()->SetType(CPlayer::BULLET_TYPE::RANGE_EXTENSION);
		player->SetBulletType(CPlayer::BULLET_TYPE::RANGE_EXTENSION);
		player->ShotBoost(300);
		CItemRange::Uninit();
		IsHit = true;
	}
	return IsHit;
}
