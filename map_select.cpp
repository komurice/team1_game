//==================================================
// map_select.cpp
// Author: Buriya Kota
//==================================================
//**************************************************
// include
//**************************************************
#include "map_select.h"

#include "manager.h"
#include "fade.h"
#include "input.h"
#include "sound.h"
#include "texture.h"

#include "object2D.h"

//**************************************************
// 静的メンバ変数
//**************************************************
CMapSelect *CMapSelect::m_pMapSelect = nullptr;

namespace
{// マップの数
const std::string fileName[] =
{
	"data/MAP/STAGE_TUTORIAL.json",
	"data/MAP/STAGE001.json",
	"data/MAP/STAGE002.json",
	"data/MAP/STAGE003.json",
	"data/MAP/STAGE004.json",
};
} // namespace

// 数が一致しなかったらエラー
static_assert(sizeof(fileName) / sizeof(fileName[0]) == CMapSelect::MAP_MAX, "baka");

static const float MOVE_TIME = 45.0f;

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CMapSelect::CMapSelect()
{
	for (int i = 0; i < MAP_MAX; i++)
	{
		m_pObject2D[i] = nullptr;
	}
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CMapSelect::~CMapSelect()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CMapSelect::Init()
{
	m_time = 0;
	m_nSelectMap = 0;
	m_disp = (TEXTURE)0;

	m_mode = MODE_INPUT;

	CTexture::TEXTURE tex[] =
	{
		CTexture::TEXTURE_TUTORIAL,
		CTexture::TEXTURE_STAGE1,
		CTexture::TEXTURE_STAGE2,
		CTexture::TEXTURE_STAGE3,
		CTexture::TEXTURE_STAGE4
	};

#define ARRAY_LENGTH(a) (sizeof(a)/sizeof((a)[0])) 
	static_assert(ARRAY_LENGTH(tex) == TEXTURE_MAX, "baka");

	for (int nCntMap = 0; nCntMap < MAP_MAX; nCntMap++)
	{
		// テクスチャ
		m_pObject2D[nCntMap] = CObject2D::Create(
			D3DXVECTOR3(CManager::SCREEN_WIDTH * (0.5f + nCntMap), CManager::SCREEN_HEIGHT * 0.5f, 0.0f),
			D3DXVECTOR3((float)CManager::SCREEN_WIDTH, (float)CManager::SCREEN_HEIGHT, 0.0f),
			PRIORITY_BG);
		m_pObject2D[nCntMap]->SetTexture(tex[nCntMap]);
	}

	CManager::GetManager()->GetSound()->Play(CSound::LABEL_BGM_MAP);

	return S_OK;
}

//--------------------------------------------------
// 終了
//--------------------------------------------------
void CMapSelect::Uninit()
{
	CObject::DeletedObj();
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CMapSelect::Update()
{
	CInput *pInput = CInput::GetKey();

	switch (m_mode)
	{
	case MODE_INPUT:
		if (pInput->Trigger(KEY_RIGHT))
		{// 右に行く
			if (m_disp == (TEXTURE)(TEXTURE_MAX - 1))
			{
				// 何もしない
			}
			else
			{
				// サウンド
				CManager::GetManager()->GetSound()->Play(CSound::LABEL_SE_ENTER);

				m_mode = MODE_SCROLL_L;
				m_disp = (TEXTURE)(m_disp + 1);
				m_time = 0;
				m_nSelectMap++;
			}
		}
		else if (pInput->Trigger(KEY_LEFT))
		{// 左に行く
			if (m_disp == 0)
			{
				// 何もしない
			}
			else
			{
				// サウンド
				CManager::GetManager()->GetSound()->Play(CSound::LABEL_SE_ENTER);

				m_mode = MODE_SCROLL_R;
				m_disp = (TEXTURE)(m_disp - 1);
				m_time = 0;
				m_nSelectMap--;
			}
		}

		if (pInput->Trigger(KEY_DECISION) || pInput->Trigger(MOUSE_INPUT_LEFT))
		{// マップ決定
			// サウンド
			CManager::GetManager()->GetSound()->Play(CSound::LABEL_SE_ENTER2);

			if (m_nSelectMap == 0)
			{// 遷移
				CFade::GetInstance()->SetFade(CManager::MODE_TUTORIAL);
			}
			else
			{
				CManager::GetManager()->SetMap(fileName[m_nSelectMap]);
				// 遷移
				CFade::GetInstance()->SetFade(CManager::MODE_GAME);
			}
		}

		break;

	case MODE_SCROLL_L:

		for (int nCnt = 0; nCnt < TEXTURE_MAX; nCnt++)
		{// 横にスクロール
			D3DXVECTOR3 pos = m_pObject2D[nCnt]->GetPos();

			pos.x -= CManager::SCREEN_WIDTH / MOVE_TIME;

			m_pObject2D[nCnt]->SetPos(pos);
		}

		m_time++;

		if (m_time >= MOVE_TIME)
		{// スクロールが終わったら
			m_mode = MODE_INPUT;
		}

		break;

	case MODE_SCROLL_R:
		for (int nCnt = 0; nCnt < TEXTURE_MAX; nCnt++)
		{
			D3DXVECTOR3 pos = m_pObject2D[nCnt]->GetPos();

			pos.x += CManager::SCREEN_WIDTH / MOVE_TIME;

			m_pObject2D[nCnt]->SetPos(pos);
		}

		m_time++;

		if (m_time >= MOVE_TIME)
		{// スクロールが終わったら
			m_mode = MODE_INPUT;
		}

		break;
	default:
		assert(false);
		break;
	}
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CMapSelect *CMapSelect::Create()
{
	m_pMapSelect = new CMapSelect;

	if (m_pMapSelect != nullptr)
	{
		m_pMapSelect->Init();
	}
	else
	{
		assert(false);
	}

	return m_pMapSelect;
}
