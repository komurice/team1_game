//============================
//
// 背景
// Author:hamada ryuuga
//
//============================
#ifndef _STAGE_UI_H_
#define _STAGE_UI_H_

//**************************************************
// インクルード
//**************************************************
#include "object3D.h"
#include "object2D.h"
#include "player.h"

//**************************************************
// 前方前言　実態はNG　ポインタだけならOK
//**************************************************
class CStunGauge;

//**************************************************
// クラス
//**************************************************
class CStage_Ui : public CObject3D
{
public:

	const float Timer = 0.001f;

	explicit CStage_Ui(int nPriority = PRIORITY_BG);
	~CStage_Ui();

	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw(DRAW_MODE drawMode) override;

	static CStage_Ui *CStage_Ui::Create(int Type, int nPriority);

	void SetMyNumber(int Number) { m_MyNumber = Number; };

	void SetType(CPlayer::BULLET_TYPE EVENY);
	void SetBullet();
	void SetBoost(int Boost) { m_BoostTime = Boost; }
	void PopEffect(D3DXVECTOR3 pos);
	
private:
	CObject2D *m_UiBox;
	CObject2D *m_Bullet[5];
	CObject2D *m_Boost;
	float m_blender;
	int m_Noudraw;
	int m_Nextdraw;
	int m_MyNumber;
	int m_Max;
	int m_BoostTime;
	int m_ShotTime;
	float m_alpha;
	CPlayer::BULLET_TYPE m_Type;
};

#endif