//============================
//
// マップチップ
// Author:hamada ryuuga
//
//============================

//**************************************************
// include
//**************************************************
#include <assert.h>

#include "manager.h"
#include "object2D.h"
#include "renderer.h"
#include "map.h"
#include "utility.h"
#include "sound.h"

#include <chrono>
#include <random>

#include "mapblack.h"
#include "game.h"
#include "goal.h"
#include "input.h"
#include "item.h"
#include "debug_proc.h"
#include "enemy.h"
#include "audience.h"
#include "risk.h"
#include "signboard.h"
#include "audience.h"
//**************************************************
// 定数定義
//**************************************************
namespace
{
	// ポリゴンの幅
	const float POLYGON_WIDTH = 50.0f;
	// ポリゴンの幅
	const float POLYGON_HEIGHT = 50.0f;
}

// 形成する四角形の基準値
const D3DXVECTOR3 sVtx[4] =
{
	D3DXVECTOR3(-1.0f, -1.0f, 0.0f),
	D3DXVECTOR3(+1.0f, -1.0f, 0.0f),
	D3DXVECTOR3(-1.0f, +1.0f, 0.0f),
	D3DXVECTOR3(+1.0f, +1.0f, 0.0f),
};

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CMap::CMap(int nPriority) : CObject(nPriority)
{

}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CMap::~CMap()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CMap::Init()
{
	m_block.clear();
	m_item.clear();

	CModelData* Data = CManager::GetManager()->GetModelData();
	Data->SetModelTex(CModelData::MODEL_BOX, 0, "data/TEXTURE/block001.png");
	Data->SetModelTex(CModelData::MODEL_BOXLEFT, 0, "data/TEXTURE/block001.png");
	Data->SetModelTex(CModelData::MODEL_BOXRIGHT, 0, "data/TEXTURE/block001.png");
	//Data->SetModelTex(CModelData::MODEL_WOOD1, 0, "data/TEXTURE/Wood001.jpg");



	/*CAdience::Create(D3DXVECTOR3(0.0f, -300.0f, 300.0f), D3DXVECTOR3(3.0f, 3.0f, 3.0f), "data/TEXT/hamadaEnemy/snake.txt");
	CAdience::Create(D3DXVECTOR3(200.0f, -300.0f, 300.0f), D3DXVECTOR3(3.0f, 3.0f, 3.0f), "data/TEXT/hamadaEnemy/snake.txt");
	CAdience::Create(D3DXVECTOR3(-200.0f, -300.0f, 300.0f), D3DXVECTOR3(3.0f, 3.0f, 3.0f), "data/TEXT/hamadaEnemy/snake.txt");
	CAdience::Create(D3DXVECTOR3(400.0f, -300.0f, 300.0f), D3DXVECTOR3(3.0f, 3.0f, 3.0f),"data/TEXT/hamadaEnemy/snake.txt");
	CAdience::Create(D3DXVECTOR3(-400.0f, -300.0f, 300.0f), D3DXVECTOR3(3.0f, 3.0f, 3.0f),"data/TEXT/hamadaEnemy/snake.txt");*/

	m_Log = 0;

	m_time = 0.0f;
	m_itemTimeCount = 0.0f;
	m_itemSpawnTimer = 5.0f;

	m_enemyTimeCount = 0.0f;
	m_enemySpawnTimer = 6.0f;
	m_rondomSpawn = 0;
	m_enemyPopSet = false;
	return S_OK;
}

//--------------------------------------------------
// 終了
//--------------------------------------------------
void CMap::Uninit()
{
	CObject::DeletedObj();
}



//--------------------------------------------------
// 更新
//--------------------------------------------------
void CMap::Update()
{
	CInput* pInput = CInput::GetKey();
	int wheel = pInput->GetMouseWheel();

	CManager::MODE mode = CManager::GetGameMode();
	if ((mode == CManager::MODE_GAME))
	{
		if (CGame::GetGame() != nullptr)
		{
			if (!CGame::GetGame()->GetStagr())
			{
				return;
			}
		}
	}

	UpdateTime();
}

//--------------------------------------------------
// 描画
//--------------------------------------------------
void CMap::Draw(DRAW_MODE /*drawMode*/)
{
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CMap* CMap::Create()
{
	CMap* pObject2D;
	pObject2D = new CMap(0);

	if (pObject2D != nullptr)
	{
		pObject2D->Init();

	}
	else
	{
		assert(false);
	}

	return pObject2D;
}

//--------------------------------------------------
// セーブ
//--------------------------------------------------
void CMap::Save()
{
	int nIndex = 0;
	std::string MyfilePass = ("data/MAP/STAGE002.json");

	for (int nCnt = 0; nCnt < GetblockSize(); nCnt++)
	{

		std::string name = "BLOCK";
		std::string Number = std::to_string(nIndex);
		name += Number;
		m_Jsonblock[name] = { { "POS",{
			{ "X", Getblock(nCnt)->GetPos().x } ,
			{ "Y", Getblock(nCnt)->GetPos().y - m_Log } ,
			{ "Z", Getblock(nCnt)->GetPos().z } } },
			{ "TYPE", Getblock(nCnt)->GetModelType() }
		};

		nIndex++;
	}
	m_Jsonblock["INDEX"] = nIndex;

	auto jobj = m_Jsonblock.dump();
	std::ofstream writing_file;
	const std::string pathToJSON = MyfilePass.c_str();
	writing_file.open(pathToJSON, std::ios::out);
	writing_file << jobj << std::endl;
	writing_file.close();

}

//--------------------------------------------------
// ロード
//--------------------------------------------------
void CMap::Load(const std::string Pass)
{
	std::string MyfilePass = (Pass.c_str());
	std::ifstream ifs(MyfilePass.c_str());

	int nIndex = 0;

	if (ifs)
	{
		ifs >> m_Jsonblock;
		nIndex = m_Jsonblock["INDEX"];

		for (int nCnt = 0; nCnt < nIndex; nCnt++)
		{
			std::string name = "BLOCK";
			std::string Number = std::to_string(nCnt);
			name += Number;

			D3DXVECTOR3 Pos = (D3DXVECTOR3(m_Jsonblock[name]["POS"]["X"], m_Jsonblock[name]["POS"]["Y"], m_Jsonblock[name]["POS"]["Z"]));
			int Type = m_Jsonblock[name]["TYPE"];

			switch ((BLOCKTYPE)Type)
			{
			case CMap::BLOCKTYPE_NORMAL:
				Addblock(CBlock::Create(Pos, Type, CBlock::BLOCKTYPE_NORMAL));
				break;
			case CMap::BLOCKTYPE_GOAL:
				//Addblock(CBlock::Create(Pos, Type));
				Addblock(CGoal::Create(Pos, Type));
				break;
			case CMap::BLOCKTYPE_LEFT:
				Addblock(CBlock::Create(Pos, Type, CBlock::BLOCKTYPE_LEFT));
				break;
			case CMap::BLOCKTYPE_RIGHT:
				Addblock(CBlock::Create(Pos, Type, CBlock::BLOCKTYPE_RIGHT));
				break;
			case CMap::BLOCKTYPE_SPAWN:
				m_itemSpawnPos.push_back(Pos);
				break;
			case CMap::BLOCKTYPE_BROKEN:
				Addblock(CBlock::Create(Pos, Type, CBlock::BLOCKTYPE_BROKEN));
				break;
			case CMap::ENEMY_SAI:
				m_enemySpawn.push_back(EnemySpawnData{ Pos ,CEnemy::ENEMY_SAI });
				break;
			case CMap::ENEMY_FOX:
				m_enemySpawn.push_back(EnemySpawnData{ Pos ,CEnemy::ENEMY_FOX });
				break;
			case CMap::ENEMY_ARAIGUMA:
				m_enemySpawn.push_back(EnemySpawnData{ Pos ,CEnemy::ENEMY_ARAIGUMA });
				break;
			case CMap::ENEMY_KYOURYU:
				m_enemySpawn.push_back(EnemySpawnData{ Pos ,CEnemy::ENEMY_KYOURYU });
				break;
			case CMap::BLOCKTYPE_BOARD:
			{
				int model = m_Jsonblock[name]["SET"];
				Addblock(CSignBoard::Create(Pos, (CSignBoard::SIGN_BOARD_TYPE)model));
			}
			break;
			case CMap::BLOCKTYPE_BUSH:
			{
				Pos.y += 75.0f;
				CObject3D* obj = CObject3D::Create(Pos, D3DXVECTOR3(100.0f, 150.0f, 0.0f), PRIORITY_EFFECT);
				obj->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
				obj->SetTexture(CTexture::TEXTURE_KUSA);
			}
			break;
			case CMap::BLOCKTYPE_MAX:
				break;
			default:
				break;
			}
		}
	}
}

//--------------------------------------------------
// 当たり判定
//--------------------------------------------------
bool CMap::blockHit(const D3DXVECTOR3 pos, const D3DXVECTOR3 Size, CPlayer* player)
{
	const D3DXVECTOR3& screenSize = br::GetScreenSize();
	bool Is = false;
	bool Hit = false;

	for (int block = 0; block < GetblockSize(); block++)
	{
		CBlock* Noublock = Getblock(block);
		D3DXVECTOR3 blockPos = Noublock->GetPos();
		D3DXVECTOR3 blockSize = Noublock->GetSize();

		if (((blockPos.y - blockSize.y) <= (screenSize.y)) &&
			((blockPos.y + blockSize.y) >= (-screenSize.y)) &&
			((blockPos.x - blockSize.x) <= (screenSize.x)) &&
			((blockPos.x + blockSize.x) >= (-screenSize.x)))
		{
			Is = Noublock->Collision(pos, Size, player);
			if (Is)
			{
				Hit = true;
			}
		}
	}
	return Hit;
}

bool CMap::itemHit(const D3DXVECTOR3 pos, const D3DXVECTOR3 Size, CPlayer* player)
{
	if (m_item.empty())
		return false;

	const D3DXVECTOR3& screenSize = br::GetScreenSize();
	bool IsHit = false;

	for (auto it = m_item.begin(); it != m_item.end(); )
	{
		if ((*it).second->getIsRemove() || (*it).second->GetModelData() == CModelData::MODEL_NONE)
		{
			(*it).second->Uninit();
			it = m_item.erase(it);
			continue;
		}

		const D3DXVECTOR3 blockPos = (*it).second->GetPos();
		const D3DXVECTOR3 blockSize = (*it).second->GetSize();

		if (((blockPos.y - blockSize.y) <= (screenSize.y)) &&
			((blockPos.y + blockSize.y) >= (-screenSize.y)) &&
			((blockPos.x - blockSize.x) <= (screenSize.x)) &&
			((blockPos.x + blockSize.x) >= (-screenSize.x)))
		{
			IsHit = (*it).second->Collision(pos, Size, player);
		}
		++it;
	}

	return IsHit;
}


bool CMap::EnemyHit(const D3DXVECTOR3 pos, const D3DXVECTOR3 Size, CPlayer* player, bool isHit)
{
	if (m_enemy.empty())
		return false;

	bool IsHit = false;

	for (auto it = m_enemy.begin(); it != m_enemy.end(); )
	{
		if (*it == nullptr || (*it)->GetIsLive() == false)
		{
			(*it)->Uninit();
			it = m_enemy.erase(it);
		}
		else
		{
			if ((*it)->Collision(pos, Size, player, isHit))
				IsHit = true;
			++it;
		}
	}

	return IsHit;
}

void CMap::UpdateTime()
{
	static auto start = std::chrono::steady_clock::now();

	// 現在の時間を取得
	const auto current = std::chrono::steady_clock::now();

	// 前回のフレームからの経過時間を計算
	const auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(current - start);

	// 経過時間を秒単位に変換
	const float elapsedTime = duration.count() / 1000.0f;

	// 経過時間を合計時間に加算
	m_time += elapsedTime;

	// 現在の時間を次のフレームの開始時間として保存
	start = current;

	// アイテムの更新
	UpdateItem(elapsedTime);
	UpdateEnemy(elapsedTime);
}

void CMap::UpdateItem(float elapsedTime)
{
	// アイテムのスポーン時間を加算
	m_itemTimeCount += elapsedTime;

	if (m_itemSpawnPos.empty())
		return;

	// アイテムをスポーンする時間になった場合
	if (m_itemTimeCount >= m_itemSpawnTimer)
	{
		int randomSpawn = GetRandom(0, m_itemSpawnPos.size() - 1);

		auto it = m_item.find(randomSpawn);
		if (it != m_item.end())
		{
			m_itemTimeCount = 0.0f;
			return;
		}

		// ランダムにアイテムの種類を選択し、対応するアイテムを生成して配置する
		switch (GetRandom(BLOCKTYPE_ITEM_RANGE, BLOCKTYPE_ITEM_STUN))
		{
		case BLOCKTYPE_ITEM_RANGE:
			// アイテムの範囲を作成し、位置を設定する
			m_item[randomSpawn] = CItem::Create(m_itemSpawnPos[randomSpawn], CItem::ITEMTYPE_RANGE);
			break;

		case BLOCKTYPE_ITEM_SHOTGUN:
			// ショットガンのアイテムを作成し、位置を設定する
			m_item[randomSpawn] = (CItem::Create(m_itemSpawnPos[randomSpawn], CItem::ITEMTYPE_SHOTGUN));
			break;

		case BLOCKTYPE_ITEM_STUN:
			// スタンのアイテムを作成し、位置を設定する
			m_item[randomSpawn] = (CItem::Create(m_itemSpawnPos[randomSpawn], CItem::ITEMTYPE_STUN));
			break;

		case BLOCKTYPE_ITEM_METAL:
			// デバフのアイテムを作成し、位置を設定する
			m_item[randomSpawn] = (CItem::Create(m_itemSpawnPos[randomSpawn], CItem::ITEMTYPE_METAL));
			break;

		case BLOCKTYPE_ITEM_SONICSHOT:
			// デバフのアイテムを作成し、位置を設定する
			m_item[randomSpawn] = (CItem::Create(m_itemSpawnPos[randomSpawn], CItem::ITEMTYPE_SONICSHOT));
			break;
		}

		// スポーンタイマーをリセット
		m_itemTimeCount = 0.0f;
	}


}

void CMap::UpdateEnemy(float elapsedTime)
{
	// アイテムのスポーン時間を加算
	m_enemyTimeCount += elapsedTime;

	if (m_enemySpawn.empty())
		return;

	if (m_enemyTimeCount >= m_enemySpawnTimer - 2.0f && !m_enemyPopSet)
	{
		m_enemyPopSet = true;
		m_rondomSpawn = GetRandom(0, m_enemySpawn.size() - 1);
		CRisk::Create(m_enemySpawn[m_rondomSpawn].pos, D3DXVECTOR3(50.0f, 50.0f, 0.0f));
	}
	// アイテムをスポーンする時間になった場合
	if (m_enemyTimeCount >= m_enemySpawnTimer && m_enemyPopSet)
	{
		m_enemyPopSet = false;

		switch ((CEnemy::ACTION_TYPE)m_enemySpawn[m_rondomSpawn].type)
		{
		case CEnemy::ENEMY_SAI:
			m_enemy.push_back(CEnemy::Create(m_enemySpawn[m_rondomSpawn].pos, m_enemySpawn[m_rondomSpawn].type));
			break;
		case CEnemy::ENEMY_FOX:
			m_enemy.push_back(CEnemy::Create(m_enemySpawn[m_rondomSpawn].pos, m_enemySpawn[m_rondomSpawn].type));
			break;
		case CEnemy::ENEMY_ARAIGUMA:
			m_enemy.push_back(CEnemy::Create(m_enemySpawn[m_rondomSpawn].pos, m_enemySpawn[m_rondomSpawn].type));
			break;
		case CEnemy::ENEMY_KYOURYU:
			CManager::GetManager()->GetSound()->Play(CSound::LABEL_SE_REX);					//鳴き声
			m_enemy.push_back(CEnemy::Create(m_enemySpawn[m_rondomSpawn].pos, m_enemySpawn[m_rondomSpawn].type));
			break;
		default:
			break;
		}

		// スポーンタイマーをリセット
		m_enemyTimeCount = 0.0f;
	}
}


void CMap::ResetItem()
{
	m_itemTimeCount = 0.0f;
}

int CMap::GetRandom(int min, int max) const
{
	// ランダムな数を生成するための乱数生成器を作成
	std::random_device rd;
	std::mt19937 gen(rd());

	// 乱数の範囲を指定
	const int minNum = min;
	const int maxNum = max;
	const std::uniform_int_distribution<int> distribution(minNum, maxNum);

	// 乱数生成器のシードを設定
	gen.seed(static_cast<unsigned int>(m_time));

	// ランダムな数を生成
	const int randomNumber = distribution(gen);

	// 生成したランダムな数
	return randomNumber;
}

//------------------------------------
//今あるEnemyのロード
//------------------------------------
void CMap::LoadfileEnemy(const char* pFileName)
{
	std::ifstream ifs(pFileName);
	nl::json JlistEnemy;//リストの生成
	int nIndex = 0;

	if (ifs)
	{
		ifs >> JlistEnemy;
		nIndex = JlistEnemy["INDEX"];
		D3DXVECTOR3 pos;
		D3DXVECTOR3 Min;
		D3DXVECTOR3 Max;
		D3DXVECTOR3 rot;
		std::string Type;
		int ModelType = 0;

		for (int nCntEnemy = 0; nCntEnemy < nIndex; nCntEnemy++)
		{
			CEnemy* Enemy = nullptr;
			std::string name = "ENEMY";
			std::string Number = std::to_string(nCntEnemy);
			name += Number;

			pos = D3DXVECTOR3(JlistEnemy[name]["POS"]["X"], JlistEnemy[name]["POS"]["Y"], JlistEnemy[name]["POS"]["Z"]);
			rot = D3DXVECTOR3(JlistEnemy[name]["ROT"]["X"], JlistEnemy[name]["ROT"]["Y"], JlistEnemy[name]["ROT"]["Z"]);
			Min = D3DXVECTOR3(JlistEnemy[name]["MIN"]["X"], JlistEnemy[name]["MIN"]["Y"], JlistEnemy[name]["MIN"]["Z"]);
			Max = D3DXVECTOR3(JlistEnemy[name]["MAX"]["X"], JlistEnemy[name]["MAX"]["Y"], JlistEnemy[name]["MAX"]["Z"]);

			Type = JlistEnemy[name]["PASS"];
			ModelType = JlistEnemy[name]["TYPE"];

			switch ((CMap::ENEMYTYPE)ModelType)
			{
			case CMap::ENEMY:

				CAdience::Create(pos, D3DXVECTOR3(1.0f, 1.0f, 1.0f), "data\\TEXT\\hamadaEnemy\\snake.txt")->SetRot(rot);


				break;
			case CMap::CHASEENEMY:
				pos.y += -400;
				CAdience::Create(pos, D3DXVECTOR3(1.0f, 1.0f, 1.0f), "data\\TEXT\\hamadaEnemy\\snake.txt")->SetRot(rot);


				break;
			case CMap::BOSS:
			{
				CAdience::Create(pos, D3DXVECTOR3(3.0f, 3.0f, 3.0f), "data\\TEXT\\hamadaEnemy\\ponske.txt")->SetRot(rot);

				break;
			}
			default:

				break;
			}

		}
	}
}

//------------------------------------
//今あるオブジェクトの読み込み
//------------------------------------
void CMap::Loadfile(const char* pFileName)
{
	std::ifstream ifs(pFileName);
	nl::json JBuilding;//リストの生成

	int nIndex = 0;

	if (ifs)
	{
		ifs >> JBuilding;
		nIndex = JBuilding["INDEX"];

		D3DXVECTOR3 pos;
		D3DXVECTOR3 size;
		D3DXVECTOR3 rot;
		std::string Type;
		bool gold;
		bool Check;
		int ModelType = 0;

		for (int nCntBuilding = 0; nCntBuilding < nIndex; nCntBuilding++)
		{
			CObjectX* Buil = nullptr;
			std::string name = "BUILDING";
			std::string Number = std::to_string(nCntBuilding);
			name += Number;

			if (JBuilding.count(name) == 0)
			{
				//int a = 0;
			}
			pos = D3DXVECTOR3(JBuilding[name]["POS"]["X"], JBuilding[name]["POS"]["Y"], JBuilding[name]["POS"]["Z"]);
			rot = D3DXVECTOR3(JBuilding[name]["ROT"]["X"], JBuilding[name]["ROT"]["Y"], JBuilding[name]["ROT"]["Z"]);
			Type = JBuilding[name]["TYPE"];
			ModelType = JBuilding[name]["MODELTYPE"];
			gold = JBuilding[name]["GOLD"];
			Check = JBuilding[name]["CHECK"];

			switch ((CMap::MODELTYPE)ModelType)
			{
			case CMap::NORMAL:
			{
				Buil = CObjectX::Create();
				//Buil->SetModelData(Type.c_str());
				Buil->SetPos(D3DXVECTOR3(200.0f, 200.0f, 0.0f));
				Buil->SetRot(rot);

				break;
			}
			default:
				break;
			}
		}
	}
}
