//==================================================
// 後ろのやつ.h
// Author: Buriya Kota
//==================================================
#ifndef _AUBIENCE＿LIST_H_
#define _AUBIENCE＿LIST_H_

//**************************************************
// インクルード
//**************************************************
#include "object.h"

//**************************************************
// 前方前言　実態はNG　ポインタだけならOK
//**************************************************
class CObjectX;
class CAdience;
//**************************************************
// クラス
//**************************************************
class CAubience_list : public CObject
{
public:
	static const int aubience_list = 5;
	CAubience_list(int nPriority = PRIORITY_PLAYER);
	~CAubience_list() override;

	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw(DRAW_MODE /*drawMode*/) override;
	static CAubience_list *Create();
private:
	CObjectX* m_Model;
	CAdience* m_Adience[aubience_list];
	D3DXVECTOR3 m_Point;
	int m_delay;
	int m_timer;
	float m_beesY;
};

#endif	// _GAME_MODE_H_