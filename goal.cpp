//==================================================
// goal.cpp
// Author: Buriya Kota
//==================================================

//**************************************************
// include
//**************************************************
#include "goal.h"

#include "model_data.h"
#include "fade.h"
#include "game.h"
#include "manager.h"
#include "player.h"

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CGoal::CGoal(int nPriority) : CBlock(nPriority)
{
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CGoal::~CGoal()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CGoal::Init()
{
	CBlock::Init();

	CObjectX::SetModelData(CModelData::MODEL_GOAL);

	return S_OK;
}

//--------------------------------------------------
// 終了
//--------------------------------------------------
void CGoal::Uninit()
{
	CBlock::Uninit();
}


//--------------------------------------------------
// 更新
//--------------------------------------------------
void CGoal::Update()
{
	CBlock::Update();

}

//--------------------------------------------------
// 描画
//--------------------------------------------------
void CGoal::Draw(DRAW_MODE drawMode)
{



	CBlock::Draw(drawMode);
	
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CGoal *CGoal::Create(D3DXVECTOR3 pos, int Model)
{
	CGoal *pGoal;
	pGoal = new CGoal;

	if (pGoal != nullptr)
	{
		pGoal->Init();
		pGoal->SetPos(pos);
		pGoal->SetPosOriginPos(pos);
		pGoal->SetModelType(Model);
	}
	else
	{
		assert(false);
	}

	return pGoal;
}


//=============================================================================
// ヒット
//=============================================================================
bool CGoal::hitevent(CPlayer*player)
{
	CModelData* Model = CManager::GetManager()->GetModelData();

	D3DXVECTOR3 Size = Model->GetModel(GetModelData()).size;
	D3DXVECTOR3 Pos = GetPos();
	D3DXVECTOR3 PlayerSize = D3DXVECTOR3(20.0f, 20.0f, 50.0f);
	D3DXVECTOR3 PlayerPos = player->GetPos();
	D3DXVECTOR3 PlayerPosOld = player->GetPosOld();
	bool isHit = false;
	if (((Pos.y - Size.y) <= (PlayerPos.y + PlayerSize.y)) &&
		((Pos.y + Size.y) >= (PlayerPos.y - PlayerSize.y)) &&
		((Pos.x - Size.x) <= (PlayerPos.x + PlayerSize.x)) &&
		((Pos.x + Size.x) >= (PlayerPos.x - PlayerSize.x)))
	{
		isHit = true;
		CFade::GetInstance()->SetFade(CManager::MODE_GAME);
	}
	return isHit;
}