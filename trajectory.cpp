//============================
//
// lineeffect
// Author:hamada ryuuga
//
//============================


#include "Trajectory.h"
#include "manager.h"
#include "utility.h"
#include "input.h"
#include "object3D.h"

CTrajectory::CTrajectory(int nPriority) : CObject(nPriority)
{

}
CTrajectory::~CTrajectory()
{

}

//=========================================
// 初期化処理
//=========================================
HRESULT CTrajectory::Init(void)
{
	LPDIRECT3DDEVICE9 pDevice = CManager::GetManager()->GetRenderer()->GetDevice();
	m_pTextureEmesh = NULL;
	// 初期化処理
	m_rot = D3DXVECTOR3(0.0f, 0.0f, 0.0f);	// 回転座標

	m_pVtxBuff = nullptr;	    // 頂点バッファーへのポインタ
	m_pTextureEmesh = nullptr;        //テクスチャのポインタ


	m_pos = { 0.0f,0.0f,0.0f };	// 頂点座標
	m_posOrigin = { 0.0f,-25.0f*0.5f,0.0f };	// 頂点座標
	m_rot = { 0.0f,0.0f,0.0f };	// 回転座標
	m_vtx = MAXTRAJECTORY;//頂点数							// m_mtxWorld;// ワールドマトリックス
	
	// 頂点バッファの生成
	pDevice->CreateVertexBuffer(sizeof(VERTEX_3D) * m_vtx,
		D3DUSAGE_WRITEONLY,
		FVF_VERTEX_3D,
		D3DPOOL_MANAGED,
		&m_pVtxBuff,
		NULL);

	VERTEX_3D* pVtx = NULL;
	m_pVtxBuff->Lock(0, 0, (void**)&pVtx, 0);
	for (int nVtx =0; nVtx < m_vtx; nVtx++)
	{
		pVtx[nVtx].pos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);	
		pVtx[nVtx].col = D3DXCOLOR(0.0f, 0.0f, 1.0f, 0.5f);

	}
	m_color = D3DXCOLOR(0.0f, 0.0f, 1.0f, 0.5f);
	// 頂点座標をアンロック
	m_pVtxBuff->Unlock();
	m_IsSetMtx = false;
	return S_OK;
}

//=========================================
// 終了処理
//=========================================
void CTrajectory::Uninit(void)
{
	// 頂点バッファーの解放
	if (m_pVtxBuff != NULL)
	{
		m_pVtxBuff->Release();
		m_pVtxBuff = NULL;
	}
	if (m_pTextureEmesh != NULL)
	{
		m_pTextureEmesh->Release();
		m_pTextureEmesh = NULL;
	}


	DeletedObj();
}

//=========================================
// 更新処理
//=========================================
void CTrajectory::Update(void)
{

	VERTEX_3D* pVtx = NULL;
	m_pVtxBuff->Lock(0, 0, (void**)&pVtx, 0);

	m_color.a = 0.0f;
	for (int nVtx = MAXTRAJECTORY - 3; nVtx >= 0; nVtx--)
	{
		pVtx[nVtx + 2].pos = pVtx[nVtx].pos;

		pVtx[nVtx + 2].col = m_color;

		m_color.a += 1.0f/MAXTRAJECTORY;
	}


	D3DXVECTOR3 OfSetPos(0.0f, 25.0f, 0.0f);	//	縦座標の変換 サイズ変更可能
	D3DXVec3TransformCoord(&OfSetPos, &OfSetPos, m_ModelWorld);
	D3DXVECTOR3 Pos(0.0f, 0.0f, 0.0f);	//原点
	D3DXVec3TransformCoord(&Pos, &Pos, m_ModelWorld);

	pVtx[1].pos = OfSetPos;	//高さ変更
	pVtx[0].pos = Pos;//原点
	m_color.a = 1.0f;
	pVtx[0].col = m_color;
	pVtx[1].col = m_color;
	// 頂点座標をアンロック
	m_pVtxBuff->Unlock();
}

//=========================================
// 描画処理
//=========================================
void CTrajectory::Draw(DRAW_MODE drawMode)
{
	if (!m_myDraw)
	{
		return;
	}
	LPDIRECT3DDEVICE9 pDevice = CManager::GetManager()->GetRenderer()->GetDevice();
	D3DXMATRIX mtxRot, mtxTrans;	// 計算用マトリックス

	pDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
	//ライト設定falseにするとライトと食らわない
	pDevice->SetRenderState(D3DRS_LIGHTING, FALSE);
	//pDevice->SetRenderState(D3DRS_LIGHTING, TRUE);
	// ワールドマトリックスの初期化
	// 行列初期化関数(第1引数の行列を単位行列に初期化)
	D3DXMatrixIdentity(&m_mtxWorld);

	// 向きを反映
	// 行列回転関数(第1引数にヨー(y)ピッチ(x)ロール(z)方向の回転行列を作成)
	D3DXMatrixRotationYawPitchRoll(&mtxRot, m_rot.y, m_rot.x, m_rot.z);
	// 行列掛け算関数(第2引数×第3引数第を１引数に格納)
	D3DXMatrixMultiply(&m_mtxWorld, &m_mtxWorld, &mtxRot);

	// 位置を反映
	// 行列移動関数(第１引数にX,Y,Z方向の移動行列を作成)
	D3DXMatrixTranslation(&mtxTrans, m_posOrigin.x, m_posOrigin.y, m_posOrigin.z);
	// 行列掛け算関数(第2引数×第3引数第を１引数に格納)
	D3DXMatrixMultiply(&m_mtxWorld, &m_mtxWorld, &mtxTrans);

	// ワールド座標行列の設定
	pDevice->SetTransform(D3DTS_WORLD, &m_mtxWorld);

	// 頂点バッファをデバイスのデータストリームに設定
	pDevice->SetStreamSource(0, m_pVtxBuff, 0, sizeof(VERTEX_3D));

	// 頂点フォーマットの設定
	pDevice->SetFVF(FVF_VERTEX_3D);
	//テクスチャの設定
	pDevice->SetTexture(0, NULL);

	// ポリゴンの描画
	pDevice->DrawPrimitive(D3DPT_TRIANGLESTRIP, 0, m_vtx-2);


	//テクスチャの設定
	pDevice->SetTexture(0, NULL);

	pDevice->SetRenderState(D3DRS_LIGHTING, TRUE);
	pDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);
}


//=========================================
// 軌跡の色変更
//=========================================
void CTrajectory::SetPlayerNum(int Is)
{
	m_num = Is;
	switch (m_num)
	{
	case 0://P1
		//水色
		SetColor(D3DXCOLOR(0.7f, 1.0f, 1.0f, 1.0f));
		break;
	case 1://P2
		//あか
		SetColor(D3DXCOLOR(1.0f, 0.4f, 0.4f, 1.0f));
		break;
	case 2://P3
		//黄色
		SetColor(D3DXCOLOR(1.0f, 1.0f, 0.4f, 1.0f));
		break;
	case 3://P4
		//緑
		SetColor(D3DXCOLOR(0.78f, 1.0f, 0.7f, 1.0f));
		break;
	case 99://NPC
		SetColor(D3DXCOLOR(0.0f, 0.0f, 0.0f, 1.0f));
		break;
	default:
		break;
	}
}

//=============================================================================
// GetPos関数
//=============================================================================
const D3DXVECTOR3 *CTrajectory::GetPos() const
{
	return &m_posOrigin;
}

//=============================================================================
// SetPos関数
//=============================================================================
void CTrajectory::SetPos(const D3DXVECTOR3 &pos)
{
	m_pos = pos;
	//m_posOrigin = pos;
	VERTEX_3D* pVtx = NULL;
	m_pVtxBuff->Lock(0, 0, (void**)&pVtx, 0);
	for (int nVtx = 0; nVtx < m_vtx; nVtx++)
	{
		pVtx[nVtx].pos = m_pos;
	}
	// 頂点座標をアンロック
	m_pVtxBuff->Unlock();

}
//=============================================================================
// Create関数
//=============================================================================
CTrajectory* CTrajectory::Create()
{
	CTrajectory * pObject = nullptr;
	pObject = new CTrajectory(PRIORITY_EFFECT);

	if (pObject != nullptr)
	{
		pObject->Init();
	}
	return pObject;
}




