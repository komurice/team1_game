//==================================================
// bullet.h
// Author: Buriya Kota
//==================================================
#ifndef _RISK_H_
#define _RISK_H_

//**************************************************
// インクルード
//**************************************************
#include "object3d.h"

//**************************************************
// 前方前言　実態はNG　ポインタだけならOK
//**************************************************

//**************************************************
// 構造体定義
//**************************************************

//**************************************************
// クラス
//**************************************************


class CRisk : public CObject3D
{
public:

	enum class BULLET_TYPE
	{
		DEFAULT,
		RANGE_EXTENSION,
		SHOTGUN,
		STUN_BULLET
	};

	explicit CRisk(int nPriority = PRIORITY_BULLET);
	~CRisk();

	HRESULT Init() override;
	void Update() override;

	static CRisk* Create(const D3DXVECTOR3 pos, const D3DXVECTOR3 size);



private:

private:

	float m_timer;
	bool m_up;
	int m_life;
	
};

#endif	// _BULLET_H_