//==================================================
// game.h
// Author: Buriya Kota
//==================================================
#ifndef _GAME_H_
#define _GAME_H_

//**************************************************
// インクルード
//**************************************************
#include "game_mode.h"
#include "model_data.h"

//**************************************************
// 名前付け
//**************************************************
namespace nl = nlohmann;

//**************************************************
// マクロ定義
//**************************************************

//**************************************************
// 前方前言　実態はNG　ポインタだけならOK
//**************************************************
class CScore;
class CPause;
class CMeshField;
class CTimer;
class CObjectX;
class CPlayer;
class CMap;
class CEnemyCount;
class CBg;
class CCount;

//**************************************************
// クラス
//**************************************************
class CGame : public CGameMode
{
public:

	enum MODEL
	{// 状態
		MODEL_UP = 0,
		MODEL_DOWN,
		MODEL_LEFT,
		MODEL_RIGHT,
		MODEL_MAX
	};

	CGame();
	~CGame() override;

	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw(DRAW_MODE /*drawMode*/) override {}

	// プレイヤーの情報の取得
	static CGame* GetGame() { return m_pGame; }
	CPlayer* GetPlayer3D(int nNumber) { return m_pPlayer[nNumber]; }
	CScore* GetScore() { return m_pScore; }
	CPause* GetPause() { return m_pPause; }
	CCount* GetCount() { return m_count; }
	
	CMeshField* GetMeshField(int num) { return m_pMeshField[num]; }
	CTimer* GetTimer() { return m_pTimer; }
	std::string GetfilePass(int num) { return m_pFileName[num]; }
	// フレームの設定
	int GetFrame() { return m_time; }
	 CMap*GetMap() { return m_Map; }
	 CEnemyCount* GetEnemy() { return m_enemy; }
	static CGame *Create();
	bool GetStagr() { return m_stagr; }
	void SetStagr(bool IsStagr) { m_stagr = IsStagr; }
	void SetSound(std::string pass);
private:
	int m_time;		// ゲーム開始からの時間
	int m_popEnemy;
	static CGame* m_pGame;
	std::vector<CPlayer*> m_pPlayer;
	std::vector <std::string> m_pFileName;
	CScore *m_pScore;
	CPause *m_pPause;
	std::vector<CMeshField*> m_pMeshField;
	CTimer *m_pTimer;
	CMap *m_Map;
	CEnemyCount*m_enemy;
	CBg*m_Bg;
	CBg*m_Bg2;
	
	CCount*m_count;
	bool m_stagr;
	
};

#endif	// _GAME_H_