//==================================================
// bullet.h
// Author: Buriya Kota
//==================================================
#ifndef _SPEECH_H_
#define _SPEECH_H_

//**************************************************
// インクルード
//**************************************************
#include "object3d.h"

//**************************************************
// 前方前言　実態はNG　ポインタだけならOK
//**************************************************

//**************************************************
// 構造体定義
//**************************************************

//**************************************************
// クラス
//**************************************************


class CSpeech : public CObject3D
{
public:

	enum class BULLET_TYPE
	{
		DEFAULT,
		RANGE_EXTENSION,
		SHOTGUN,
		STUN_BULLET
	};

	explicit CSpeech(int nPriority = PRIORITY_BULLET);
	~CSpeech();

	HRESULT Init() override;
	void Update() override;

	static CSpeech* Create(const D3DXVECTOR3 pos, const D3DXVECTOR3 size, int type);

	void SetTex(int type);

private:

private:

	float m_timer;
	bool m_up;
	int m_life;
	float m_Size;
};

#endif

