//==================================================
// tutorial.h
// Author: Buriya Kota
//==================================================
#ifndef _TUTORIAL_H_
#define _TUTORIAL_H_

//**************************************************
// インクルード
//**************************************************
#include "game_mode.h"

//**************************************************
// 前方宣言
//**************************************************
class CObject2D;
class CPlayer;
class CMap;
class CEnemyCount;
class CBg;
class CPause;

//**************************************************
// クラス
//**************************************************
class CTutorial : public CGameMode
{
public:
	CTutorial();
	~CTutorial() override;

	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw(DRAW_MODE /*drawMode*/) override {}

	static CTutorial* GetTutorial() { return m_pTutorial; }

	CPlayer* GetPlayer3D(int nNumber) { return m_pPlayer[nNumber]; }
	CPause* GetPause() { return m_pPause; }
	CMap* GetMap() { return m_pMap; }

	CEnemyCount* GetEnemy() { return m_enemy; }

	static CTutorial* Create();

private:
	enum MODE
	{// 状態
		MODE_INPUT = 0,
		MODE_SCROLL_L,
		MODE_SCROLL_R,
		MODE_MAX
	};

	enum TEXTURE
	{// 使用しているテクスチャ
		TEXTURE_MAP = 0,
		TEXTURE_MANUAL,
		TEXTURE_OPERATION_METHOD,
		TEXTURE_MAX
	};

	enum TEXTURE_CHOICE
	{// 使用しているテクスチャ
		TEXTURE_R = 0,
		TEXTURE_L,
		TEXTURE_CHOICE_MAX
	};

	// オブジェクト2Dの箱
	CObject2D *m_pObject2D[TEXTURE_MAX];
	CObject2D *m_pChoice[2];

	int m_time;

	MODE m_mode;
	TEXTURE m_disp;

	static CTutorial *m_pTutorial;
	std::vector<CPlayer*> m_pPlayer;
	CMap *m_pMap;

	int m_popEnemy;
	CEnemyCount* m_enemy;

	CBg* m_pBg;
	CPause *m_pPause;
};

#endif	// _TUTORIAL_H_