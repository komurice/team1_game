//==================================================
// player3D.cpp
// Author: Buriya Kota
//==================================================

//**************************************************
// include
//**************************************************
#include <assert.h>

#include "utility.h"
#include "debug_proc.h"

#include "game.h"
#include "tutorial.h"

#include "manager.h"
#include "camera.h"
#include "sound.h"
#include "input.h"
#include "model.h"

#include "score.h"
#include "object3D.h"

#include "shadow.h"
#include "meshfield.h"
#include "bullet.h"
#include "map.h"
#include "utility.h"
#include "motion.h"
#include "particle.h"
#include "fade.h"
#include "stage_ui.h"
#include "player.h"
#include "cpu.h"
#include "enemy_count.h"
#include "effect.h"
//**************************************************
// マクロ定義
//**************************************************
#define MAX_SPEED			(10.0f)
#define FRICTION			(0.07f)
#define GRAVITY				(0.5f)
#define MAX_ACCEL			(1.0f)
#define MIN_ACCEL			(0.0f)
#define COIN_PARTICLE		(5)
#define NUM_GOAL			(2)
#define KNOCKBACK			(20.0f)
#define BULLET_TIME			(12)
#define JUMP				(15.0f)
#define X_MOVE_LIMIT		(50.0f)
#define Y_MOVE_LIMIT		(6.0f)
#define KNOCK_BACK_POWER_X	(0.7f)
#define KNOCK_BACK_POWER_Y	(3.0f)
#define CHARGE_TIME			(120)

namespace
{
	const std::string fileName[5] =
	{ "data/TEXT/motion_apple.txt",
		"data/TEXT/motion_grape.txt",
		"data/TEXT/motion_Orange.txt",
		"data/TEXT/motion_strawberry.txt",
		"data/TEXT/motion_cherry.txt" };

} // namespace

  //**************************************************
  // 静的メンバ変数
  //**************************************************
const float CCpu::PLAYER_SPEED = 5.0f;

// 
const CObject::UPDATE_FUNC CCpu::mUpdateFunc[] =
{
	UPDATE_FUNC_CAST(Update_Idle),
	UPDATE_FUNC_CAST(Update_Land),
	UPDATE_FUNC_CAST(Update_Fly),
};

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CCpu::CCpu(int nPriority) : CPlayer(nPriority)
{
	// 数が一致しなかったらエラー
	static_assert(sizeof(mUpdateFunc) / sizeof(mUpdateFunc[0]) == STATE_MAX, "baka");
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CCpu::~CCpu()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CCpu::Init()
{
	CPlayer::Init();

	SetType(TYPE_PLAYER);
	if (CManager::GetGameMode() == CManager::MODE_GAME)
	{
		//m_Ui = CStage_Ui::Create(m_nPlayerNumber, 0);
	}

	SetPlayerNumber(99);
	InitStateFunc(mUpdateFunc, STATE_MAX);

	// プレイヤーの初期状態
	SetState(STATE_IDLE);
	m_delay = 0;
	m_attackTimer = 0;
	return S_OK;
}

//--------------------------------------------------
// 終了
//--------------------------------------------------
void CCpu::Uninit()
{
	//CManager::GetManager()->AddRanking(m_nPlayerNumber);

	// 現在のモーション番号の保管
	CMotionModel3D::Uninit();

	// deleteフラグを立てる
	CObject::DeletedObj();
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CCpu::Update()
{
	if (CManager::GetGameMode() == CManager::MODE_GAME)
	{
		MoveRandom();
	}

	CObject::Update();

	// posOld の更新
	D3DXVECTOR3 posOld = CPlayer::GetPos();
	CPlayer::SetPosOld(posOld);

	if (CManager::GetGameMode() == CManager::MODE_GAME)
	{
		Plantarea();
	}

	D3DXVECTOR3 move = CPlayer::GetMove();
	if ((CManager::GetGameMode() == CManager::MODE_GAME) ||
		(CManager::GetGameMode() == CManager::MODE_TUTORIAL))
	{
		
		// 重力
		move.y += -GRAVITY;

		CPlayer::SetMove(move);

		if (!m_IsStun)
		{// スタン中かどうか
			// プレイヤーの動き
			Control_();
		}

		// リコールの処理
		Recall_();

		// ノックバック
		KnockBack_();

		// スタン
		Stun_();

		HitEnemy();

		D3DXVECTOR3 pos = CPlayer::GetPos();
		//ブースト
		m_MoveboostTime--;

		move = CPlayer::GetMove();
		if (m_MoveboostTime <= 0)
		{
			m_Moveboost = 0.0f;
			m_MoveboostTime = 0;
		}
		if (m_Moveboost > 0.0f)
		{
			move.x *= m_Moveboost;

		}
		move.x += (0.0f - move.x) * (0.025f);    //（目的の値-現在の値）＊減衰係数

		pos += move;
		SetPos(pos);
	
		CPlayer::SetMove(move);
		if (CGame::GetGame() != nullptr)
		{
			if (CGame::GetGame()->GetMap()->blockHit(pos, m_size, this))
			{//ここあたったとき
				m_playerState = PLAYER_GROUND;
				move.y = 0.0f;
			}
			else
			{
				m_playerState = PLAYER_SKY;
			}
			if (CGame::GetGame()->GetMap()->itemHit(pos, m_size, this))
			{//ここあたったとき

			}
			if (CGame::GetGame()->GetMap()->EnemyHit(pos, m_size, this,false))
			{//ここあたったとき

			}

		}
		else if (CTutorial::GetTutorial() != nullptr)
		{
			if (CTutorial::GetTutorial()->GetMap()->blockHit(pos, m_size, this))
			{//ここあたったとき
				m_playerState = PLAYER_GROUND;
				move.y = 0.0f;
			}
			else
			{
				m_playerState = PLAYER_SKY;
			}
			if (CTutorial::GetTutorial()->GetMap()->itemHit(pos, m_size, this))
			{//ここあたったとき

			}
		}
		move = CPlayer::GetMove();
		Outarea();
		
		CPlayer::SetMove(move);
	}
	
#ifdef _DEBUG
	// デバッグ表示
	//SetPos(pos);

#endif // DEBUG
}

//--------------------------------------------------
// 描画
//--------------------------------------------------
void CCpu::Draw(DRAW_MODE drawMode)
{
	CPlayer::Draw(drawMode);
}

//--------------------------------------------------
// アイドルの処理
//--------------------------------------------------
void CCpu::Update_Idle()
{
	SetState(STATE_LAND);
}

//--------------------------------------------------
// 歩いてるとき
//--------------------------------------------------
void CCpu::Update_Land()
{
	SetState(STATE_FLY);
}

//--------------------------------------------------
// ジャンプしたとき
//--------------------------------------------------
void CCpu::Update_Fly()
{
	SetState(STATE_IDLE);
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CCpu* CCpu::Create(D3DXVECTOR3 pos, int number, int playerNumber)
{
	CCpu* pPlayer3D = nullptr;
	pPlayer3D = new CCpu;

	if (pPlayer3D != nullptr)
	{
		pPlayer3D->SetPlayerNumber(number);
		pPlayer3D->Init();
		pPlayer3D->SetPos(pos);
		pPlayer3D->CPlayer::SetPos(pos);
		pPlayer3D->SetMotion(fileName[playerNumber].c_str());
	}
	else
	{
		assert(false);
	}

	return pPlayer3D;
}

//--------------------------------------------------
// 画面内の納める
//--------------------------------------------------
void CCpu::InScreen_()
{
	D3DXVECTOR3 pos = CPlayer::GetPos();

	// 画面内に収める処理
	//if (pos.y < -240.0f)
	//{// 上
	//	pos.y = -240.0f;
	//	m_move.y = 0.0f;
	//}
	//if (pos.x > 360.0f)
	//{// 右
	//	pos.x = 360.0f;
	//}
	//if (pos.x < -360.0f)
	//{// 左
	//	pos.x = -360.0f;
	//}

	CPlayer::SetPos(pos);
}

//--------------------------------------------------
// リコール
//--------------------------------------------------
void CCpu::Recall_()
{
	D3DXVECTOR3 pos = GetPos();

	if (!m_bRecall)
	{
		// プレイヤーの過去の位置を保存
		m_playerRecallPos.push_back(pos);

		if (m_playerRecallPos.size() > 120)
		{// sizeが120を超えたら最初の要素を消す
			m_playerRecallPos.erase(m_playerRecallPos.begin());
		}
	}

	// 値が同じだった場合(値が隣接している場合)は詰める
	// erase　コンテナの要素を削除する
	// unique 隣り合った重複要素を除いた要素を、範囲の先頭に集める 削除はされないため　erase　で削除する
	//m_playerRecallPos.erase(std::unique(m_playerRecallPos.begin(), m_playerRecallPos.end()), m_playerRecallPos.end());

	if (m_bRecall)
	{// リコール中
		m_nCountRecall++;
		SetPos(m_playerRecallPos[m_nVectorSize - m_nCountRecall]);
	}

	if (m_nCountRecall == m_nVectorSize)
	{// カウントとサイズが一緒になったら
		for (int i = 0; i < m_nVectorSize; i++)
		{// 現在の位置を代入する
			m_playerRecallPos[i] = m_pos;
		}
		// 初期化
		m_nVectorSize = 0;
		m_nCountRecall = 0;
		m_bMaxSize = false;
		m_bRecall = false;
	}
}

//--------------------------------------------------
// ノックバック
//--------------------------------------------------
void CCpu::KnockBack_()
{
	D3DXVECTOR3 bulletMove = GetbulletMove();
	bool IsKnockBack = GetKnockBack();
	// 弾の方向を取ってくる
	D3DXVec3Normalize(&bulletMove, &bulletMove);

	if (m_isBulletAttack)
	{
		m_fKnockBackPowerX = KNOCK_BACK_POWER_X * 2.0f;
		m_fKnockBackPowerY = KNOCK_BACK_POWER_Y * 2.0f;
	}
	else
	{
		m_fKnockBackPowerX = KNOCK_BACK_POWER_X;
		m_fKnockBackPowerY = KNOCK_BACK_POWER_Y;
	}

	if (IsKnockBack)
	{	
		D3DXVECTOR3 knockBackPower(0.0f, 0.0f, 0.0f);

		knockBackPower.x = m_fKnockBackPowerX;
		knockBackPower.y = m_fKnockBackPowerY;

		// ノックバックの値
		m_move.x += knockBackPower.x * bulletMove.x;
		m_move.y += knockBackPower.y;

		if (m_isBulletAttack)
		{
			if (m_move.x >= X_MOVE_LIMIT * 2.0f)
			{// Xの上限
				m_move.x = X_MOVE_LIMIT * 2.0f;
			}

			if (m_move.x <= -X_MOVE_LIMIT * 2.0f)
			{// Xの上限
				m_move.x = -X_MOVE_LIMIT * 2.0f;
			}

			if (m_move.y >= Y_MOVE_LIMIT * 1.5f)
			{// Yの上限
				m_move.y = Y_MOVE_LIMIT * 1.5f;
			}
		}
		else
		{
			if (m_move.x >= X_MOVE_LIMIT)
			{// Xの上限
				m_move.x = X_MOVE_LIMIT;
			}

			if (m_move.x <= -X_MOVE_LIMIT)
			{// Xの上限
				m_move.x = -X_MOVE_LIMIT;
			}

			if (m_move.y >= Y_MOVE_LIMIT)
			{// Yの上限
				m_move.y = Y_MOVE_LIMIT;
			}
		}

		m_delay++;
	
		if (m_delay >= 5)
		{
			m_delay = 0;
			CParticle::Create(GetPos(), bulletMove*2.0f, 10, CParticle::MOVE_HANABI, PRIORITY_EFFECT);

			//CEffect::Create(GetPos(), -move*2.0f, D3DXVECTOR3(50.0f, 50.0f, 0.0f), CParticle::MOVE_DAMAGE, PRIORITY_EFFECT);
		}
		m_nKnockBackTime++;
	}

	if (m_nKnockBackTime > 10)
	{
		m_nKnockBackTime = 0;
		IsKnockBack = false;
		SetKnock(IsKnockBack);
	}
}

//--------------------------------------------------
// ジャンプ
//--------------------------------------------------
void CCpu::Jump_()
{
	D3DXVECTOR3 move = CPlayer::GetMove();

	CMotion* pMotion = CPlayer::GetMotion();

	if (m_playerState == PLAYER_GROUND&& m_isJump == false)
	{// ジャンプ
		move.y += JUMP;
		m_playerState = PLAYER_SKY;

		if (pMotion != nullptr && m_action != TYPE_JUMP)
		{//アニメーション設定
			m_action = TYPE_JUMP;
			pMotion->SetNumMotion(m_action);
		}
		m_isJump = true;
		CPlayer::SetMove(move);
	}

}

//--------------------------------------------------
// スタン
//--------------------------------------------------
void CCpu::Stun_()
{
	
}
//=============================================================================
// 画面外
//=============================================================================
void  CCpu::Outarea()
{
	D3DXVECTOR3 PlayerPos = GetPos();
	D3DXVECTOR3 worldPos = D3DXVECTOR3((float)CManager::SCREEN_WIDTH, (float)CManager::SCREEN_HEIGHT, 0.0f);
	worldPos.x += 150.0f;
	bool IsHit = false;
	if (PlayerPos.x <= -worldPos.x / 2)
	{
		IsHit = true;
		CParticle::Create(PlayerPos, D3DXVECTOR3(10.0f, 0.0f, 0.0f), 100, CParticle::MOVE_DEF, PRIORITY_BG);
		CManager::GetManager()->GetSound()->Play(CSound::LABEL_SE_SPLASH);					//爆発音
	}

	if (PlayerPos.x >= worldPos.x / 2)
	{
		IsHit = true;
		CParticle::Create(PlayerPos, D3DXVECTOR3(-10.0f, 0.0f, 0.0f), 100, CParticle::MOVE_DEF, PRIORITY_BG);
		CManager::GetManager()->GetSound()->Play(CSound::LABEL_SE_SPLASH);					//爆発音
	}
	if (PlayerPos.y >= worldPos.y)
	{
		IsHit = true;
		CParticle::Create(PlayerPos, D3DXVECTOR3(0.0f, -10.0f, 0.0f), 100, CParticle::MOVE_DEF, PRIORITY_BG);
		CManager::GetManager()->GetSound()->Play(CSound::LABEL_SE_SPLASH);					//爆発音
	}

	if (PlayerPos.y <= -worldPos.y)
	{
		IsHit = true;
		CParticle::Create(PlayerPos, D3DXVECTOR3(0.0f, 10.0f, 0.0f), 100, CParticle::MOVE_DEF, PRIORITY_BG);
		CManager::GetManager()->GetSound()->Play(CSound::LABEL_SE_SPLASH);					//爆発音
	}
	if (IsHit)
	{
		if (CManager::GetGameMode() == CManager::MODE_GAME)
		{
			CGame::GetGame()->GetEnemy()->SudEnemy();
		}
		if (CManager::GetGameMode() == CManager::MODE_TUTORIAL)
		{
			CTutorial::GetTutorial()->GetEnemy()->SudEnemy();
		}

		Uninit();
	}

}
//--------------------------------------------------
// 弾を撃つ処理
//--------------------------------------------------
void CCpu::Shoot(const D3DXVECTOR3& pos, const D3DXVECTOR3& EnemyPos)
{
	D3DXVECTOR3 vec(0.0f, 0.0f, 0.0f);

	{// スティックの方向を取ってくる
		D3DXVECTOR3 Pos = (EnemyPos-m_pos);
		Pos.y *= -1;
		D3DXVec3Normalize(&Pos, &Pos);

		D3DXVECTOR3 stick = Pos;

		if ((stick.x == 0.0f) && (stick.y == 0.0f) && (stick.z == 0.0f))
		{// スティックが倒れていなかったら
			//m_pArrow->SetCol(D3DXCOLOR(0.0f, 0.0f, 0.0f, 0.0f));
			return;
		}
		else
		{
			// チャージ時間
			m_nChargeTime++;
			m_bIsCharge = true;
			//m_pArrow->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));

			if (m_nChargeTime > CHARGE_TIME)
			{
				m_nChargeTime = CHARGE_TIME;
			}

			// スティックの角度の計算
			float stickAngle = atan2f(-stick.y, stick.x);
			if (stickAngle < 0.0f)
			{// 角度の正規化
				stickAngle += (D3DX_PI * 2.0f);
			}
			// 範囲
			float range = (D3DX_PI * 2.0f) / 8.0f;
			// 今倒しているスティックの方向
			int direction = (int)(stickAngle / range + 0.5f);
			// 弾の角度
			float bulletAnge = direction * range;
			vec.x = cosf(bulletAnge);
			vec.y = sinf(bulletAnge);
			vec.z = 0.0f;

			D3DXVec3Normalize(&vec, &vec);

			m_pArrow->SetPos(m_pos + vec * 100.0f);
			m_pArrow->SetRot(D3DXVECTOR3(0.0f, 0.0f, bulletAnge - D3DX_PI / 2.0f));
		}
	}

	{
		// 弾の大きさ
		float bulletSize = m_nChargeTime * 0.1f;

		// 弾の残り数が0以下の場合はデフォルトの弾に戻す
		if (m_remainingBullets <= 0)
		{
			m_bulletType = BULLET_TYPE::DEFAULT;
			m_remainingBullets = 999;
		}
	
			m_nChargeTime = 0;

			// 弾のタイプに応じて処理を行う
			switch (m_bulletType)
			{
			case BULLET_TYPE::DEFAULT:
				// デフォルトの弾を作成し、位置を設定する
				CBullet::Create(pos,vec * 10.0f, D3DXVECTOR3(10.0f + bulletSize, 10.0f + bulletSize, 0.0f), 99);
				m_remainingBullets--;
				break;

			case BULLET_TYPE::RANGE_EXTENSION:
				// 射程延長の弾を作成し、位置を設定する
				CBullet::Create(pos,vec * 10.0f, D3DXVECTOR3(10.0f, 10.0f, 0.0f), 99, CBullet::BULLET_TYPE::RANGE_EXTENSION);
				m_remainingBullets--;
				m_Ui->SetBullet();
				break;

			case BULLET_TYPE::SHOTGUN:
			{
				// ジョイスティックの方向角度を計算する
				float joystick_angle = atan2(-vec.y, vec.x);
				// ショットガンを生成する
				constexpr int num_bullets = 4;  // ショットガンの数
				constexpr float spread = D3DX_PI / 8;  // ショットガンの全角度（この例では、ショットガンは45度の角度で発射されます）
				for (int i = 0; i < num_bullets; i++) 
				{
					// このショットガンのオフセット角度を計算する
					float bullet_angle_offset = spread * ((float)i / (num_bullets - 1) - 0.5f);
					// このショットガンの最終的な方向を計算する
					float bullet_angle = joystick_angle + bullet_angle_offset;
					// 新しい方向ベクトルを計算する
					D3DXVECTOR3 bullet_vec(cos(bullet_angle), -sin(bullet_angle), 0.0f);
					// 新しい方向ベクトルを使用してショットガンの弾を作成する
					CBullet::Create(pos,bullet_vec * 10.0f, D3DXVECTOR3(10.0f, 10.0f, 0.0f), 99, CBullet::BULLET_TYPE::SHOTGUN);
				}
				m_Ui->SetBullet();
				m_remainingBullets--;
			}
			break;

			case BULLET_TYPE::STUN_BULLET:
				// スタン弾を作成し、位置を設定する
				CBullet::Create(pos,vec * 10.0f, D3DXVECTOR3(10.0f, 10.0f, 0.0f), 99, CBullet::BULLET_TYPE::STUN_BULLET)->SetPos(pos);
				m_remainingBullets--;
				break;

			default:
				assert(false);
				break;
			}
		
	}
}

//--------------------------------------------------
// らんど
//--------------------------------------------------
void CCpu::MoveRandom()
{
	if (m_moveFrame <= 0)
	{
		m_isJump = false;
		m_moveFrame = br::IntRandom(1, 35);
		m_moveVec = (MOVE_VEC)br::IntRandom(0, VEC_MAX-1);
	}
}

//--------------------------------------------------
// 操作
//--------------------------------------------------
void CCpu::Control_()
{
	CMotion* pMotion = CPlayer::GetMotion();

	D3DXVECTOR3 vec(0.0f, 0.0f, 0.0f);

	D3DXVECTOR3 rotDest = GetRotDest();
	bool IsAnimation = false;
	if (m_moveFrame >= 0)
	{
		m_moveFrame -- ;
		IsAnimation = true;
		
		switch (m_moveVec)
		{
		case CCpu::VEC_UP:
			Jump_();
			m_moveFrame = 0;
			m_Speed = PLAYER_SPEED;
			break;
		case CCpu::VEC_LEFT:
			vec.x = 1;
			m_Speed = PLAYER_SPEED;
			break;
		case CCpu::VEC_RIGHT:
			vec.x = -1;
			m_Speed = PLAYER_SPEED;
			break;
		case CCpu::VEC_SLOW_LEFT:
			vec.x = 1;
			m_Speed = PLAYER_SPEED*0.3f;
			break;
		case CCpu::VEC_SLOW_RIGHT:
			vec.x = -1;
			m_Speed = PLAYER_SPEED*0.3f;
			break;
		case CCpu::VEC_UPLEFT:
			Jump_();
			vec.x = 1;
			m_Speed = PLAYER_SPEED;
			break;
		case CCpu::VEC_UPRIGHT:
			Jump_();
			vec.x = -1;
			m_Speed = PLAYER_SPEED;
			break;
		case CCpu::VEC_MAX:
			break;
		default:
			break;
		}
	}

	D3DXVECTOR3 pos = CPlayer::GetPos();
	D3DXVECTOR3 rot = CPlayer::GetRot();

	if (vec.x != 0.0f || vec.y != 0.0f)
	{// 移動とそれに合わせた回転
	 // ベクトルの正規化
		D3DXVec3Normalize(&vec, &vec);
		// キーボード
		pos += vec * m_Speed;
		CPlayer::SetPos(pos);
	}

	if (vec != D3DXVECTOR3(0.0f, 0.0f, 0.0f))
	{// プレイヤーが移動した方向に向きを合わせる
		IsAnimation = true;
		if (pMotion != nullptr && m_action != TYPE_MOVE)
		{//アニメーション設定
			m_action = TYPE_MOVE;
			pMotion->SetNumMotion(m_action);

		}
		if (vec != D3DXVECTOR3(0.0f, 0.0f, 0.0f))
		{
			m_rotDest.y = atan2f(vec.x / 32767.0f, vec.y / 32767.0f);
		}

		m_rotDest.y += D3DX_PI;
	}

	// 目的の角度の正規化
	br::NormalizeAngleDest(&rot.y, &m_rotDest.y);

	// 目的の角度を現在の角度に近づける
	rot.y += (m_rotDest.y - rot.y) * 0.2f;

	SetRot(rot);
	// 現在の角度の正規化
	br::NormalizeAngle(&rot.y);



	if (IsAnimation == false)
	{//全てのkeyがおしてない
		m_action = TYPE_NEUTRAL;
		pMotion->SetNumMotion(m_action);
	}

	// 弾の発射
	//Shoot(pos);
}

//=============================================================================
// 画面ないに納める
//=============================================================================
void CCpu::Plantarea()
{

	D3DXVECTOR3 PlayerPos = GetPos();
	D3DXVECTOR3 worldPos = D3DXVECTOR3(480.0f, 480.0f, 0.0f);

	bool IsHit = false;
	if (PlayerPos.x <= -worldPos.x)
	{
		IsHit = true; 
		m_moveVec = VEC_UPLEFT;
	}

	if (PlayerPos.x >= worldPos.x)
	{
		IsHit = true;
		m_moveVec = VEC_UPRIGHT;
	}
	if (IsHit)
	{
	
		m_moveFrame = 15;
	}
}


//=============================================================================
// 画面ないに納める
//=============================================================================
void CCpu::HitEnemy()
{
	D3DXVECTOR3 PlayerPos = GetPos();
	static const float ATTACKTIMER = 30;

	D3DXVECTOR3 EnemyPos(0.0f, 0.0f, 0.0f);

	for (int i = 0; i < CManager::GetManager()->GetPlayerNumber(); i++)
	{
		if (CManager::GetGameMode() == CManager::MODE_GAME)
		{
			EnemyPos = CGame::GetGame()->GetPlayer3D(i)->GetPos();
		}
		else if (CManager::GetGameMode() == CManager::MODE_TUTORIAL)
		{
			EnemyPos = CTutorial::GetTutorial()->GetPlayer3D(i)->GetPos();
		}

		D3DXVECTOR3 hitPos = D3DXVECTOR3(100.0f, 100.0f, 0.0f);

		bool isHit = false;

		isHit = hmd::CollisionCircle(PlayerPos, 100.0f, EnemyPos, 100.0f);
	
		if (isHit)
		{
			m_attackTimer++;
			if (m_attackTimer >= ATTACKTIMER)
			{
				m_attackTimer = 0;
				if (CManager::GetGameMode() == CManager::MODE_GAME)
				{
					Shoot(PlayerPos, EnemyPos);
				}
			}
		}
	}
}