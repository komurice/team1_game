//==================================================
// stun_gauge.h
// Author: Buriya Kota
//==================================================
#ifndef _STUN_GAUGE_H_
#define _STUN_GAUGE_H_

//**************************************************
// インクルード
//**************************************************
#include "object.h"

//**************************************************
// 前方前言　実態はNG　ポインタだけならOK
//**************************************************
class CObject2D;

//**************************************************
// クラス
//**************************************************
class CStunGauge : public CObject
{
public:
	static const int MAX_PLAYER = 4;
public:
	explicit CStunGauge(int nPriority = PRIORITY_UI2D);
	~CStunGauge() override;

	HRESULT Init() override;
	HRESULT Init(const int& playerNum);
	void Uninit() override;
	void Update() override;
	void Draw(DRAW_MODE /*drawMode*/) override {}

	static CStunGauge *Create(const int& playerNum);

	int GetNumStun() { return m_nNumStun; }
	void SetNumStun(int nNumStun) { m_nNumStun = nNumStun; }

	void SetAnyPlayer(int nAnyPlayer) { m_nAnyPlayer = nAnyPlayer; }

	void SetIsStun(bool isStun) { m_isStun = isStun; }

	void CStunGauge::SetStunTex(int Point, int Max, int Player);
private:
	void Flashing_();
	void StunGauge_();

private:
	CObject2D *m_pStunBG;
	CObject2D *m_pStunGauge;
	CObject2D *m_pStunChar;

	// プレイヤーの人数
	int m_nPlayerNum;

	// 現在のスタン数
	int m_nNumStun;
	// どのプレイヤーか
	int m_nAnyPlayer;
	// スタン中かどうか
	bool m_isStun;

	// フレーム
	int m_nFrame;
};

#endif // _STUN_GAUGE_H_