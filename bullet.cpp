//==================================================
// bullet.cpp
// Author: Buriya Kota
//==================================================

//**************************************************
// include
//**************************************************
#include "bullet.h"
#include "player.h"
#include "block.h"
#include "debug_proc.h"
#include "effect.h"
#include "particle.h"
#include "trajectory.h"
#include "sound.h"
#include "manager.h"
#include "ring.h"
#include "spike.h"

//静的メンバ変数
int CBullet::m_nSoundCnt = 0;	//サウンドの番号1

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CBullet::CBullet(int nPriority) : CObject3D(nPriority)
{
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CBullet::~CBullet()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CBullet::Init()
{
	m_tra = CTrajectory::Create();
	m_tra->SetPos(GetPos());
	D3DXMATRIX* mat = GetWorldMtxPointer();

	// 位置を反映			↓posの情報を使って移動行列を作る
	D3DXMatrixTranslation(mat, GetPos().x, GetPos().y, GetPos().z);
	m_tra->SetMtx(mat);
	m_tra->SetIsDraw(false);
	m_tra->SetPlayerNum(m_nShotPlayerNumber);

    CObject3D::Init();
    SetBillboard(true);
	SetPlayerNumColor(m_nShotPlayerNumber);
	SetTexture(CTexture::TEXTURE_BULLET);
	

    return S_OK;
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CBullet::Update()
{
    CObject3D::Update();

    MoveBullet_();
    CollisionPlayer_();
    CollisionBlock_();
	//m_tra->SetPosOr(GetPos());
    m_life--;
    if (m_life <= 0)
    {
		m_tra->Uninit();
        Uninit();
    }
}

void CBullet::Draw(DRAW_MODE drawMode)
{
	CObject3D::Draw(drawMode);
	if (m_bulletType != BULLET_TYPE::ATTACK)
	{
		m_tra->SetIsDraw(true);
	}
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CBullet* CBullet::Create(const D3DXVECTOR3 pos, D3DXVECTOR3 move, const D3DXVECTOR3 size, int nShotPlayerNumber, BULLET_TYPE type)
{
    CBullet* pBullet;
    pBullet = new CBullet(PRIORITY_OBJECT);

    if (pBullet != nullptr)
    {
        pBullet->m_nShotPlayerNumber = nShotPlayerNumber;
		pBullet->SetPos(pos);
        pBullet->Init();
		pBullet->SetPos(pos);
        pBullet->SetMove(move);
        pBullet->SetSize(size);
        pBullet->SetBulletType(type);
    }
    else
    {
        assert(false);
    }

    return pBullet;
}

void CBullet::SetBulletType(BULLET_TYPE type)
{
    m_bulletType = type;

    switch (type)
    {
    case BULLET_TYPE::DEFAULT:
        m_life = 35;
        break;
    case BULLET_TYPE::SHOTGUN:
        m_life = 20;
        break;
    case BULLET_TYPE::RANGE_EXTENSION:
        m_life = 70;
        break;
    case BULLET_TYPE::STUN_BULLET:
        m_life = 35;
        break;
	case BULLET_TYPE::ATTACK:
		m_life = 10;
		SetSize(D3DXVECTOR3(50.0f, 50.0f, 0.0f));
		SetMove(D3DXVECTOR3(0.0f,0.0f,0.0f));
		SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.0f));
		break;
	default:
		break;
    }
}

CBullet::BULLET_TYPE CBullet::GetBulletType() const
{
    return m_bulletType;
}

//--------------------------------------------------
// 制御
//--------------------------------------------------
void CBullet::MoveBullet_()
{
    D3DXVECTOR3 move = GetMove();
    SetMove(move);

    MovePos(move);

    if ((GetPos().x >= 1000.0f) || (GetPos().x <= -1000.0f))
    {
		m_tra->Uninit();
        Uninit();
    }
}

//--------------------------------------------------
// プレイヤーとの当たり判定
//--------------------------------------------------
bool CBullet::CollisionPlayer_()
{
	bool bIsLanding = false;

	D3DXVECTOR3 pos = GetPos();
	D3DXVECTOR3 size = GetSize();

	CObject* pObject = CObject::GetTop(PRIORITY_PLAYER);

	while (pObject)
	{// オブジェクトがnullptrになるまで
		CObject::TYPE_3D objType = pObject->GetType();
		if (objType == CObject::TYPE_PLAYER)
		{// そのオブジェクトがアイテムじゃなかったら
		 // ダイナミックキャスト
			CPlayer* pPlayer = dynamic_cast<CPlayer*>(pObject);
			if ((pPlayer == nullptr))
			{
				assert(false);
				return bIsLanding;
			}

			// 当たったほうのプレイヤー番号
			int playerNumber = pPlayer->GetPlayerNumber();

			if (playerNumber != m_nShotPlayerNumber)
			{
				D3DXVECTOR3 targetPos = pPlayer->GetPos();
				D3DXVECTOR3 targetSize = pPlayer->GetSize();

				bool bIsHit = IsCollision(targetPos, pos, targetSize, size);
				if (bIsHit)
				{// 当たった時
				 // サウンド
					if (m_nSoundCnt == 0)
					{
						CManager::GetManager()->GetSound()->Play(CSound::LABEL_SE_DAMAGE1);		//サウンド1
						m_nSoundCnt++;
					}
					else if (m_nSoundCnt == 1)
					{
						CManager::GetManager()->GetSound()->Play(CSound::LABEL_SE_DAMAGE2);		//サウンド2
						m_nSoundCnt = 0;
					}

					D3DXVECTOR3 move = GetMove();
					pPlayer->SetKnockBack(move, true);

					if (m_bulletType == BULLET_TYPE::ATTACK)
					{
						//CSpike::Create(GetPos(), m_nShotPlayerNumber);
						CRing::Create(GetPos(), m_nShotPlayerNumber);
						pPlayer->IsAttack(true);
					}

					if (!pPlayer->GetIsStun())
					{// スタン中は加算しない
						pPlayer->AddStun(1);
					}

					CEffect::Create(GetPos(), D3DXVECTOR3(0.0f, 0.0f, 0.0f), D3DXVECTOR3(50.0f, 50.0f, 0.0f), CParticle::MOVE_DAMAGE, PRIORITY_EFFECT);
					
					m_tra->Uninit();
					Uninit();

					bIsLanding = true;
				}
			}
		}

		pObject = pObject->GetNext();
	}

	return bIsLanding;
}

//--------------------------------------------------
// ブロックとの当たり判定
//--------------------------------------------------
bool CBullet::CollisionBlock_()
{
    bool bIsLanding = false;

    D3DXVECTOR3 pos = GetPos();
    D3DXVECTOR3 size = GetSize();

    CObject* pObject = CObject::GetTop(PRIORITY_OBJECT);

    while (pObject)
    {// オブジェクトがnullptrになるまで
        CObject::TYPE_3D objType = pObject->GetType();
        if (objType == CObject::TYPE_BLOCK)
        {// そのオブジェクトがアイテムじゃなかったら
         // ダイナミックキャスト
            CBlock* pBlock = dynamic_cast<CBlock*>(pObject);
            if ((pBlock == nullptr))
            {
                assert(false);
                return bIsLanding;
            }

            D3DXVECTOR3 targetPos = pBlock->GetPos();
            D3DXVECTOR3 targetSize = pBlock->GetSize();

            bool bIsHit = IsCollision(targetPos, pos, targetSize, size);
            if (bIsHit)
            {// 当たった時
             // サウンド
             //CManager::GetManager()->GetSound()->Play(CSound::LABEL_SE_GET_ITEM);
                bIsLanding = true;
				m_tra->Uninit();
                Uninit();
            }
        }

        pObject = pObject->GetNext();
    }

    return bIsLanding;
}

//=========================================
// 軌跡の色変更
//=========================================
void CBullet::SetPlayerNumColor(int Is)
{
	int num = Is;
	switch (num)
	{
	case 0://P1
		   //水色
		SetCol(D3DXCOLOR(0.7f, 1.0f, 1.0f, 1.0f));
		break;
	case 1://P2
		   //あか
		SetCol(D3DXCOLOR(1.0f, 0.4f, 0.4f, 1.0f));
		break;
	case 2://P3
		   //黄色
		SetCol(D3DXCOLOR(1.0f, 1.0f, 0.4f, 1.0f));
		break;
	case 3://P4
		   //緑
		SetCol(D3DXCOLOR(0.78f, 1.0f, 0.7f, 1.0f));
		break;
	case 99://NPC
		SetCol(D3DXCOLOR(0.0f, 0.0f, 0.0f, 1.0f));
		break;
	default:
		break;
	}
}
