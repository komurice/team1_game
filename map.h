//============================
//
// マップチップヘッター
// Author:hamada ryuuga
//
//============================
#ifndef _MAP_H_
#define _MAP_H_

//**************************************************
// インクルード
//**************************************************
#include "main.h"
#include "texture.h"
#include "object.h"
#include "block.h"
#include "enemy.h"
#include "item.h"

//**************************************************
// 前方宣言
//**************************************************
class CMapBlack;
class CSignBoard;
namespace nl = nlohmann;

//**************************************************
// クラス
//**************************************************
class CMap : public CObject
{
public:
	const float BLOCKMOVE = 5.0f;
	enum BLOCKTYPE
	{
		BLOCKTYPE_NORMAL = 0,
		BLOCKTYPE_GOAL,
		BLOCKTYPE_ITEM,
		BLOCKTYPE_LEFT,
		BLOCKTYPE_RIGHT,
		BLOCKTYPE_SPAWN,
		ENEMY_SAI,
		ENEMY_FOX,
		ENEMY_ARAIGUMA,
		ENEMY_KYOURYU,
		BLOCKTYPE_BROKEN,
		BLOCKTYPE_BOARD,
		BLOCKTYPE_BUSH,
		BLOCKTYPE_ITEM_RANGE,
		BLOCKTYPE_ITEM_SHOTGUN,
		BLOCKTYPE_ITEM_STUN,
		BLOCKTYPE_ITEM_METAL,
		BLOCKTYPE_ITEM_SONICSHOT,
		BLOCKTYPE_MAX
	};

	enum ENEMYTYPE
	{
		ENEMY = 0,		// 通常
		CHASEENEMY,		// Playerを追ってくるやつ
		BOSS,		// BOSS
		MAXENEMY			//あんただれや？
	};
	enum MODELTYPE
	{
		NORMAL = 0,		// 通常
		PENDULUM,		// 振り子
		WOOD,			// 木
		BALL,			// ボール
		RAIL,			// 乗ったら動く床
		CHANGE,			// 乗ったら動く床
		GOAL,			// ゴール
		DASHITEM,			// dashitem
		BANE,			// bame
		BELL,			// 得点アイテム
		CAMERA,			// CAMERA
		MAXMODEL				//あんただれや？
	};
	static const int MaxX = 30;
	static const int MaxY = 20;

	struct EnemySpawnData
	{
		D3DXVECTOR3 pos;
		CEnemy::ENEMY_TYPE type;
	};


	const float BLOCKSIZE = 50.0f;
	explicit CMap(int nPriority = PRIORITY_OBJECT);
	~CMap() override;

	virtual HRESULT Init() override;
	virtual void Uninit() override;
	virtual void Update() override;
	virtual void Draw(DRAW_MODE drawMode) override;
	static CMap* CMap::Create();
	void modelset(const D3DXVECTOR3 pos);

	//	blockポイントを追加
	void Addblock(const CBlock* block) { m_block.push_back((CBlock*)block); }
	//	blockポイントをセット
	void Setblock(const int IsPoptime, const CBlock* block) { m_block.at(IsPoptime) = (CBlock*)block; }
	//	blockポイントをゲット
	CBlock* Getblock(const int IsPoptime) { return m_block.at(IsPoptime); }
	//	blockポイントのさいずの取得
	int GetblockSize() { return m_block.size(); }

	float GetLog() { return m_Log; }

	void Save();
	void Load(const std::string Pass);
	bool blockHit(const D3DXVECTOR3 pos, const D3DXVECTOR3 Size, CPlayer* player);
	bool itemHit(const D3DXVECTOR3 pos, const D3DXVECTOR3 Size, CPlayer* player);
	bool EnemyHit(const D3DXVECTOR3 pos, const D3DXVECTOR3 Size, CPlayer* player, bool isHit);

	int GetNowMAX() { return m_NowMAX; }
	void SetNowMAX(int nNumber) { m_NowMAX = nNumber; }

	int GetPlayerMAX() { return m_PlayerMAX; }

	void SetFast(int nNumber) { m_PlayerMAX = nNumber; m_NowMAX = nNumber; }


	void UpdateTime();
	void UpdateItem(float elapsedTime);
	void UpdateEnemy(float elapsedTime);
	void ResetItem();
	void SetList(CEnemy* enemy);
	void LoadfileEnemy(const char* pFileName);
	void Loadfile(const char* pFileName);
private:
	int m_PlayerMAX;
	int m_NowMAX;
	std::vector<CBlock*> m_block;
	std::map<int, CItem*> m_item;
	std::vector<CEnemy*> m_enemy;
	std::vector<D3DXVECTOR3> m_itemSpawnPos;
	std::vector<EnemySpawnData> m_enemySpawn;
	nl::json m_Jsonblock;//リストの生成
	float m_Log;

	float m_time;
	float m_itemSpawnTimer;
	float m_itemTimeCount;
	float m_enemySpawnTimer;
	float m_enemyTimeCount;
	bool m_enemyPopSet;
	int m_rondomSpawn;
	int GetRandom(int min, int max) const;
};



#endif	// _OBJECT2D_H_
