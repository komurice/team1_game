//==================================================
// エフェクト
// Author: HAMADA
//==================================================

//**************************************************
// include
//**************************************************
#include "risk.h"
#include "player.h"
#include "block.h"
#include "debug_proc.h"
#include "effect.h"
#include "particle.h"
#include "manager.h"
#include "sound.h"

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CRisk::CRisk(int nPriority) : CObject3D(nPriority)
{
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CRisk::~CRisk()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CRisk::Init()
{
	CObject3D::Init();
	SetBillboard(true);
	SetTexture(CTexture::TEXTURE_KIKEN);
	CManager::GetManager()->GetSound()->Play(CSound::LABEL_SE_WARNING);			//サウンド
	m_timer = 0.5f;
	m_life = 50;
	m_up = false;
	return S_OK;
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CRisk::Update()
{
	CObject3D::Update();
	float color = 0.01f;
	if (m_up)
	{
		m_timer += color;
	}
	else
	{
		m_timer -= color;
	}
	if (m_timer >= 1.0f)
	{
		m_up = false;
	}
	if (m_timer <= 0.5f)
	{
		m_up = true;
	}

	SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, m_timer));

	m_life--;
	if (m_life <= 0)
	{
		Uninit();
	}
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CRisk* CRisk::Create(const D3DXVECTOR3 pos, const D3DXVECTOR3 size)
{
	CRisk* pBullet;
	pBullet = new CRisk(PRIORITY_OBJECT);

	if (pBullet != nullptr)
	{
		pBullet->Init();
		pBullet->SetPos(pos);
		pBullet->SetSize(size);

	}
	else
	{
		assert(false);
	}

	return pBullet;
}
