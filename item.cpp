//==================================================
// item.cpp
// Author: Buriya Kota
//==================================================

//**************************************************
// include
//**************************************************
#include "item.h"

#include "model_data.h"
#include "fade.h"
#include "manager.h"
#include "player.h"
#include "block.h"
#include "stage_ui.h"
#include "sound.h"

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CItem::CItem(int nPriority) : CBlock(nPriority)
{
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CItem::~CItem()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CItem::Init()
{
	CBlock::Init();

	CBlock::SetCollisio(true);

	m_isRemove = false;
	m_rotationSpeed = 0.1f;     // 回転速度
	m_amplitude = 0.3f;         // 振幅
	m_frequency = 1.0f;         // 周波 
	m_phaseOffset = 0.0f;       // 位相オフセッ 
	m_fixedDelta = 0.05f;
	m_elapsed = 0.f;

	switch (m_itemType)
	{
	case ITEMTYPE_RANGE:
		CObjectX::SetModelData(CModelData::MODEL_ITEM_RANGE);
		break;
	case ITEMTYPE_SHOTGUN:
		CObjectX::SetModelData(CModelData::MODEL_ITEM_SHOTGUN);
		break;
	case ITEMTYPE_METAL:
		CObjectX::SetModelData(CModelData::MODEL_ITEM_METAL);
		break;
	case ITEMTYPE_SONICSHOT:
		CObjectX::SetModelData(CModelData::MODEL_ITEM_SONICSHOT);
		break;
	}

	return S_OK;
}

//--------------------------------------------------
// 終了
//--------------------------------------------------
void CItem::Uninit()
{
	CBlock::Uninit();
}


//--------------------------------------------------
// 更新
//--------------------------------------------------
void CItem::Update()
{
	CBlock::Update();

	UpdateMovementState();
}

//--------------------------------------------------
// 描画
//--------------------------------------------------
void CItem::Draw(DRAW_MODE drawMode)
{
	CBlock::Draw(drawMode);
}

void CItem::UpdateMovementState()
{
	const float initialPosY = m_pos.y;

	m_rot.y += m_rotationSpeed * m_fixedDelta;  // 回転速度に基づいて Y 軸の回転角度を更新する

	m_rot = RotNormalization(m_rot);  // 回転角度を正規化する

	m_elapsed += m_fixedDelta;  // 固定の時間間隔を加算する

	// 現在の角度の正規化
	if (m_elapsed > D3DX_PI)
	{
		m_elapsed -= D3DX_PI * 2.0f;
	}
	else if (m_elapsed < -D3DX_PI)
	{
		m_elapsed += D3DX_PI * 2.0f;
	}

	const float phase = m_phaseOffset + m_frequency * m_elapsed;  // 位相オフセットと周波数に基づいて位相を計算する

	const float verticalOffset = m_amplitude * sin(phase);  // 振幅と位相に基づいて垂直方向のオフセットを計算する

	m_pos.y = initialPosY + verticalOffset;  // Y 座標位置を更新する（垂直方向のオフセットを加算する）
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CItem* CItem::Create(D3DXVECTOR3 pos, ITEMTYPE type)
{
	CItem* pItem;
	pItem = new CItem;

	if (pItem != nullptr)
	{
		pItem->SetItemType(type);
		pItem->Init();
		pItem->SetPos(pos);
		pItem->SetPosOriginPos(pos);
	}
	else
	{
		assert(false);
	}
	return pItem;
}


//=============================================================================
// ヒット
//=============================================================================
bool CItem::hitevent(CPlayer* player)
{

	CModelData* Model = CManager::GetManager()->GetModelData();

	D3DXVECTOR3 Size = Model->GetModel(GetModelData()).size;
	D3DXVECTOR3 Pos = GetPos();
	D3DXVECTOR3 PlayerSize = D3DXVECTOR3(20.0f, 20.0f, 50.0f);
	D3DXVECTOR3 PlayerPos = player->GetPos();
	D3DXVECTOR3 PlayerPosOld = player->GetPosOld();

	bool IsHit = false;

	if (((Pos.y - Size.y) <= (PlayerPos.y + PlayerSize.y)) &&
		((Pos.y + Size.y) >= (PlayerPos.y - PlayerSize.y)) &&
		((Pos.x - Size.x) <= (PlayerPos.x + PlayerSize.x)) &&
		((Pos.x + Size.x) >= (PlayerPos.x - PlayerSize.x)))
	{
		CManager::GetManager()->GetSound()->Play(CSound::LABEL_SE_ITEM);	//アイテム取得音
		switch (m_itemType)
		{
		case CItem::ITEMTYPE_RANGE:
			player->GetUi()->SetType(CPlayer::BULLET_TYPE::RANGE_EXTENSION);
			player->SetBulletType(CPlayer::BULLET_TYPE::RANGE_EXTENSION);
			player->ShotBoost(300);
			break;

		case CItem::ITEMTYPE_SHOTGUN:
			player->GetUi()->SetType(CPlayer::BULLET_TYPE::SHOTGUN);
			player->SetBulletType(CPlayer::BULLET_TYPE::SHOTGUN);
			player->ShotBoost(300);
			break;

		case CItem::ITEMTYPE_STUN:
			player->GetUi()->SetType(CPlayer::BULLET_TYPE::STUN_BULLET);
			//ステータスブースト
			player->Boost(180, 1.33f);

			break;

		case CItem::ITEMTYPE_METAL:
			player->GetUi()->SetType(CPlayer::BULLET_TYPE::DEFAULT);
			break;

		case CItem::ITEMTYPE_SONICSHOT:
			player->GetUi()->SetType(CPlayer::BULLET_TYPE::DEFAULT);
			break;

		default:
			break;
		}
		m_isRemove = true;
		IsHit = true;
	}
	return IsHit;
}