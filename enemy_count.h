//==================================================
// time.h
// Author: hamada?
//==================================================
#ifndef _ENEMY_COUNT_H_
#define _ENEMY_COUNT_H_

//**************************************************
// インクルード
//**************************************************
#include "object.h"

//**************************************************
// 前方前言　実態はNG　ポインタだけならOK
//**************************************************
class CNumber;

//**************************************************
// クラス
//**************************************************
class CEnemyCount : public CObject
{
public:
	static const int NUM_TIME = 3;
public:
	explicit CEnemyCount(int nPriority = PRIORITY_UI2D);
	~CEnemyCount() override;

	HRESULT Init() override;
	HRESULT Init(D3DXVECTOR3 pos, D3DXVECTOR3 size);
	void Uninit() override;
	void Update() override;
	void Draw(DRAW_MODE /*drawMode*/) override {}

	void SetPos(D3DXVECTOR3 pos, D3DXVECTOR3 size);

	void SetTimer(int nTime);
	void SubTimer(int nValue) { SetTimer(m_nTime - nValue); }
	void StopTimer(bool stop) { m_bIsStop = stop; }
	int GetTimer() { return m_nTime; }

	void SetEnemy(int Enemy) { m_popEnemy = Enemy; };
	int GetEnemy() {return m_popEnemy; };
	static CEnemyCount *Create(D3DXVECTOR3 pos, D3DXVECTOR3 size);
	void SudEnemy();
private:
	// Number型の配列
	CNumber *m_pNumber[NUM_TIME];
	// 時間
	int m_nTime;
	// フレームカウント
	int m_nCounter;
	// タイマーを止める
	bool m_bIsStop;
	// フレーム
	int m_nFrame;
	// フレーム
	int m_nSceneFrame;
	// 今画面に出てる敵の数
	int m_popEnemy;

	int m_time;

	int m_popMax;

};

#endif // _TIMER_H_
