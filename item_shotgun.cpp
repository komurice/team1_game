﻿//==================================================
// item_shotgun.h
// Author: Huang Qiyue
//==================================================

//**************************************************
// include
//**************************************************
#include "item_shotgun.h"

#include "model_data.h"
#include "fade.h"
#include "game.h"
#include "manager.h"
#include "player.h"
#include "stage_ui.h"

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CItemShotgun::CItemShotgun(int nPriority) : CItem(nPriority)
{
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CItemShotgun::~CItemShotgun()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CItemShotgun::Init()
{

	CObjectX::SetModelData(CModelData::MODEL_ITEM_SHOTGUN);
	CItem::Init();

	return S_OK;
}

//--------------------------------------------------
// 終了
//--------------------------------------------------
void CItemShotgun::Uninit()
{
	CItem::Uninit();
}


//--------------------------------------------------
// 更新
//--------------------------------------------------
void CItemShotgun::Update()
{
	CItem::Update();
}

//--------------------------------------------------
// 描画
//--------------------------------------------------
void CItemShotgun::Draw(DRAW_MODE drawMode)
{
	CItem::Draw(drawMode);
	
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CItemShotgun* CItemShotgun::Create(D3DXVECTOR3 pos, int Model)
{
	CItemShotgun* pPower = new CItemShotgun;

	if (pPower != nullptr)
	{
		pPower->Init();
		pPower->SetPos(pos);
		pPower->SetPosOriginPos(pos);
		pPower->SetModelType(Model);
	}
	else
	{
		assert(false);
	}
	return pPower;
}


//=============================================================================
// ヒット
//=============================================================================
bool CItemShotgun::hitevent(CPlayer* player)
{

	CModelData* Model = CManager::GetManager()->GetModelData();

	D3DXVECTOR3 Size = Model->GetModel(GetModelData()).size;
	D3DXVECTOR3 Pos = GetPos();
	D3DXVECTOR3 PlayerSize = D3DXVECTOR3(20.0f, 20.0f, 50.0f);
	D3DXVECTOR3 PlayerPos = player->GetPos();
	D3DXVECTOR3 PlayerPosOld = player->GetPosOld();

	bool IsHit = false;

	if (((Pos.y - Size.y) <= (PlayerPos.y + PlayerSize.y)) &&
		((Pos.y + Size.y) >= (PlayerPos.y - PlayerSize.y)) &&
		((Pos.x - Size.x) <= (PlayerPos.x + PlayerSize.x)) &&
		((Pos.x + Size.x) >= (PlayerPos.x - PlayerSize.x)))
	{
		CItemShotgun::Uninit();
		player->GetUi()->SetType(CPlayer::BULLET_TYPE::SHOTGUN);
		player->SetBulletType(CPlayer::BULLET_TYPE::SHOTGUN);
		player->ShotBoost(300);
		IsHit = true;
	}
	return IsHit;
}
