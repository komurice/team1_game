//==================================================
// result.cpp
// Author: Buriya Kota
//==================================================

//**************************************************
// include
//**************************************************
#include <assert.h>

#include "manager.h"
#include "input.h"

#include "object2D.h"
#include "result.h"
#include "fade.h"
#include "sound.h"
#include "player.h"
#include "utility.h"
#include "camera.h"
#include "object3D.h"


CResult* CResult::m_pResult = nullptr;

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CResult::CResult()
{
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CResult::~CResult()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CResult::Init()
{
	m_pObject2D[0] = CObject2D::Create(
		D3DXVECTOR3(CManager::SCREEN_WIDTH * 0.5f, CManager::SCREEN_HEIGHT * 0.5f, 0.0f),
		D3DXVECTOR3((float)CManager::SCREEN_WIDTH, (float)CManager::SCREEN_HEIGHT, 0.0f),
		PRIORITY_BG);
	m_pObject2D[0]->SetTexture(CTexture::TEXTURE_RESULT_BG);


	m_pObject2D[1] = CObject2D::Create(
		D3DXVECTOR3(CManager::SCREEN_WIDTH * 0.25f, CManager::SCREEN_HEIGHT * 0.25f, 0.0f),
		D3DXVECTOR3(CManager::SCREEN_WIDTH * 0.25f, CManager::SCREEN_HEIGHT * 0.25f, 0.0f),
		PRIORITY_BG);
	m_pObject2D[1]->SetTexture(CTexture::TEXTURE_RESULT);

	int nPlayerNumber = CManager::GetManager()->GetPlayerNumber();

	float Pos = (float)CManager::SCREEN_WIDTH / (nPlayerNumber);


	for (int nCnt = 0; nCnt < nPlayerNumber; nCnt++)
	{
		m_pObject[nCnt] = CObject3D::Create(D3DXVECTOR3(0.0f, 0.0f, 100.0f),
			D3DXVECTOR3(50.0f, 50.0f, 0.0f), PRIORITY_BULLET);

		switch (nCnt)
		{
		case 0:
			m_pObject[nCnt]->SetTexture(CTexture::TEXTURE_RANKING_NUMBER_1);
			break;
		case 1:
			m_pObject[nCnt]->SetTexture(CTexture::TEXTURE_RANKING_NUMBER_2);
			break;
		case 2:
			m_pObject[nCnt]->SetTexture(CTexture::TEXTURE_RANKING_NUMBER_3);
			break;
		case 3:
			m_pObject[nCnt]->SetTexture(CTexture::TEXTURE_RANKING_NUMBER_4);
			break;
		default:
			break;
		}
		CPlayer* pPlayer = CPlayer::Create(D3DXVECTOR3(0.0f + nCnt * 100.0f, 200.0f, 0.0f), nCnt, CManager::GetManager()->GetList(nCnt));
		m_pPlayer.push_back(pPlayer);

		m_enemy.push_back(CEnemy::Create(D3DXVECTOR3(-500.f, -300.f, 0.f), CEnemy::ENEMY_FOX));
		m_enemy.push_back(CEnemy::Create(D3DXVECTOR3(500.f, -300.f, 0.f), CEnemy::ENEMY_FOX));
	}


	
	
	

	m_block.push_back(CBlock::Create(D3DXVECTOR3(0.f, -300.f, 0.f), CModelData::MODEL_CAKE, CBlock::BLOCKTYPE_PODIUM1));

	int Num = 0;
	for (int i = CManager::GetManager()->GetRankingSize()-1; i >= 0; i--)
	{

		float posX = 0.0f;

		switch (Num)
		{
		case 1:
			posX = -100.0f;
			break;
		case 2:
			posX = 200.0f;
			break;
		case 3:
			posX = 300.0f;
			break;
		default:
			break;
		}
		D3DXVECTOR3 screenPos = D3DXVECTOR3((Pos * i) + (Pos / 2), (float)CManager::SCREEN_HEIGHT + 50, 0.0f);

		D3DXVECTOR3 worldPos = br::WorldCastScreen(&screenPos,			// スクリーン座標
			D3DXVECTOR3((float)CManager::SCREEN_WIDTH, (float)CManager::SCREEN_HEIGHT, 0.0f),									// スクリーンサイズ
			CManager::GetManager()->GetCamera()->GetViewMatrix(),									// ビューマトリックス
			CManager::GetManager()->GetCamera()->GetProjMatrix());								// プロジェクションマトリックス


		D3DXVECTOR3 mixPos = D3DXVECTOR3(worldPos.x, screenPos.y, 0.0f);
		D3DXVECTOR3	pos;
		m_pObject[i]->SetPos((D3DXVECTOR3(posX, screenPos.y, 0.0f)));
		switch ((CManager::PLAYERID)CManager::GetManager()->GetRanking(i))
		{
		case CManager::PLAYER000:
			m_pPlayer[0]->SetPos(D3DXVECTOR3(posX, screenPos.y, 0.0f));
			break;
		case CManager::PLAYER001:
			m_pPlayer[1]->SetPos(D3DXVECTOR3(posX, screenPos.y, 0.0f));
			break;
		case CManager::PLAYER002:
			m_pPlayer[2]->SetPos(D3DXVECTOR3(posX, screenPos.y, 0.0f));
			break;
		case CManager::PLAYER003:
			m_pPlayer[3]->SetPos(D3DXVECTOR3(posX, screenPos.y, 0.0f));
			break;
		default:
			break;
		}

		m_block.push_back(CBlock::Create(D3DXVECTOR3(0.f, 230.f, 0.0f), CModelData::MODEL_CLEENNESS_OBJECT, CBlock::BLOCKTYPE_CLEENNESS));
		m_block.push_back(CBlock::Create(D3DXVECTOR3(-100.f, 90.f, 0.0f), CModelData::MODEL_CLEENNESS_OBJECT, CBlock::BLOCKTYPE_CLEENNESS));
		m_block.push_back(CBlock::Create(D3DXVECTOR3(200.f, -70.f, 0.0f), CModelData::MODEL_CLEENNESS_OBJECT, CBlock::BLOCKTYPE_CLEENNESS));
		m_block.push_back(CBlock::Create(D3DXVECTOR3(300.f, -300.f, 0.0f), CModelData::MODEL_CLEENNESS_OBJECT, CBlock::BLOCKTYPE_CLEENNESS));
		Num++;
	}


	//for (int nCnt = 0; nCnt < nPlayerNumber; nCnt++)
	//{
	//	D3DXVECTOR3 screenPos = D3DXVECTOR3((Pos * nCnt) + (Pos / 2.0f), (0.0f - (100.f * nPlayerNumber)) + nCnt * 200.0f, 0.0f);

	//	D3DXVECTOR3 worldPos = br::WorldCastScreen(&screenPos,			// スクリーン座標
	//		D3DXVECTOR3((float)CManager::SCREEN_WIDTH, (float)CManager::SCREEN_HEIGHT, 0.0f),									// スクリーンサイズ
	//		CManager::GetManager()->GetCamera()->GetViewMatrix(),									// ビューマトリックス
	//		CManager::GetManager()->GetCamera()->GetProjMatrix());								// プロジェクションマトリックス

	//	D3DXVECTOR3 mixPos = D3DXVECTOR3(worldPos.x, m_pPlayer[nCnt]->GetPos().y, 0.0f);
	//	m_pPlayer[nCnt]->SetPos(mixPos);

	//}

	CManager::GetManager()->GetSound()->Play(CSound::LABEL_BGM_RESULT);

	return S_OK;
}

//--------------------------------------------------
// 終了
//--------------------------------------------------
void CResult::Uninit()
{
	m_pPlayer.clear();
	CObject::DeletedObj();
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CResult::Update()
{
	CInput* pInput = CInput::GetKey();

	int List = 0;
	for (int i = CManager::GetManager()->GetRankingSize() - 1; i >= 0; i--)
	{
		D3DXVECTOR3 Pos = m_pPlayer[CManager::GetManager()->GetRanking(i)]->GetPos();
		Pos.z += 10.0f*List;
		Pos.y += 50.0f;
		m_pObject[List]->SetPos(Pos);
		List++;
	}

	if (pInput->Trigger(KEY_DECISION))
	{
		// サウンド
		CManager::GetManager()->GetSound()->Play(CSound::LABEL_SE_ENTER2);

		CFade::GetInstance()->SetFade(CManager::MODE_TITLE);
	}
}

bool CResult::blockHit(const D3DXVECTOR3 pos, const D3DXVECTOR3 Size, CPlayer* player)
{
	const D3DXVECTOR3& screenSize = br::GetScreenSize();
	bool isHit = false;

	for (auto& block : m_block)
	{
		D3DXVECTOR3 blockPos = block->GetPos();
		D3DXVECTOR3 blockSize = block->GetSize();

		if (((blockPos.y - blockSize.y) <= (screenSize.y)) &&
			((blockPos.y + blockSize.y) >= (-screenSize.y)) &&
			((blockPos.x - blockSize.x) <= (screenSize.x)) &&
			((blockPos.x + blockSize.x) >= (-screenSize.x)))
		{
			if (block->Collision(pos, Size, player))
				isHit = true;
		}
	}

	return isHit;
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CResult* CResult::Create()
{
	m_pResult = new CResult;

	if (m_pResult != nullptr)
	{
		m_pResult->Init();
	}
	else
	{
		assert(false);
	}

	return m_pResult;
}
