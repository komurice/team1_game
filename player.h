//==================================================
// player3D.h
// Author: Buriya Kota
//==================================================
#ifndef _PLAYER3D_H_
#define _PLAYER3D_H_

//**************************************************
// インクルード
//**************************************************
#include "object.h"
#include "motion_model3D.h"
#include "bullet.h"

//**************************************************
// 前方前言　実態はNG　ポインタだけならOK
//**************************************************
class CLockOnCursor;
class CModel;
class CBlock;
class CStage_Ui;
class CObject3D;
class CStunGauge;
class CStunEffect;

//**************************************************
// 定数定義
//**************************************************
#define MAX_PARTS		(13)
#define MAX_KEYDATA		(2)
#define MAX_MOTION		(5)

//**************************************************
// 構造体定義
//**************************************************

//**************************************************
// クラス
//**************************************************
class CPlayer : public CMotionModel3D
{
public:
	static const int MAX_PLAYER = 4;
	static const int MAX_STUN = 9;
	static const int STUN_FRAME = 60;
	static const int STUN_RECOVERY_TIME = 1000;

public:
	struct KEY
	{
		D3DXVECTOR3 pos;
		D3DXVECTOR3 rot;
	};

	struct KEY_SET
	{// std::vector map を勉強したら動的に配列の値を変えれる
		int nFrame;
		KEY aKey[MAX_PARTS];
	};

	//modelデータの構造体//
	struct MODELDATA
	{
		int  LOOP;		// ループするかどうか[0:ループしない / 1 : ループする]
		int NUM_KEY;  	// キー数
		KEY_SET KeySet[MAX_KEYDATA];
	};

	enum BULLET_TYPE
	{
		DEFAULT = 0,
		RANGE_EXTENSION,
		SHOTGUN,
		STUN_BULLET,
		MAX_BULLET
	};

public:
	static const float PLAYER_SPEED;

	enum ACTION_TYPE
	{
		TYPE_NEUTRAL = 0,		// ニュートラル
		TYPE_MOVE,				// 移動
		TYPE_ATTACK,			// 攻撃
		TYPE_JUMP,				// JUMP
		TYPE_JUMPOUT,			// 着地
		TYPE_MAX,				// 最大数
	};

	enum PLAYER_STATE
	{
		PLAYER_NONE = 0,
		PLAYER_GROUND,
		PLAYER_SKY,
		PLAYER_STATE_MAX
	};

private:
	enum EState
	{
		STATE_IDLE = 0,
		STATE_LAND,
		STATE_FLY,
		STATE_MAX,
		STATE_INVALID = -1,
	};

private:
	static const UPDATE_FUNC mUpdateFunc[];

public:
	explicit CPlayer(int nPriority = PRIORITY_PLAYER);
	~CPlayer();

	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw(DRAW_MODE drawMode) override;

	//　プレイヤーのステート関数
	void Update_Idle();
	void Update_Land();
	void Update_Fly();

	static CPlayer* Create(D3DXVECTOR3 pos, int number, int playerNumber);

	// セッター
	void SetPos(const D3DXVECTOR3& pos) { m_pos = pos; CMotionModel3D::SetPos(pos); }
	void SetPosOld(const D3DXVECTOR3& posOld) { m_posOld = posOld; }
	void SetMove(const D3DXVECTOR3& move) { m_move = move; }
	void SetRot(const D3DXVECTOR3& rot) { m_rot = rot;   CMotionModel3D::SetRot(rot); }
	void SetRotDest(const D3DXVECTOR3& rotDest) { m_rotDest = rotDest; }
	void SetSize(const D3DXVECTOR3& size) { m_size = size; }
	void SetWorldMtx(const D3DXMATRIX& mtx) { m_mtx = mtx; }
	void SetPlayerNumber(const int& playerNumber) { m_nPlayerNumber = playerNumber; }
	void SetBulletType(BULLET_TYPE type);
	void SetKnockBack(D3DXVECTOR3 bulletMove, bool isHit) { m_bKnockBack = isHit; m_bulletMove = bulletMove; }
	void AddStun(int nStunNum) { m_nStun += nStunNum; }
	void SetKnock(bool isHit) { m_bKnockBack = isHit; }
	void IsAttack(bool isAttack) { m_isBulletAttack = isAttack; }

	// ゲッター
	const D3DXVECTOR3& GetPos() const { return m_pos; }
	const D3DXVECTOR3& GetPosOld() const { return m_posOld; }
	const D3DXVECTOR3& GetMove() const { return m_move; }
	const D3DXVECTOR3& GetRot() const { return m_rot; }
	const D3DXVECTOR3& GetRotDest() const { return m_rotDest; }
	const D3DXVECTOR3& GetSize() const { return m_size; }
	const int& GetPlayerNumber() const { return m_nPlayerNumber; }
	const bool GetKnockBack() { return m_bKnockBack; };
	const D3DXVECTOR3& GetbulletMove() const { return m_bulletMove; }
	const int& GetStunNum() { return m_nStun; }
	const bool GetIsStun() { return m_IsStun; };

	// 角度の正規化
	D3DXVECTOR3 RotNormalization(D3DXVECTOR3 rot);
	D3DXVECTOR3 RotDestNormalization(D3DXVECTOR3 rot, D3DXVECTOR3 rotDest);

	// 移動量代入
	void MovePos(const D3DXVECTOR3& move) { m_pos += move; }

	bool CPlayer::Collision(const D3DXVECTOR3 PlayerPos, const D3DXVECTOR3 PlayerSize, CBlock* Block);
	bool CPlayer::hitevent(CBlock* Block);

	void Outarea();

	CStage_Ui* GetUi() { return m_Ui; }
	void Boost(int Time, float boost);
	void ShotBoost(int Time);

	void SetIsHitSignBoard(bool inHit) { m_isHitSignBoard = inHit; }
	bool GetIsHitSignBoard() { return m_isHitSignBoard; }

private:
	void Control_();
	void Motion_();
	void Recall_();
	void KnockBack_();
	void Jump_();
	void Stun_();

	void Shoot(const D3DXVECTOR3& pos);
	void SetShootType(BULLET_TYPE List);
	void Attack();

protected:
	// モデルの情報
	CModel* m_pModel[MAX_PARTS];
	D3DXMATRIX m_mtx;
	D3DXVECTOR3 m_pos;
	D3DXVECTOR3 m_posOld;
	D3DXVECTOR3 m_move;
	D3DXVECTOR3 m_size;
	D3DXVECTOR3 m_rot;
	D3DXVECTOR3 m_rotDest;
	CStage_Ui* m_Ui;
	// キーの総数
	int m_nNumKey;
	// 現在のキー
	int m_nCurrentKey;
	// モーションカウンター
	int m_nCountMotion;
	//
	MODELDATA m_ModelData[MAX_MOTION];
	//
	int m_nSetModel;
	int m_nSetCurrentMotion;

	// リコール
	std::vector <D3DXVECTOR3> m_playerRecallPos;
	int m_nCountRecall;
	int m_nVectorSize;
	bool m_bRecall;
	bool m_bMaxSize;

	// プレイヤー番号
	int m_nPlayerNumber;
	// プレイヤーの状態
	PLAYER_STATE m_playerState;
	ACTION_TYPE m_action;

	BULLET_TYPE m_bulletType;
	int m_remainingBullets;

	// 弾の間隔
	int m_nBulletTime;
	int m_nChargeTime;
	bool m_bIsCharge;
	CObject3D* m_pArrow;

	// 当たった時
	D3DXVECTOR3 m_bulletMove;
	bool m_bKnockBack;
	int m_nKnockBackTime;
	float m_fKnockBackFrame;
	float m_fKnockBackPowerX;
	float m_fKnockBackPowerY;

	int m_MoveboostTime;
	float m_Moveboost;

	// スタン
	int m_nStun;
	int m_nStunTime;
	bool m_IsStun;
	D3DXVECTOR3 m_Speed;
	CStunGauge* m_pStunGauge;
	int m_nStanRecoveryTime;
	CStunEffect* m_Stun;

	bool m_IsclearStun;
	// ステート
	EState m_state;

	// 入力がマウスかパッドか
	bool m_isMouse;


	int m_timer;

	int m_Shottimer;

	int m_Diff;

	D3DXVECTOR3 m_vec;
	bool m_isBulletAttack;

	bool m_IsAnime;

	bool m_IsInSky;

	bool m_isHitSignBoard;
};

#endif	// _PLAYER3D_H_