//============================
//
// 演出
// Author:hamada ryuuga
//
//============================
#ifndef _PARTICLE_H_
#define _PARTICLE_H_

//**************************************************
// インクルード
//**************************************************
#include "object3D.h"

//**************************************************
// クラス
//**************************************************
class CParticle : public CObject3D
{
public:
	enum MOVE
	{// 状態
		MOVE_DEF = 0,
		MOVE_DAMAGE,
		MOVE_HANABI,
		MOVE_ITEM,
		MOVE_OTO,
		MOVE_MAX
	};
	const float Timer = 0.001f;

	explicit CParticle(int nPriority = PRIORITY_BG);
	~CParticle();

	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw(DRAW_MODE drawMode) override;

	void SetMove(D3DXVECTOR3 Move) { m_Move = Move; }
	void SetLife(int Life) { m_Life = Life; }
	void SetMode(MOVE Mode) { m_Mode = Mode; };
	void SetplayerNumber(int playerNumber) { m_playerNumber = playerNumber; }
	static CParticle* CParticle::Create(D3DXVECTOR3 pos, D3DXVECTOR3 move, int Life, MOVE mode, int nPriority);
	void SetItem(CTexture::TEXTURE tex);
	D3DXCOLOR SetPlayerNumColor(int Is);

private:
	CObject3D* m_Main;
	float m_blender;
	int m_Noudraw;
	int m_Nextdraw;
	int m_Life;
	D3DXVECTOR3 m_Move;
	MOVE m_Mode;
	int m_playerNumber;
};

#endif

