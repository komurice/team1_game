//==================================================
// tutorial.cpp
// Author: Buriya Kota
//==================================================

//**************************************************
// include
//**************************************************
#include "manager.h"

#include "input.h"
#include "sound.h"
#include "text.h"
#include "pause.h"

#include "tutorial.h"
#include "fade.h"

#include "object2D.h"
#include "player.h"
#include "map.h"
#include "enemy_count.h"
#include "bg.h"

static const float MOVE_TIME = 60.0f;
static const float CHOICE_ICON_HEIGHT = 55.0f;

//**************************************************
// 静的メンバ変数
//**************************************************
CTutorial *CTutorial::m_pTutorial = nullptr;

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CTutorial::CTutorial()
{
	m_pPlayer.clear();
	m_pPause = nullptr;
	m_pMap = nullptr;
	m_pBg = nullptr;
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CTutorial::~CTutorial()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CTutorial::Init()
{
	m_pMap = CMap::Create();
	m_pMap->Load("data/MAP/STAGE_TUTORIAL.json");

	m_pBg = CBg::Create(D3DXVECTOR3(CManager::SCREEN_WIDTH * 0.5f, 50.0f, 0.0f), D3DXVECTOR3(50.0f, 100.0f, 0.0f), PRIORITY_BG);
	m_pBg->BlenderTex(CTexture::TEXTURE_BG1, CTexture::TEXTURE_BG2, CTexture::TEXTURE_BG3);

	m_pPause = CPause::Create();

	//CText::Create(D3DXVECTOR3(0.0f, 100.0f, 0.0f),
	//	D3DXVECTOR3(0.0f, 0.0f, 0.0f), 
	//	CText::MAX, 1000, 10, 
	//	"あるふぁーゲーム");

	int nPlayerNumber = CManager::GetManager()->GetPlayerNumber();

	for (int nCnt = 0; nCnt < nPlayerNumber; nCnt++)
	{
		CPlayer* pPlayer = CPlayer::Create(D3DXVECTOR3(0.0f + nCnt * 100.0f, 500.0f, 0.0f), nCnt, nCnt);
		m_pPlayer.push_back(pPlayer);
	}

	m_popEnemy = 0;

	m_enemy = CEnemyCount::Create(
		D3DXVECTOR3(CManager::SCREEN_WIDTH * 0.5f-200.0f, 100.0f, 0.0f),
		D3DXVECTOR3(25.0f, 50.0f, 0.0f));
	m_enemy->SetTimer(10);

	m_pMap->SetFast(nPlayerNumber);

	CManager::GetManager()->GetSound()->Play(CSound::LABEL_BGM_TUTORIAL);

	return S_OK;
}

//--------------------------------------------------
// 終了
//--------------------------------------------------
void CTutorial::Uninit()
{
	m_pTutorial = nullptr;
	m_pPause = nullptr;

	for (int i = 0; i < TEXTURE_MAX; i++)
	{
		m_pObject2D[i] = nullptr;
	}

	for (int i = 0; i < 2; i++)
	{
		m_pChoice[i] = nullptr;
	}

	m_pMap = nullptr;
	m_pBg = nullptr;
	m_pPlayer.clear();

	CObject::DeletedObj();
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CTutorial::Update()
{
	//CInput *pInput = CInput::GetKey();

	//if (pInput->Trigger(KEY_DECISION))
	//{
	//	// サウンド
	//	CManager::GetManager()->GetSound()->Play(CSound::LABEL_SE_ENTER);

	//	CFade::GetInstance()->SetFade(CManager::MODE_TITLE);
	//}
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CTutorial* CTutorial::Create()
{
	m_pTutorial = new CTutorial;

	if (m_pTutorial != nullptr)
	{
		m_pTutorial->Init();
	}
	else
	{
		assert(false);
	}

	return m_pTutorial;
}
