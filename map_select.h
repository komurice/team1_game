//==================================================
// map_select.h
// Author: Buriya Kota
//==================================================
#ifndef _MAP_SELECT_H_
#define _MAP_SELECT_H_

//**************************************************
// インクルード
//**************************************************
#include "game_mode.h"

//**************************************************
// 前方宣言
//**************************************************
class CObject2D;
class CObject3D;
class CPlayer;

//**************************************************
// クラス
//**************************************************
class CMapSelect : public CGameMode
{
public:
	enum MODE
	{// 状態
		MODE_INPUT = 0,
		MODE_SCROLL_L,
		MODE_SCROLL_R,
		MODE_MAX
	};

	enum TEXTURE
	{// 使用しているテクスチャ
		TEXTURE_TUTORIAL = 0,
		TEXTURE_MAP01,
		TEXTURE_MAP02,
		TEXTURE_MAP03,
		TEXTURE_MAP04,
		TEXTURE_MAX
	};

	enum MAP
	{
		MAP_TUTORIAL = 0,
		MAP_01,
		MAP_02,
		MAP_03,
		MAP_04,
		MAP_MAX
	};

	CMapSelect();
	~CMapSelect() override;

	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw(DRAW_MODE /*drawMode*/) override {}

	static CMapSelect *Create();

	// プレイヤーの情報の取得
	static CMapSelect* GetMapSelect() { return m_pMapSelect; }

private:
	static CMapSelect* m_pMapSelect;

	CObject2D *m_pObject2D[MAP_MAX];

	MODE m_mode;
	TEXTURE m_disp; 
	int m_time;
	int m_nSelectMap;
};

#endif	// _PLAYER_SELECT_H_