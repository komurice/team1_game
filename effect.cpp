//============================
//
// エフェクト
// Author:hamada ryuuga
//
//============================

//**************************************************
// include
//**************************************************
#include <assert.h>
#include "manager.h"
#include "effect.h"
#include "particle.h"
#include "utility.h"


//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CEffect::CEffect(int nPriority /* =0 */) : CObject3D(nPriority)
{
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CEffect::~CEffect()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CEffect::Init()
{
	m_Life = 100;
	m_Move = { 0.0f,0.0f,0.0f };
	CObject3D::Init();
	m_isal = false;

	m_oto = OTO_MAX;

	return S_OK;
}

//--------------------------------------------------
// 終了
//--------------------------------------------------
void CEffect::Uninit()
{
	CObject3D::Uninit();
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CEffect::Update()
{
	CObject3D::Update();

	switch (m_moveType)
	{
	case CParticle::MOVE_DEF:
		SetDef();
		break;
	case CParticle::MOVE_DAMAGE:
		SetDamage();
		break;
	case CParticle::MOVE_MAX:
		break;
	default:
		break;
	}

	m_Life--;
	//移動
	D3DXVECTOR3 pos = GetPos();
	SetPos(pos + m_Move);
	D3DXCOLOR Col = GetCol();
	m_Life -= 1;

	if (m_isal)
	{
		Col.a -= 0.1f;
		SetCol(Col);
		if (Col.a <= 0.0f)
		{
			Uninit();
		}
	}

	if (m_Life <= 0.0f)
	{
		Uninit();
	}
}

//--------------------------------------------------
// 描画
//--------------------------------------------------
void CEffect::Draw(DRAW_MODE drawMode)
{
	CObject3D::Draw(drawMode);
}

//--------------------------------------------------
// 生成関数
//--------------------------------------------------
CEffect* CEffect::Create(D3DXVECTOR3 pos, D3DXVECTOR3 move, D3DXVECTOR3 Size, CParticle::MOVE mode, int nPriority, OTO oto, int life)
{
	CEffect* pObject;
	pObject = new CEffect(nPriority);

	if (pObject != nullptr)
	{
		pObject->Init();
		pObject->SetPos(pos);
		pObject->SetMove(move);
		pObject->SetSize(Size);
		pObject->setOto(oto);
		pObject->SetLife(life);
		pObject->SetEffect(mode);
	}
	else
	{
		assert(false);
	}

	return pObject;
}

//--------------------------------------------------
// 生成関数
//--------------------------------------------------
void CEffect::SetDef()
{
	float speedX = (rand() % 70 - 50) * 0.001f;
	float speedY = (rand() % 70 - 50) * 0.001f;

	m_Move.x += speedX;
	m_Move.y += speedY;
}

//--------------------------------------------------
// 生成関数
//--------------------------------------------------
void CEffect::SetDamage()
{
}

//--------------------------------------------------
// 画像生成
//--------------------------------------------------
void CEffect::SetEffect(CParticle::MOVE Mode)
{
	m_moveType = Mode;
	switch (m_moveType)
	{
	case CParticle::MOVE_DEF:
		SetTexture(CTexture::TEXTURE_EXPLOSION);
		break;
	case CParticle::MOVE_DAMAGE:
	{
		int rand = br::IntRandom(3, 0);

		switch ((OTO)rand)
		{
		case CEffect::OTO_BOKO:
			SetTexture(CTexture::TEXTURE_BOKO);
			break;
		case CEffect::OTO_BAKI:
			SetTexture(CTexture::TEXTURE_BAKI);
			break;
		case CEffect::OTO_DOKO:
			SetTexture(CTexture::TEXTURE_DOKO);
			break;
		case CEffect::OTO_MAX:
			break;
		default:
			break;
		}
		//m_Life = 3;
		m_isal = true;
	}
	break;

	case CParticle::MOVE_OTO:
	{
		switch (m_oto)
		{
		case CEffect::OTO_BOKO:
			SetTexture(CTexture::TEXTURE_BOKO);
			break;
		case CEffect::OTO_BAKI:
			SetTexture(CTexture::TEXTURE_BAKI);
			break;
		case CEffect::OTO_DOKO:
			SetTexture(CTexture::TEXTURE_DOKO);
			break;
		case CEffect::OTO_SUTA:
			SetTexture(CTexture::TEXTURE_SUTA);
			break;
		case CEffect::OTO_DA:
			SetTexture(CTexture::TEXTURE_DA);
			break;
		case CEffect::OTO_ZA:
			SetTexture(CTexture::TEXTURE_ZA);
			break;
		case CEffect::OTO_MAX:
			break;
		default:
			break;
		}
	}
	case CParticle::MOVE_MAX:
		break;
	default:
		break;
	}
}

void CEffect::SetEffect(CTexture::TEXTURE tex)
{
	if (m_moveType == CParticle::MOVE_ITEM)
	{
		SetTexture(tex);
	}
}

