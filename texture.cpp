//**************************************************
// 
// texture.cpp
// Author  : katsuki mizuki
// 
//**************************************************

//==================================================
// インクルード
//==================================================
#include "manager.h"
#include "renderer.h"
#include "texture.h"

#include <assert.h>

//==================================================
// 定義
//==================================================
const char* CTexture::s_FileName[] =
{// テクスチャのパス
	"data/TEXTURE/hasegawa_nagi.png",			// プレイヤー
	"data/TEXTURE/thumbnail.png",				// 地面
	"data/TEXTURE/shadow000.jpg",				// 影
	"data/TEXTURE/snow.png",					// TITLE_BG
	"data/TEXTURE/buri-zuboader.png",			// TITLE
	"data/TEXTURE/BG_Tutorial.png",				// TUTORIAL
	"data/TEXTURE/UI_PressEnter.png",			// Enter
	"data/TEXTURE/UI_Result.png",					// RESULT
	"data/TEXTURE/number001.png",				// タイム
	"data/TEXTURE/snow1.png",					// 雪
	"data/TEXTURE/snow_ground.png",				// 雪の地面
	"data/TEXTURE/sky.jpg",						// 空
	"data/TEXTURE/Glitter000.jpg",				// コイン
	"data/TEXTURE/choice_R.png",				// 選択アイコンR
	"data/TEXTURE/choice_L.png",				// 選択アイコンL
	"data/TEXTURE/ranking_number.png",			// 順位
	"data/TEXTURE/UI_Pause1.png",				// タイトルへ
	"data/TEXTURE/UI_Pause2.png",				// はじめから
	"data/TEXTURE/UI_Pause3.png",				// とじる
	"data/TEXTURE/pause.png",					// ポーズ
	"data/TEXTURE/press_enter_or_a.png",		// 押してほしいボタン
	"data/TEXTURE/UI_RankingBG_2.png",					// ランキング
	"data/TEXTURE/operationMethod.png",			// 操作方法
	"data/TEXTURE/mountain.png",				// マップ
	"data/TEXTURE/manual.png",					// チュートリアル
	"data/TEXTURE/goal.png",					// ごーる
	"data/TEXTURE/Bg000.png",					// 	背景
	"data/TEXTURE/Bg001.png",					// 	背景
	"data/TEXTURE/Bg002.png",					// 	背景
	"data/TEXTURE/UI_1P.png",					// 	UI1P
	"data/TEXTURE/UI_2P.png",					// 	UI2P
	"data/TEXTURE/UI_3P.png",					// 	UI3P
	"data/TEXTURE/UI_4P.png",					// 	UI4P
	"data/TEXTURE/UI_OK.png",					// 	ready（仮）
	"data/TEXTURE/selectBG0.png",				// セレクトBG
	"data/TEXTURE/selectBG1.png",				// セレクトBG2
	"data/TEXTURE/kyara0.png",					// セレクトBG背景
	"data/TEXTURE/kyara1.png",					// セレクトBG背景
	"data/TEXTURE/UI_GameStart.png",			// ゲーム開始
	"data/TEXTURE/UI_CharacterSelect1.png",		// セレクトBGプレイヤーの箱
	"data/TEXTURE/UI_CharacterSelect2.png",		// セレクトBGプレイヤーの箱
	"data/TEXTURE/UI_CharacterSelect3.png",		// セレクトBGプレイヤーの箱
	"data/TEXTURE/UI_CharacterSelect4.png",		// セレクトBGプレイヤーの箱
	"data/TEXTURE/arrow.png",					// やじるし
	"data/TEXTURE/UI_Select1.png",		// 	人数の選択
	"data/TEXTURE/UI_Select2.png",		// 	人数の選択
	"data/TEXTURE/UI_Select3.png",		// 	人数の選択
	"data/TEXTURE/UI_Select4.png",		// 	人数の選択
	"data/TEXTURE/UI_Stun1.png",		// 	スタンUI
	"data/TEXTURE/UI_Stun2.png",		// 	スタンUI
	"data/TEXTURE/UI_StunAnim.png",		// 	スタンUI
	"data/TEXTURE/Bullet001.png",		//　弾テクスチャ
	"data/TEXTURE/Explosion001.png",	//　爆発
	"data/TEXTURE/Item_UI_Shotgun.png",	//　アイテムUIショットガン
	"data/TEXTURE/Item_UI_LongRange.png",// アイテムUIロングレンジ
	"data/TEXTURE/Item_UI_Speed.png",	//　アイテムUIスピードアップ
	"data/TEXTURE/TiTle.png",	//　キングダム
	"data/TEXTURE/BG_leaf_spring.png",	//　フルーツ
	"data/TEXTURE/BG_leaf_summer.png",	//　フルーツ
	"data/TEXTURE/BG_leaf_autumn.png",	//　フルーツ
	"data/TEXTURE/BG_leaf_winter.png",	//　フルーツ
	"data/TEXTURE/BG_wood.png",	//　フルーツ
	"data/TEXTURE/bom.png",	//　爆発
	"data/TEXTURE/UI_ResultBG.png",	//　フルーツ
	"data/TEXTURE/fuki.png",	//　吹き出し
	"data/TEXTURE/ma052_11_0.png",	//　効果音
	"data/TEXTURE/Player_Effect_003.png",	//　効果音
	"data/TEXTURE/ma209_3_1.png",	//　効果音
	"data/TEXTURE/kiken.png",	//　危険	
	"data/TEXTURE/Player_Effect_001.png",	//　効果音
	"data/TEXTURE/Player_Effect_004.png",	//　効果音
	"data/TEXTURE/Player_Effect_006.png",	//　効果音
	"data/TEXTURE/effect001.png",	//　エフェクト1
	"data/TEXTURE/effect002.png",	//　エフェクト2
	"data/TEXTURE/effect003.png",	//　エフェクト3
	"data/TEXTURE/kusanew4.png",	//　KUSA
	"data/TEXTURE/StunEffect.png",	//　KUSA
	"data/TEXTURE/CountDown_00.png",	//　エフェクト2
	"data/TEXTURE/CountDown_01.png",	//　エフェクト3
	"data/TEXTURE/CountDown_02.png",	//　KUSA
	"data/TEXTURE/CountDown_03.png",	//　KUSA
	"data/TEXTURE/veworequ000967.png",	//　KUSA
	"data/TEXTURE/clown.png",	//　KUSA
	"data/TEXTURE/fight.png",	//　hukidasi
	"data/TEXTURE/fool.png",	//　hukidasi
	"data/TEXTURE/TUTORIAL_MOVE.png",	//　KUSA
	"data/TEXTURE/UI_Tutorial02.png",	//　KUSA
	"data/TEXTURE/UI_Tutorial03.png",	//　KUSA
	"data/TEXTURE/mesugaki1.png",	//　hukidasi
	"data/TEXTURE/BG_spring.png",	//　背景
	"data/TEXTURE/BG_summer.png",	//　背景
	"data/TEXTURE/BG_autumn.png",	//　背景
	"data/TEXTURE/BG_winter.png",	//　背景
	"data/TEXTURE/effect004.png",	//　星
	"data/TEXTURE/Gaya_001.png",	//　hukidasi
	"data/TEXTURE/Gaya_002.png",	//　hukidasi
	"data/TEXTURE/Gaya_003.png",	//　hukidasi
	"data/TEXTURE/Gaya_004.png",	//　hukidasi
	"data/TEXTURE/Gaya_005.png",	//　hukidasi
	"data/TEXTURE/score_effect.png",	//　Effect
	"data/TEXTURE/BG_leaf_springNight.png",	//　桜夜
	"data/TEXTURE/BG_leaf_summerNight.png",	//　夏葉夜
	"data/TEXTURE/BG_leaf_autumnNight.png",	//　紅葉夜
	"data/TEXTURE/BG_leaf_winterNight.png",	//　雪葉夜
	"data/TEXTURE/BG_woodNight.png",		//　幹夜
	"data/TEXTURE/BG_springNight.png",	//　背景
	"data/TEXTURE/BG_summerNight.png",	//　背景
	"data/TEXTURE/BG_autumnNight.png",	//　背景
	"data/TEXTURE/BG_winterNight.png",	//　背景
	"data/TEXTURE/BG_kusa.png",	//　背景
	"data/TEXTURE/BG_winterSnow.png",	//　背景
	"data/TEXTURE/UI_Ranking_1.png",	//　順位１
	"data/TEXTURE/UI_Ranking_2.png",	//　順位２
	"data/TEXTURE/UI_Ranking_3.png",	//　順位３
	"data/TEXTURE/UI_Ranking_4.png",	//　順位４
	"data/TEXTURE/UI_Ranking_5.png",	//　順位５
	"data/TEXTURE/BG_leafFuck.png",	//　桜モノ
	"data/TEXTURE/BG_woodFuck.png",		//　幹モノ
	"data/TEXTURE/BG_springFuck.png",	//　背景
	"data/TEXTURE/BG_summerFuck.png",	//　背景
	"data/TEXTURE/BG_autumnFuck.png",	//　背景
	"data/TEXTURE/BG_winterFuck.png",	//　背景
	"data/TEXTURE/BG_SelectStage1.png",	//　ステージセレクト1
	"data/TEXTURE/BG_SelectStage2.png",	//　ステージセレクト2
	"data/TEXTURE/BG_SelectStage3.png",	//　ステージセレクト3
	"data/TEXTURE/BG_SelectStage4.png",	//　ステージセレクト4
	"data/TEXTURE/UI_Tutorial02.png",					// チュートリアル
	"data/TEXTURE/UI_Tutorial03.png",					// チュートリアル

};

static_assert(sizeof(CTexture::s_FileName) / sizeof(CTexture::s_FileName[0]) == CTexture::TEXTURE_MAX, "aho");

//--------------------------------------------------
// デフォルトコンストラクタ
//--------------------------------------------------
CTexture::CTexture() :
	s_pTexture()
{
	memset(s_pTexture, 0, sizeof(s_pTexture));
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CTexture::~CTexture()
{
}

//--------------------------------------------------
// 全ての読み込み
//--------------------------------------------------
void CTexture::LoadAll()
{
	// デバイスへのポインタの取得
	LPDIRECT3DDEVICE9 pDevice = CManager::GetManager()->GetRenderer()->GetDevice();

	for (int i = 0; i < TEXTURE_MAX; ++i)
	{
		if (s_pTexture[i] != nullptr)
		{// テクスチャの読み込みがされている
			continue;
		}

		// テクスチャの読み込み
		D3DXCreateTextureFromFile(pDevice,
			s_FileName[i],
			&s_pTexture[i]);
	}
}

//--------------------------------------------------
// 読み込み
//--------------------------------------------------
void CTexture::Load(TEXTURE inTexture)
{
	assert(inTexture >= 0 && inTexture < TEXTURE_MAX);

	if (s_pTexture[inTexture] != nullptr)
	{// テクスチャの読み込みがされている
		return;
	}

	// デバイスへのポインタの取得
	LPDIRECT3DDEVICE9 pDevice = CManager::GetManager()->GetRenderer()->GetDevice();

	// テクスチャの読み込み
	D3DXCreateTextureFromFile(pDevice,
		s_FileName[inTexture],
		&s_pTexture[inTexture]);
}

//--------------------------------------------------
// 全ての解放
//--------------------------------------------------
void CTexture::ReleaseAll(void)
{
	for (int i = 0; i < TEXTURE_MAX; ++i)
	{
		if (s_pTexture[i] != nullptr)
		{// テクスチャの解放
			s_pTexture[i]->Release();
			s_pTexture[i] = nullptr;
		}
	}
}

//--------------------------------------------------
// 解放
//--------------------------------------------------
void CTexture::Release(TEXTURE inTexture)
{
	assert(inTexture >= 0 && inTexture < TEXTURE_MAX);

	if (s_pTexture[inTexture] != nullptr)
	{// テクスチャの解放
		s_pTexture[inTexture]->Release();
		s_pTexture[inTexture] = nullptr;
	}
}

//--------------------------------------------------
// 取得
//--------------------------------------------------
LPDIRECT3DTEXTURE9 CTexture::GetTexture(TEXTURE inTexture)
{
	if (inTexture == TEXTURE_NONE)
	{// テクスチャを使用しない
		return nullptr;
	}

	assert(inTexture >= 0 && inTexture < TEXTURE_MAX);

	// 読み込み
	Load(inTexture);

	return s_pTexture[inTexture];
}
