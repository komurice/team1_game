//============================
//
// 軌跡設定ヘッター
// Author:hamada ryuuga
//
//============================
#ifndef _TRAJECTORY_H_
#define _TRAJECTORY_H_

#include "main.h"
#include "object.h"

class CTrajectory : public CObject
{
public:

	const int MAXTRAJECTORY = 50;
	CTrajectory(int nPriority = PRIORITY_OBJECT);
	~CTrajectory() override;

	HRESULT Init()override;//初期化
	void Uninit()override;//破棄
	void Update()override;//更新
	void Draw(DRAW_MODE drawMode)override;//描画


	static CTrajectory* Create();

	//セッター
	void SetPos(const D3DXVECTOR3 &pos);
	void SetPosOr(const D3DXVECTOR3 &pos) { m_posOrigin = pos; };
	void SetMove(float ismove) { m_move = ismove; }
	
	void SetNumber(int IsNumber) { m_number = IsNumber; }
	void SetType(int IsType) { m_type = IsType; }
	void SwitchCollision(bool onCollision) { m_iscollision = onCollision; };
	void SetMtx(D3DXMATRIX *MtxWorld) { m_ModelWorld = MtxWorld; m_IsSetMtx = true; }
	void SetIsDraw(bool Isdraw) { m_myDraw = Isdraw; }
	void SetColor(D3DXCOLOR color) { m_color = color; }
	void SetPlayerNum(int Is);
	//ゲッター
	const D3DXVECTOR3 *GetPos() const;
	D3DXVECTOR3 GetOneMeshSize() { return m_meshSize; }
	D3DXVECTOR3 GetMeshSize() { return D3DXVECTOR3(m_X *m_meshSize.x, 0.0f, m_Z *m_meshSize.z); }
	D3DXVECTOR3 * GetPos() { return &m_posOrigin; }
	float GetMove() { return m_move; }
	int GetNumber() { return m_number; }
	int GetMeshType() { return m_type; }

	int GetMeshSizeX() { return m_X; }

private:


	LPDIRECT3DVERTEXBUFFER9 m_pVtxBuff;	    // 頂点バッファーへのポインタ
	LPDIRECT3DTEXTURE9 m_pTextureEmesh;        //テクスチャのポインタ
	LPDIRECT3DINDEXBUFFER9 m_pIdxBuff;         //インデックスバッファ

	D3DXVECTOR3 m_pos;	// 頂点座標
	D3DXVECTOR3 m_posOrigin;	// 頂点座標
	D3DXVECTOR3 m_rot;	// 回転座標
	D3DXMATRIX m_mtxWorld;// ワールドマトリックス
	D3DXMATRIX *m_ModelWorld;// ワールドマトリックス
	int m_xsiz;//面数
	int m_zsiz;//面数
	int m_X;//辺の頂点数
	int m_Z;//辺の頂点数
	int m_vtx;//頂点数
	int m_index; //インデックス
	int m_por;
	int m_nowMesh;
	int m_number;
	int m_type;
	int m_num;
	float m_move;
	D3DXVECTOR3 m_meshSize;
	D3DXVECTOR3* m_posMesh;
	bool m_IsSetMtx;
	bool m_iscollision;
	bool m_myDraw;
	D3DXCOLOR m_color;



};
#endif


