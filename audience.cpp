//==================================================
// player3D.cpp
// Author: Buriya Kota
//==================================================

//**************************************************
// include
//**************************************************
#include <assert.h>

#include "utility.h"
#include "debug_proc.h"

#include "game.h"
#include "tutorial.h"

#include "manager.h"
#include "camera.h"
#include "sound.h"
#include "input.h"
#include "model.h"

#include "utility.h"
#include "motion.h"

#include "stun_gauge.h"
#include "audience.h"

//**************************************************
// マクロ定義
//**************************************************
#define MAX_SPEED			(10.0f)
#define FRICTION			(0.07f)
#define GRAVITY				(0.5f)
#define MAX_ACCEL			(1.0f)
#define MIN_ACCEL			(0.0f)
#define COIN_PARTICLE		(5)
#define NUM_GOAL			(2)
#define KNOCKBACK			(20.0f)
#define BULLET_TIME			(12)
#define JUMP				(15.0f)
#define X_MOVE_LIMIT		(100.0f)
#define Y_MOVE_LIMIT		(6.0f)
#define KNOCK_BACK_POWER_X	(0.7f)
#define KNOCK_BACK_POWER_Y	(8.0f)
#define CHARGE_TIME			(120)

  //**************************************************
  // 静的メンバ変数
  //**************************************************
const float CAdience::PLAYER_SPEED = 5.0f;

// 
const CObject::UPDATE_FUNC CAdience::mUpdateFunc[] =
{
	UPDATE_FUNC_CAST(Update_Idle),
	UPDATE_FUNC_CAST(Update_Land),
	UPDATE_FUNC_CAST(Update_Fly),
};

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CAdience::CAdience(int nPriority) : CMotionModel3D(nPriority)
{
	// 数が一致しなかったらエラー
	static_assert(sizeof(mUpdateFunc) / sizeof(mUpdateFunc[0]) == STATE_MAX, "baka");

}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CAdience::~CAdience()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CAdience::Init()
{
	CMotionModel3D::Init();

	m_pos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	m_posOld = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	m_rot = D3DXVECTOR3(0.0f, D3DX_PI, 0.0f);
	SetRot(m_rot);
	m_rotDest = D3DXVECTOR3(0.0f, D3DX_PI, 0.0f);
	m_move = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	m_size = D3DXVECTOR3(10.0f, 25.0f, 50.0f);
	m_scale = D3DXVECTOR3(1.0f, 1.0f, 1.0f);
	//m_action = TYPE_NEUTRAL;
	SetType(TYPE_NONE);

	InitStateFunc(mUpdateFunc, STATE_MAX);

	// プレイヤーの初期状態
	SetState(STATE_IDLE);

	return S_OK;
}

//--------------------------------------------------
// 終了
//--------------------------------------------------
void CAdience::Uninit()
{
	// 現在のモーション番号の保管
	CMotionModel3D::Uninit();

	// deleteフラグを立てる
	CObject::DeletedObj();
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CAdience::Update()
{
	CObject::Update();
	CMotion* pMotion = CMotionModel3D::GetMotion();
	// posOld の更新
	D3DXVECTOR3 posOld = CMotionModel3D::GetPos();
	SetPosOld(posOld);

	if ((CManager::GetGameMode() == CManager::MODE_GAME) ||
		(CManager::GetGameMode() == CManager::MODE_TUTORIAL))
	{
		if (pMotion != nullptr && m_action != TYPE_NEUTRAL)
		{//アニメーション設定
			m_action = TYPE_NEUTRAL;
			pMotion->SetNumMotion(m_action);

		}
		//// 重力
		/*	m_move.y += -GRAVITY;
		m_move.x += (0.0f - m_move.x) * (0.025f); */   //（目的の値-現在の値）＊減衰係数

		D3DXVECTOR3 pos = CMotionModel3D::GetPos();
		//ブースト
		m_move.x += (0.0f - m_move.x) * (0.025f);    //（目的の値-現在の値）＊減衰係数

		pos += m_move;
		SetPos(pos);

		CMotionModel3D::Update();
	
	}
}

//--------------------------------------------------
// 描画
//--------------------------------------------------
void CAdience::Draw(DRAW_MODE /*drawMode*/)
{	
	CMotionModel3D::Draw(m_scale);
}

//--------------------------------------------------
// アイドルの処理
//--------------------------------------------------
void CAdience::Update_Idle()
{
	SetState(STATE_LAND);
}

//--------------------------------------------------
// 歩いてるとき
//--------------------------------------------------
void CAdience::Update_Land()
{
	SetState(STATE_FLY);
}

//--------------------------------------------------
// ジャンプしたとき
//--------------------------------------------------
void CAdience::Update_Fly()
{
	SetState(STATE_IDLE);
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CAdience* CAdience::Create(D3DXVECTOR3 pos, D3DXVECTOR3 scale, std::string Model)
{
	CAdience* pAdience = nullptr;
	pAdience = new CAdience;

	if (pAdience != nullptr)
	{
		pAdience->Init();
		pAdience->SetPos(pos);
		pAdience->CMotionModel3D::SetPos(pos);
		pAdience->SetMotion(Model.c_str());
		pAdience->SetScale(scale);
	}
	else
	{
		assert(false);
	}

	return pAdience;
}



//--------------------------------------------------
// 現在の角度の正規化
//--------------------------------------------------
D3DXVECTOR3 CAdience::RotNormalization(D3DXVECTOR3 rot)
{
	m_rot = rot;

	// 現在の角度の正規化
	if (m_rot.y > D3DX_PI)
	{
		m_rot.y -= D3DX_PI * 2.0f;
	}
	else if (m_rot.y < -D3DX_PI)
	{
		m_rot.y += D3DX_PI * 2.0f;
	}

	return m_rot;
}

//--------------------------------------------------
// 目的の角度の正規化
//--------------------------------------------------
D3DXVECTOR3 CAdience::RotDestNormalization(D3DXVECTOR3 rot, D3DXVECTOR3 rotDest)
{
	m_rot = rot;
	m_rotDest = rotDest;

	// 目的の角度の正規化
	if (m_rotDest.y - m_rot.y > D3DX_PI)
	{
		m_rotDest.y -= D3DX_PI * 2.0f;
	}
	else if (m_rotDest.y - m_rot.y < -D3DX_PI)
	{
		m_rotDest.y += D3DX_PI * 2.0f;
	}

	return m_rotDest;
}
