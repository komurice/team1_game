//============================
//
// マップチップ
// Author:hamada ryuuga
//
//============================

//**************************************************
// include
//**************************************************
#include <assert.h>
#include "manager.h"
#include "judgment.h"
#include "objectX.h"
#include "object2D.h"
#include "fade.h"
#include "player.h"
#include "sound.h"

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CJudgment::CJudgment(int nPriority /* =0 */) : CObject(nPriority)
{
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CJudgment::~CJudgment()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CJudgment::Init()
{
	m_Bgblender = CObject3D::Create(D3DXVECTOR3(0.0f, 0.0f, -100.0f),
		D3DXVECTOR3(CManager::SCREEN_WIDTH*0.5f, CManager::SCREEN_HEIGHT*0.6f, 0.0f), PRIORITY_EFFECT);
	m_Bgblender->SetTexture(CTexture::TEXTURE_FIN);
	m_Bgblender->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
	m_Bgblender->SetSize(D3DXVECTOR3(0.0f, 0.0f, 0.0f));
	m_size = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	m_count = 0;
	m_is = true;
	//m_IsResult
	m_win = nullptr;

	// フィニッシュサウンド
	CManager::GetManager()->GetSound()->Play(CSound::LABEL_SE_FINISH);
	
	return S_OK;
}

//--------------------------------------------------
// 終了
//--------------------------------------------------
void CJudgment::Uninit()
{

	CObject::DeletedObj();
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CJudgment::Update()
{
	
	if (m_is)
	{
		m_size.x += CManager::SCREEN_WIDTH  / 90.0f;
		m_size.y += CManager::SCREEN_HEIGHT / 90.0f;
		m_Bgblender->SetSize(m_size);
		m_count++;
		if (m_count >= 60)
		{
			m_is = false;
		}
		if (m_win != nullptr)
		{
			//D3DXVECTOR3 Pos =m_win->GetPos();
			//m_clown->SetPos(Pos);
			//m_clown->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
		}
	}
	else
	{
		if (m_IsResult)
		{
			CFade::GetInstance()->SetFade(CManager::MODE_RESULT);

		}
		else
		{
			CFade::GetInstance()->SetFade(CManager::MODE_RANKING);
		}
	}


}

//--------------------------------------------------
// 描画
//--------------------------------------------------
void CJudgment::Draw(DRAW_MODE drawMode)
{

}

//--------------------------------------------------
// 生成関数
//--------------------------------------------------
CJudgment * CJudgment::Create(bool IsResult)
{
	CJudgment *pObject2D;
	pObject2D = new CJudgment(1);

	if (pObject2D != nullptr)
	{
		pObject2D->Init();
		pObject2D->SetResult(IsResult);
	}
	else
	{
		assert(false);
	}

	return pObject2D;
}


