//==================================================
// bullet.h
// Author: Buriya Kota
//==================================================
#ifndef _BULLET_H_
#define _BULLET_H_

//**************************************************
// インクルード
//**************************************************
#include "object3d.h"

//**************************************************
// 前方前言　実態はNG　ポインタだけならOK
//**************************************************

//**************************************************
// 構造体定義
//**************************************************
class CTrajectory;
//**************************************************
// クラス
//**************************************************


class CBullet : public CObject3D
{
public:

	enum class BULLET_TYPE
	{
		DEFAULT,
		RANGE_EXTENSION,
		SHOTGUN,
		STUN_BULLET,
		ATTACK
	};

    explicit CBullet(int nPriority = PRIORITY_BULLET);
    ~CBullet();

    HRESULT Init() override;
    void Update() override;
	void Draw(DRAW_MODE drawMode) override;
    static CBullet* Create(const D3DXVECTOR3 pos, D3DXVECTOR3 move, const D3DXVECTOR3 size, int nShotPlayerNumber, BULLET_TYPE type = BULLET_TYPE::DEFAULT);

    void SetBulletType(BULLET_TYPE type);
	CBullet::BULLET_TYPE GetBulletType() const;

private:
    void MoveBullet_();
    // プレイヤーと弾の当たり判定
    bool CollisionPlayer_();
    bool CollisionBlock_();
	void SetPlayerNumColor(int Is);
private:
    // 球を撃ったプレイヤー番号
    int m_nShotPlayerNumber;
	CTrajectory*m_tra;
    int m_life;
    BULLET_TYPE m_bulletType;

	static int m_nSoundCnt;			//ヒットサウンドを切り替える
};

#endif	// _BULLET_H_