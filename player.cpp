//==================================================
// player3D.cpp
// Author: Buriya Kota
//==================================================

//**************************************************
// include
//**************************************************
#include <assert.h>

#include "utility.h"
#include "debug_proc.h"

#include "game.h"
#include "tutorial.h"

#include "manager.h"
#include "camera.h"
#include "sound.h"
#include "input.h"
#include "model.h"

#include "score.h"
#include "object3D.h"
#include "player.h"
#include "shadow.h"
#include "meshfield.h"
#include "bullet.h"
#include "map.h"
#include "utility.h"
#include "motion.h"
#include "particle.h"
#include "fade.h"
#include "stage_ui.h"
#include "stun_gauge.h"
#include "result.h"
#include "stuneffect.h"
#include "signboard.h"
#include "judgment.h"
#include "effect.h"

//**************************************************
// マクロ定義
//**************************************************
#define MAX_SPEED			(10.0f)
#define FRICTION			(0.07f)
#define GRAVITY				(0.5f)
#define MAX_ACCEL			(1.0f)
#define MIN_ACCEL			(0.0f)
#define COIN_PARTICLE		(5)
#define NUM_GOAL			(2)
#define KNOCKBACK			(20.0f)
#define BULLET_TIME			(12)
#define JUMP				(15.0f)
#define X_MOVE_LIMIT		(50.0f)
#define Y_MOVE_LIMIT		(3.0f)
#define KNOCK_BACK_POWER_X	(0.7f)
#define KNOCK_BACK_POWER_Y	(8.0f)
#define CHARGE_TIME			(120)

namespace
{
	const std::string fileName[5] =
	{ "data/TEXT/motion_apple.txt",
		"data/TEXT/motion_grape.txt",
		"data/TEXT/motion_Orange.txt",
		"data/TEXT/motion_strawberry.txt",
		"data/TEXT/motion_cherry.txt" };

} // namespace

//**************************************************
// 静的メンバ変数
//**************************************************
const float CPlayer::PLAYER_SPEED = 5.0f;

// 
const CObject::UPDATE_FUNC CPlayer::mUpdateFunc[] =
{
	UPDATE_FUNC_CAST(Update_Idle),
	UPDATE_FUNC_CAST(Update_Land),
	UPDATE_FUNC_CAST(Update_Fly),
};

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CPlayer::CPlayer(int nPriority) : CMotionModel3D(nPriority), m_isHitSignBoard(false)
{
	// 数が一致しなかったらエラー
	static_assert(sizeof(mUpdateFunc) / sizeof(mUpdateFunc[0]) == STATE_MAX, "baka");

	m_nPlayerNumber = -1;
	m_pArrow = nullptr;
	m_pStunGauge = nullptr;
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CPlayer::~CPlayer()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CPlayer::Init()
{
	CMotionModel3D::Init();

	m_nCurrentKey = 0;
	m_Diff = 0;
	m_pos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	m_posOld = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	m_rot = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	SetRot(m_rot);
	m_rotDest = D3DXVECTOR3(0.0f, D3DX_PI, 0.0f);
	m_move = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	m_size = D3DXVECTOR3(10.0f, 25.0f, 50.0f);
	m_vec = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	m_isBulletAttack = false;

	m_playerRecallPos.clear();
	m_nCountRecall = 0;
	m_nVectorSize = 0;
	m_bRecall = false;
	m_bMaxSize = false;

	m_bulletType = BULLET_TYPE::DEFAULT;
	m_Speed = { 0.0f,0.0f,0.0f };
	m_playerState = PLAYER_SKY;
	m_Moveboost = 0.0f;
	m_nBulletTime = 0;
	m_nChargeTime = 0;
	m_bIsCharge = false;

	m_bKnockBack = false;
	m_nKnockBackTime = 0;
	m_fKnockBackFrame = 0.0f;
	m_fKnockBackPowerX = 0.0f;
	m_fKnockBackPowerY = 0.0f;

	m_nStun = 0;
	m_nStunTime = 0;
	m_IsStun = false;
	m_nStanRecoveryTime = 0;
	m_Stun = nullptr;

	m_remainingBullets = 999;

	m_isMouse = false;
	m_Shottimer = 0;
	SetType(CObject::TYPE_PLAYER);
	if (CManager::GetGameMode() == CManager::MODE_GAME ||
		CManager::GetGameMode() == CManager::MODE_TUTORIAL)
	{
		m_pArrow = CObject3D::Create(D3DXVECTOR3(0.0f, 0.0f, 0.0f), D3DXVECTOR3(20.0f, 50.0f, 0.0f), PRIORITY_OBJECT);
		m_pArrow->SetCol(D3DXCOLOR(0.0f, 0.0f, 0.0f, 0.0f));
		m_pArrow->SetTexture(CTexture::TEXTURE_ARROW);
		m_Ui = CStage_Ui::Create(m_nPlayerNumber, 0);
	}

	m_pStunGauge = CStunGauge::Create(m_nPlayerNumber);

	InitStateFunc(mUpdateFunc, STATE_MAX);
	m_IsclearStun = false;
	// プレイヤーの初期状態
	SetState(STATE_IDLE);
	m_timer = 0;
	m_IsInSky = false;
	return S_OK;
}

//--------------------------------------------------
// 終了
//--------------------------------------------------
void CPlayer::Uninit()
{
	CManager::GetManager()->AddRanking(m_nPlayerNumber);

	// 現在のモーション番号の保管
	CMotionModel3D::Uninit();

	// deleteフラグを立てる
	CObject::DeletedObj();
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CPlayer::Update()
{

	m_IsAnime = false;
	CObject::Update();

	// posOld の更新
	D3DXVECTOR3 posOld = CMotionModel3D::GetPos();
	SetPosOld(posOld);

	CManager::MODE mode = CManager::GetGameMode();
	// 重力
	m_move.y += -GRAVITY;
	if ((mode == CManager::MODE_GAME) ||
		(mode == CManager::MODE_TUTORIAL) ||
		mode == CManager::MODE_RESULT
		)
	{
		if ((mode == CManager::MODE_GAME) ||
			(mode == CManager::MODE_TUTORIAL))
		{
			if (CGame::GetGame() != nullptr)
			{
				if (!CGame::GetGame()->GetStagr())
				{
					return;
				}
			}
			m_Shottimer--;
			if (m_Shottimer <= 0)
			{
				CPlayer::SetShootType(BULLET_TYPE::DEFAULT);
			}
			m_timer++;
			if (m_timer >= 30)
			{
				m_timer = 0;
				m_Ui->PopEffect(CMotionModel3D::GetPos());

			}
			// プレイヤーの動き
			Control_();

			// リコールの処理
			Recall_();

			// ノックバック
			KnockBack_();

			// スタンのUIのカウント
			m_pStunGauge->SetNumStun(m_nStun);

			// スタン
			Stun_();
		}

		// モーション
		Motion_();

		D3DXVECTOR3 pos = CMotionModel3D::GetPos();
		//ブースト
		m_MoveboostTime--;
		if (m_MoveboostTime <= 0)
		{
			m_Moveboost = 0.0f;
			m_MoveboostTime = 0;
			m_Speed.x = 0;
		}

		m_move.x += (0.0f - m_move.x) * (0.025f);    //（目的の値-現在の値）＊減衰係数

		pos += m_move;

		SetPos(pos);

		CMotionModel3D::Update();

		if (CGame::GetGame() != nullptr)
		{
			if (CGame::GetGame()->GetMap()->blockHit(pos, m_size, this))
			{//ここあたったとき
				m_playerState = PLAYER_GROUND;
				m_move.y = 0.0f;
			}
			else
			{
				m_playerState = PLAYER_SKY;
			}
			if (CGame::GetGame()->GetMap()->itemHit(pos, m_size, this))
			{//ここあたったとき

			}
			if (CGame::GetGame()->GetMap()->EnemyHit(pos, m_size, this, true))
			{//ここあたったとき

			}

		}
		else if (CTutorial::GetTutorial() != nullptr)
		{
			//CTutorial::GetTutorial()->GetMap()->GetSignBoard()->Collision(this);

			if (CTutorial::GetTutorial()->GetMap()->blockHit(pos, m_size, this))
			{//ここあたったとき
				m_playerState = PLAYER_GROUND;
				m_move.y = 0.0f;
			}
			else
			{
				m_playerState = PLAYER_SKY;
			}
			if (CTutorial::GetTutorial()->GetMap()->itemHit(pos, m_size, this))
			{//ここあたったとき

			}
			if (CTutorial::GetTutorial()->GetMap()->EnemyHit(pos, m_size, this, true))
			{//ここあたったとき

			}

		}
		else if (mode == CManager::MODE_RESULT)
		{
			if (CResult::GetResult()->blockHit(pos, m_size, this))
			{//ここあたったとき
				m_playerState = PLAYER_GROUND;
				m_move.y = 0.0f;
			}
			else
			{
				m_playerState = PLAYER_SKY;
			}
		}

		if (mode != CManager::MODE_RESULT)
			Outarea();

		if (m_playerState == PLAYER_GROUND && m_IsInSky != true)
		{
			CManager::GetManager()->GetSound()->Play(CSound::LABEL_SE_LANDING);	//着地音
			CEffect::Create(pos - D3DXVECTOR3(0.0f, 20.0f, 0.0f), D3DXVECTOR3(0.0f, -2.0f, 0.0f), D3DXVECTOR3(50.0f, 50.0f, 0.0f), CParticle::MOVE_OTO, PRIORITY_EFFECT, CEffect::OTO_SUTA, 30);
			m_IsInSky = true;
		}
		else if (m_playerState == PLAYER_SKY)
		{
			m_IsInSky = false;
		}
	}

#ifdef _DEBUG
	// デバッグ表示
	//SetPos(pos);
	CDebugProc::Print("プレイヤーの現在の角度 : %f\n", m_rot.y);
	CDebugProc::Print("プレイヤーの現在の位置 : %f,%f,%f\n", m_pos.x, m_pos.y, m_pos.z);
	CDebugProc::Print("プレイヤーの過去の位置 : %f,%f,%f\n", m_posOld.x, m_posOld.y, m_posOld.z);
	CDebugProc::Print("プレイヤーのMOVE : %+f, %+f, %+f\n", m_move.x, m_move.y, m_move.z);
	CDebugProc::Print("プレイヤー弾数 : %d\n", m_remainingBullets);
#endif // DEBUG
}

//--------------------------------------------------
// 描画
//--------------------------------------------------
void CPlayer::Draw(DRAW_MODE drawMode)
{
	CMotionModel3D::Draw(drawMode);
}

//--------------------------------------------------
// アイドルの処理
//--------------------------------------------------
void CPlayer::Update_Idle()
{
	SetState(STATE_LAND);
}

//--------------------------------------------------
// 歩いてるとき
//--------------------------------------------------
void CPlayer::Update_Land()
{
	SetState(STATE_FLY);
}

//--------------------------------------------------
// ジャンプしたとき
//--------------------------------------------------
void CPlayer::Update_Fly()
{
	SetState(STATE_IDLE);
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CPlayer* CPlayer::Create(D3DXVECTOR3 pos, int number, int playerNumber)
{
	CPlayer* pPlayer3D = nullptr;
	pPlayer3D = new CPlayer;

	if (pPlayer3D != nullptr)
	{
		pPlayer3D->SetPlayerNumber(number);
		pPlayer3D->Init();
		pPlayer3D->SetPos(pos);
		pPlayer3D->CMotionModel3D::SetPos(pos);
		pPlayer3D->SetMotion(fileName[playerNumber].c_str());
	}
	else
	{
		assert(false);
	}

	return pPlayer3D;
}

//--------------------------------------------------
// 弾の選択
//--------------------------------------------------
void CPlayer::SetBulletType(BULLET_TYPE type)
{
	m_bulletType = type;

	switch (m_bulletType)
	{
	case BULLET_TYPE::RANGE_EXTENSION:
		m_remainingBullets = 5;
		break;

	case BULLET_TYPE::SHOTGUN:
		m_remainingBullets = 3;
		break;

	case BULLET_TYPE::STUN_BULLET:
		m_remainingBullets = 2;
		break;

	default:
		break;
	}
}

//--------------------------------------------------
// 現在の角度の正規化
//--------------------------------------------------
D3DXVECTOR3 CPlayer::RotNormalization(D3DXVECTOR3 rot)
{
	m_rot = rot;

	// 現在の角度の正規化
	if (m_rot.y > D3DX_PI)
	{
		m_rot.y -= D3DX_PI * 2.0f;
	}
	else if (m_rot.y < -D3DX_PI)
	{
		m_rot.y += D3DX_PI * 2.0f;
	}

	return m_rot;
}

//--------------------------------------------------
// 目的の角度の正規化
//--------------------------------------------------
D3DXVECTOR3 CPlayer::RotDestNormalization(D3DXVECTOR3 rot, D3DXVECTOR3 rotDest)
{
	m_rot = rot;
	m_rotDest = rotDest;

	// 目的の角度の正規化
	if (m_rotDest.y - m_rot.y > D3DX_PI)
	{
		m_rotDest.y -= D3DX_PI * 2.0f;
	}
	else if (m_rotDest.y - m_rot.y < -D3DX_PI)
	{
		m_rotDest.y += D3DX_PI * 2.0f;
	}

	return m_rotDest;
}

//--------------------------------------------------
// リコール
//--------------------------------------------------
void CPlayer::Recall_()
{
	D3DXVECTOR3 pos = GetPos();

	if (!m_bRecall)
	{
		// プレイヤーの過去の位置を保存
		m_playerRecallPos.push_back(pos);

		if (m_playerRecallPos.size() > 120)
		{// sizeが120を超えたら最初の要素を消す
			m_playerRecallPos.erase(m_playerRecallPos.begin());
		}
	}

	// 値が同じだった場合(値が隣接している場合)は詰める
	// erase　コンテナの要素を削除する
	// unique 隣り合った重複要素を除いた要素を、範囲の先頭に集める 削除はされないため　erase　で削除する
	//m_playerRecallPos.erase(std::unique(m_playerRecallPos.begin(), m_playerRecallPos.end()), m_playerRecallPos.end());

	// インプット
	CInput* pInput = CInput::GetKey();

	if (pInput->Trigger(JOYPAD_Y, m_nPlayerNumber))
	{// リコール
		m_bRecall = true;

		if (!m_bMaxSize)
		{// 今のサイズを取る
			m_nVectorSize = m_playerRecallPos.size();
			m_bMaxSize = true;
		}
	}

	if (m_bRecall)
	{// リコール中
		m_nCountRecall++;
		SetPos(m_playerRecallPos[m_nVectorSize - m_nCountRecall]);
	}

	if (m_nCountRecall == m_nVectorSize)
	{// カウントとサイズが一緒になったら
		for (int i = 0; i < m_nVectorSize; i++)
		{// 現在の位置を代入する
			m_playerRecallPos[i] = m_pos;
		}
		// 初期化
		m_nVectorSize = 0;
		m_nCountRecall = 0;
		m_bMaxSize = false;
		m_bRecall = false;
	}
}

//--------------------------------------------------
// ノックバック
//--------------------------------------------------
void CPlayer::KnockBack_()
{
	// 弾の方向を取ってくる
	D3DXVec3Normalize(&m_bulletMove, &m_bulletMove);

	if (m_isBulletAttack)
	{
		m_fKnockBackPowerX = KNOCK_BACK_POWER_X * 2.0f;
		m_fKnockBackPowerY = KNOCK_BACK_POWER_Y * 2.0f;
	}
	else
	{
		m_fKnockBackPowerX = KNOCK_BACK_POWER_X;
		m_fKnockBackPowerY = KNOCK_BACK_POWER_Y;
	}

	if (m_bKnockBack)
	{
		D3DXVECTOR3 knockBackPower(0.0f, 0.0f, 0.0f);

		knockBackPower.x = m_fKnockBackPowerX;
		knockBackPower.y = m_fKnockBackPowerY;

		// ノックバックの値
		m_move.x += knockBackPower.x * m_bulletMove.x;
		m_move.y += knockBackPower.y;

		if (m_isBulletAttack)
		{
			if (m_move.x >= X_MOVE_LIMIT * 2.0f)
			{// Xの上限
				m_move.x = X_MOVE_LIMIT * 2.0f;
			}

			if (m_move.x <= -X_MOVE_LIMIT * 2.0f)
			{// Xの上限
				m_move.x = -X_MOVE_LIMIT * 2.0f;
			}

			if (m_move.y >= Y_MOVE_LIMIT * 2.0f)
			{// Yの上限
				m_move.y = Y_MOVE_LIMIT * 2.0f;
			}
		}
		else
		{
			if (m_move.x >= X_MOVE_LIMIT)
			{// Xの上限
				m_move.x = X_MOVE_LIMIT;
			}

			if (m_move.x <= -X_MOVE_LIMIT)
			{// Xの上限
				m_move.x = -X_MOVE_LIMIT;
			}

			if (m_move.y >= Y_MOVE_LIMIT)
			{// Yの上限
				m_move.y = Y_MOVE_LIMIT;
			}
		}

		m_nKnockBackTime++;
	}

	if (m_nKnockBackTime > 10)
	{

		m_nKnockBackTime = 0;
		m_bKnockBack = false;
	}
}

//--------------------------------------------------
// ジャンプ
//--------------------------------------------------
void CPlayer::Jump_()
{
	// インプット
	CInput* pInput = CInput::GetKey();

	CMotion* pMotion = CMotionModel3D::GetMotion();

	if ((pInput->Trigger(JOYPAD_L1, m_nPlayerNumber) ||
		//(pInput->Trigger(JOYPAD_LEFT_UP, m_nPlayerNumber)) ||
		(pInput->Trigger(DIK_W, m_nPlayerNumber))) && m_playerState == PLAYER_GROUND)
	{// ジャンプ
		m_pos.y += JUMP;
		m_move.y += JUMP;
		m_playerState = PLAYER_SKY;
		CManager::GetManager()->GetSound()->Play(CSound::LABEL_SE_JUMP);	//ジャンプ音

		if (pMotion != nullptr && m_action != TYPE_JUMP)
		{//アニメーション設定
			m_IsAnime = true;
			m_action = TYPE_JUMP;
			pMotion->SetNumMotion(m_action);
		}
	}
}

//--------------------------------------------------
// スタン
//--------------------------------------------------
void CPlayer::Stun_()
{
	if (m_nStun >= MAX_STUN)
	{
		m_IsStun = true;
		m_nStunTime++;
		m_pStunGauge->SetAnyPlayer(m_nPlayerNumber);
		m_pStunGauge->SetIsStun(true);
		if (!m_IsclearStun)
		{
			CManager::GetManager()->GetSound()->Play(CSound::LABEL_SE_STAN);	//スタン音
			m_IsclearStun = true;
			D3DXVECTOR3 pos = GetPos();
			m_Stun = CStunEffect::Create(pos, D3DXVECTOR3(100.0f, 100.0f, 0.0f));
		}
	}
	if (m_IsclearStun)
	{
		D3DXVECTOR3 pos = GetPos();
		m_Stun->SetPos(pos);
	}
	// モデル点滅させる予定

	if (m_nStunTime >= STUN_FRAME)
	{
		m_nStun = 0;
		m_nStunTime = 0;
		m_IsStun = false;
		m_pStunGauge->SetIsStun(false);
		if (m_IsclearStun)
		{
			m_IsclearStun = false;
			m_Stun->Uninit();
		}

	}

	if (m_nStun > 0)
	{// スタンゲージがたまっているとき
		m_nStanRecoveryTime++;
	}

	if (m_nStanRecoveryTime >= STUN_RECOVERY_TIME && !m_IsStun)
	{// 一定時間でスタンゲージ回復
		m_nStun--;
		m_nStanRecoveryTime = 0;

		if (m_nStun <= 0)
		{
			m_nStun = 0;
		}
	}
}

//--------------------------------------------------
// 弾を撃つ処理
//--------------------------------------------------
void CPlayer::Shoot(const D3DXVECTOR3& pos)
{
	// インプット
	CInput* pInput = CInput::GetKey();

	int playerNum = CManager::GetManager()->GetPlayerNumber();

	{// スティックの方向を取ってくる
		D3DXVECTOR3 stick = pInput->VectorMoveJoyStick(m_nPlayerNumber, true);

		if ((stick.x == 0.0f) && (stick.y == 0.0f) && (stick.z == 0.0f) && (playerNum == 1))
		{
			D3DXVECTOR3 mousePos = pInput->GetMouseCursor();
			D3DXVECTOR3 playerScreenPos = br::GetWorldToScreenPos(m_pos);							// プロジェクションマトリックス

			stick = mousePos - playerScreenPos;
#ifdef _DEBUG
			CDebugProc::Print("プレイヤースクリーン座標 %f,%f,%f\n", playerScreenPos.x, playerScreenPos.y, playerScreenPos.z);
			CDebugProc::Print("マウスカーソル座標 %f,%f,%f\n", mousePos.x, mousePos.y, mousePos.z);
			CDebugProc::Print("差分座標 %f,%f,%f\n", stick.x, stick.y, stick.z);
#endif // DEBUG
		}

		D3DXVec3Normalize(&stick, &stick);

		if ((stick.x == 0.0f) && (stick.y == 0.0f) && (stick.z == 0.0f))
		{// スティックが倒れていなかったら
			m_pArrow->SetCol(D3DXCOLOR(0.0f, 0.0f, 0.0f, 0.0f));
			return;
		}
		else
		{
			// チャージ時間
			m_nChargeTime++;
			m_bIsCharge = true;
			m_pArrow->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));

			if (m_nChargeTime > CHARGE_TIME)
			{
				m_nChargeTime = CHARGE_TIME;
			}

			// スティックの角度の計算
			float stickAngle = atan2f(-stick.y, stick.x);
			if (stickAngle < 0.0f)
			{// 角度の正規化
				stickAngle += (D3DX_PI * 2.0f);
			}
			// 範囲
			float range = (D3DX_PI * 2.0f) / 8.0f;
			// 今倒しているスティックの方向
			int direction = (int)(stickAngle / range + 0.5f);
			// 弾の角度
			float bulletAnge = direction * range;
			m_vec.x = cosf(bulletAnge);
			m_vec.y = sinf(bulletAnge);
			m_vec.z = 0.0f;

			D3DXVec3Normalize(&m_vec, &m_vec);

			m_pArrow->SetPos(m_pos + m_vec * 100.0f);
			m_pArrow->SetRot(D3DXVECTOR3(0.0f, 0.0f, bulletAnge - D3DX_PI / 2.0f));
		}
	}

	{
		// 弾の大きさ
		float bulletSize = m_nChargeTime * 0.1f;

		// 弾の残り数が0以下の場合はデフォルトの弾に戻す
		if (m_remainingBullets <= 0)
		{
			m_Ui->SetType(BULLET_TYPE::DEFAULT);
			m_bulletType = BULLET_TYPE::DEFAULT;
			m_remainingBullets = 999;
		}

		//if (playerNum == 1)
		//{
		//	m_isMouse = ;
		//}

		if (pInput->Trigger(KEY_SHOT, m_nPlayerNumber) || pInput->Trigger(MOUSE_INPUT_LEFT))
		{
			CMotion* pMotion = CMotionModel3D::GetMotion();
			if (pMotion != nullptr && m_action != TYPE_ATTACK)
			{//アニメーション設定
				m_IsAnime = true;
				m_action = TYPE_ATTACK;
				pMotion->SetNumMotion(m_action);
			}

			//サウンド
			CManager::GetManager()->GetSound()->Play(CSound::LABEL_SE_BULLET);
			m_nChargeTime = 0;

			// 弾のタイプに応じて処理を行う
			switch (m_bulletType)
			{
			case BULLET_TYPE::DEFAULT:
				// デフォルトの弾を作成し、位置を設定する
				CBullet::Create(pos, m_vec * 10.0f, D3DXVECTOR3(10.0f + bulletSize, 10.0f + bulletSize, 0.0f), m_nPlayerNumber);
				//m_remainingBullets--;
				break;

			case BULLET_TYPE::RANGE_EXTENSION:
				// 射程延長の弾を作成し、位置を設定する
				CBullet::Create(pos, m_vec * 10.0f, D3DXVECTOR3(10.0f, 10.0f, 0.0f), m_nPlayerNumber, CBullet::BULLET_TYPE::RANGE_EXTENSION);
				//m_remainingBullets--;

				break;

			case BULLET_TYPE::SHOTGUN:
			{
				// ジョイスティックの方向角度を計算する
				float joystick_angle = atan2(-m_vec.y, m_vec.x);
				// ショットガンを生成する
				constexpr int num_bullets = 4;  // ショットガンの数
				constexpr float spread = D3DX_PI / 8;  // ショットガンの全角度（この例では、ショットガンは45度の角度で発射されます）
				for (int i = 0; i < num_bullets; i++) {
					// このショットガンのオフセット角度を計算する
					float bullet_angle_offset = spread * ((float)i / (num_bullets - 1) - 0.5f);
					// このショットガンの最終的な方向を計算する
					float bullet_angle = joystick_angle + bullet_angle_offset;
					// 新しい方向ベクトルを計算する
					D3DXVECTOR3 bullet_vec(cos(bullet_angle), -sin(bullet_angle), 0.0f);
					// 新しい方向ベクトルを使用してショットガンの弾を作成する
					CBullet::Create(pos, bullet_vec * 10.0f, D3DXVECTOR3(10.0f, 10.0f, 0.0f), m_nPlayerNumber, CBullet::BULLET_TYPE::SHOTGUN);
				}

				//m_remainingBullets--;
			}
			break;

			case BULLET_TYPE::STUN_BULLET:
				// スタン弾を作成し、位置を設定する
				CBullet::Create(pos, m_vec * 10.0f, D3DXVECTOR3(10.0f, 10.0f, 0.0f), m_nPlayerNumber, CBullet::BULLET_TYPE::STUN_BULLET)->SetPos(pos);
				m_remainingBullets--;
				break;

			default:
				assert(false);
				break;
			}
		}
	}
}

//---------------------------------------------------------------------------
//　打つ玉のType設定
//---------------------------------------------------------------------------
void CPlayer::SetShootType(BULLET_TYPE List)
{
	m_bulletType = List;
	m_Ui->SetType(List);
}

//---------------------------------------------------------------------------
// 近接攻撃
//---------------------------------------------------------------------------
void CPlayer::Attack()
{
	CMotion* pMotion = CMotionModel3D::GetMotion();
	if (pMotion != nullptr && m_action != TYPE_ATTACK)
	{//アニメーション設定
		m_IsAnime = true;
		m_action = TYPE_ATTACK;
		pMotion->SetNumMotion(m_action);
	}
	// スタン弾を作成し、位置を設定する
	CBullet::Create(GetPos(), m_vec * 10.0f, D3DXVECTOR3(10.0f, 10.0f, 0.0f), m_nPlayerNumber, CBullet::BULLET_TYPE::ATTACK);
}

//--------------------------------------------------
// 操作
//--------------------------------------------------
void CPlayer::Control_()
{
	CMotion* pMotion = CMotionModel3D::GetMotion();
	// インプット
	CInput* pInput = CInput::GetKey();

	D3DXVECTOR3 vec(0.0f, 0.0f, 0.0f);

	D3DXVECTOR3 rotDest = GetRotDest();

	if ((vec.x == 0.0f) && (vec.y == 0.0f))
	{
		if (pInput->Press(KEY_MOVE_LEFT_LEFT, m_nPlayerNumber) && !m_IsStun)
		{// 左
			vec.x += -1.0f;
			m_IsAnime = true;
			m_Diff++;
			if (m_Diff >= 10)
			{
				m_Diff = 0;
				CManager::GetManager()->GetSound()->Play(CSound::LABEL_SE_DASH);	//足音
				CEffect::Create(GetPos() + D3DXVECTOR3(20.0f, -20.0f, 0.0f), D3DXVECTOR3(0.0f, 0.0f, 0.0f), D3DXVECTOR3(20.0f, 20.0f, 0.0f), CParticle::MOVE_OTO, PRIORITY_EFFECT, CEffect::OTO_DA, 20);
			}
			if (pMotion != nullptr && (m_action != TYPE_MOVE))
			{//アニメーション設定

				m_action = TYPE_MOVE;
				pMotion->SetNumMotion(m_action);

			}
		}
		if (pInput->Press(KEY_MOVE_LEFT_RIGHT, m_nPlayerNumber) && !m_IsStun)
		{// 右
			m_Diff++;
			if (m_Diff >= 10)
			{
				m_Diff = 0;
				CManager::GetManager()->GetSound()->Play(CSound::LABEL_SE_DASH);	//足音
				CEffect::Create(GetPos() + D3DXVECTOR3(-20.0f, -20.0f, 0.0f), D3DXVECTOR3(0.0f, 0.0f, 0.0f), D3DXVECTOR3(20.0f, 20.0f, 0.0f), CParticle::MOVE_OTO, PRIORITY_EFFECT, CEffect::OTO_DA, 20);
			}
			vec.x += 1.0f;
			m_IsAnime = true;
			if (pMotion != nullptr && m_action != TYPE_MOVE)
			{//アニメーション設定
				CManager::GetManager()->GetSound()->Play(CSound::LABEL_SE_DASH);	//足音
				m_action = TYPE_MOVE;
				pMotion->SetNumMotion(m_action);

			}
		}
	}

	D3DXVECTOR3 pos = CMotionModel3D::GetPos();
	D3DXVECTOR3 rot = CMotionModel3D::GetRot();

	if (vec.x != 0.0f || vec.y != 0.0f)
	{// 移動とそれに合わせた回転
	 // ベクトルの正規化
		D3DXVec3Normalize(&vec, &vec);
		// キーボード
		if (m_Moveboost != 0.0f)
		{
			//CManager::GetManager()->GetSound()->Play(CSound::LABEL_SE_DASH);	//足音
			pos += (vec * PLAYER_SPEED) * m_Moveboost;
		}
		else
		{
			//CManager::GetManager()->GetSound()->Play(CSound::LABEL_SE_DASH);	//足音
			pos += (vec * PLAYER_SPEED);
		}
		CMotionModel3D::SetPos(pos);
	}

	if (vec != D3DXVECTOR3(0.0f, 0.0f, 0.0f))
	{// プレイヤーが移動した方向に向きを合わせる
		m_IsAnime = true;
		if (pMotion != nullptr && m_action != TYPE_MOVE)
		{//アニメーション設定
			m_action = TYPE_MOVE;
			pMotion->SetNumMotion(m_action);

		}
		if (vec != D3DXVECTOR3(0.0f, 0.0f, 0.0f))
		{
			m_rotDest.y = atan2f(vec.x / 32767.0f, vec.y / 32767.0f);
		}

		m_rotDest.y += D3DX_PI;
	}

	// 目的の角度の正規化
	br::NormalizeAngleDest(&rot.y, &m_rotDest.y);

	// 目的の角度を現在の角度に近づける
	rot.y += (m_rotDest.y - rot.y) * 0.2f;

	SetRot(rot);
	// 現在の角度の正規化
	br::NormalizeAngle(&rot.y);

	Jump_();

	if (!pMotion->GetMotion())
	{//全てのkeyがおしてない

		m_action = TYPE_NEUTRAL;
		pMotion->SetNumMotion(m_action);
	}

	// 弾の発射
	Shoot(pos);

	if (pInput->Trigger(JOYPAD_R2, m_nPlayerNumber) ||
		pInput->Trigger(MOUSE_INPUT_RIGHT) ||
		pInput->Trigger(DIK_RETURN, m_nPlayerNumber))
	{
		Attack();
	}
}

//--------------------------------------------------
// モーション
//--------------------------------------------------
void CPlayer::Motion_()
{
	// モーションカウンターを進める
	m_nCountMotion++;

	if (m_nCountMotion >= m_ModelData[0].KeySet[m_nCurrentKey].nFrame)
	{
		m_nCurrentKey++;
		m_nCountMotion = 0;

		if (m_nCurrentKey >= m_nNumKey)
		{
			m_nCurrentKey = 0;
		}
	}
}

//---------------------------------------------------------------------------
// ブースター
//---------------------------------------------------------------------------
void CPlayer::Boost(int Time, float boost)
{
	m_Moveboost = boost;
	m_MoveboostTime = Time;
	m_Ui->SetBoost(Time);
}

//---------------------------------------------------------------------------
// たま
//---------------------------------------------------------------------------
void CPlayer::ShotBoost(int Time)
{
	m_Shottimer = Time;
}

//=============================================================================
// 当たり判定
//=============================================================================
bool CPlayer::Collision(const D3DXVECTOR3 /*InPos*/, const D3DXVECTOR3 /*InSize*/, CBlock* Block)
{
	D3DXVECTOR3 Size = GetSize();
	D3DXVECTOR3 Pos = GetPos();

	hitevent(Block);

	return false;
}

//=============================================================================
// ヒット
//=============================================================================
bool CPlayer::hitevent(CBlock* Block)
{
	CModelData* Model = CManager::GetManager()->GetModelData();
	D3DXVECTOR3 Size = Model->GetModel(Block->GetModelData()).size;
	D3DXVECTOR3 Pos = Block->GetPos();
	D3DXVECTOR3 PosOld = Block->GetPosOld();
	D3DXVECTOR3 PlayerSize = D3DXVECTOR3(20.0f, 20.0f, 50.0f);
	D3DXVECTOR3 PlayerPos = GetPos();
	//float Difference = 1.0f;

	////左右の壁
	//if (Pos.y + Size.y  > PlayerPos.y - PlayerSize.y 
	//	&& PosOld.y - Size.y < PlayerPos.y + PlayerSize.y)
	//{
	//	if (Pos.x + Size.x  > PlayerPos.x - PlayerSize.x
	//		&& PosOld.x + Size.x  < PlayerPos.x - PlayerSize.x)
	//	{//ブロックの座標と座標が重なり合ったら//通常モード//左
	//		
	//		PlayerPos.x = Pos.x + Size.x + PlayerSize.x;
	//	}
	//	if (Pos.x - Size.x  < PlayerPos.x + PlayerSize.x
	//		&& PosOld.x - Size.x > PlayerPos.x + PlayerSize.x)
	//	{//ブロックの座標と座標が重なり合ったら//通常モード//右
	//		PlayerPos.x = Pos.x - Size.x - PlayerSize.x;
	//	}
	//}
	////上と下のカベ処理
	//if (Pos.x + Size.x > PlayerPos.x - PlayerSize.x
	//	&& Pos.x - Size.x  < PlayerPos.x + PlayerSize.x)
	//{
	//	if (Pos.y - Size.y  < PlayerPos.y + PlayerSize.y
	//		&& PosOld.y - Size.y >= PlayerPos.y + PlayerSize.y)
	//	{//ブロックの座標と座標が重なり合ったら//通常モード//下
	//	
	//	
	//		PlayerPos.y = Pos.y + Size.y + PlayerSize.y + Difference;
	//	}
	//	if (Pos.y + Size.y  > PlayerPos.y - PlayerSize.y
	//		&& PosOld.y + Size.y <= PlayerPos.y - PlayerSize.y)
	//	{//ブロックの座標と座標が重なり合ったら//通常モード//上
	//		PlayerPos.y = Pos.y - Size.y - PlayerSize.y *- 1.0f;
	//		
	//	}
	//}
	//SetPos(PlayerPos);
	return false;
}


//=============================================================================
// 画面外
//=============================================================================
void  CPlayer::Outarea()
{
	D3DXVECTOR3 PlayerPos = GetPos();
	D3DXVECTOR3 worldPos = D3DXVECTOR3((float)CManager::SCREEN_WIDTH, (float)CManager::SCREEN_HEIGHT, 0.0f);
	worldPos.x += 150.0f;
	bool IsHit = false;
	if (PlayerPos.x <= -worldPos.x / 2)
	{
		IsHit = true;
		CParticle::Create(PlayerPos, D3DXVECTOR3(10.0f, 0.0f, 0.0f), 100, CParticle::MOVE_DEF, PRIORITY_BG);
		CManager::GetManager()->GetSound()->Play(CSound::LABEL_SE_SPLASH);					//爆発音
	}

	if (PlayerPos.x >= worldPos.x / 2)
	{
		IsHit = true;
		CParticle::Create(PlayerPos, D3DXVECTOR3(-10.0f, 0.0f, 0.0f), 100, CParticle::MOVE_DEF, PRIORITY_BG);
		CManager::GetManager()->GetSound()->Play(CSound::LABEL_SE_SPLASH);					//爆発音
	}
	if (PlayerPos.y >= worldPos.y)
	{
		IsHit = true;
		CParticle::Create(PlayerPos, D3DXVECTOR3(0.0f, -10.0f, 0.0f), 100, CParticle::MOVE_DEF, PRIORITY_BG);
		CManager::GetManager()->GetSound()->Play(CSound::LABEL_SE_SPLASH);					//爆発音
	}

	if (PlayerPos.y <= -worldPos.y)
	{
		IsHit = true;
		CParticle::Create(PlayerPos, D3DXVECTOR3(0.0f, 10.0f, 0.0f), 100, CParticle::MOVE_DEF, PRIORITY_BG);
		CManager::GetManager()->GetSound()->Play(CSound::LABEL_SE_SPLASH);					//爆発音
	}
	if (IsHit)
	{

		CMap* Map = nullptr;
		if (CGame::GetGame() != nullptr)
		{
			Map = CGame::GetGame()->GetMap();
		}
		else if (CTutorial::GetTutorial() != nullptr)
		{
			Map = CTutorial::GetTutorial()->GetMap();
		}

		Map->SetNowMAX(Map->GetNowMAX() - 1);

		if (Map->GetNowMAX() <= 1)
		{
			if (Map->GetNowMAX() <= 0)
			{
				CManager::MODE mode = CManager::GetGameMode();
				if ((mode == CManager::MODE_GAME))
				{
					if (CGame::GetGame()->GetScore() != nullptr)
					{
						CGame::GetGame()->SetStagr(false);
						CGame::GetGame()->GetScore()->SetScore(0);
						CJudgment::Create(false);
					}
				}
				else
				{
					CFade::GetInstance()->SetFade(CManager::MODE_MAP_SELECT);
				}
			}
			else
			{
				CManager::MODE mode = CManager::GetGameMode();
				if ((mode == CManager::MODE_GAME))
				{
					CJudgment::Create(true);
				}
				else
				{
					CFade::GetInstance()->SetFade(CManager::MODE_MAP_SELECT);
				}
			}


		}

		Uninit();
	}

}