//============================
//
// エフェクト
// Author:hamada ryuuga
//
//============================

//**************************************************
// include
//**************************************************
#include <assert.h>
#include "manager.h"
#include "particle.h"
#include "effect.h"
#include "utility.h"
#include "bullet.h"
//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CParticle::CParticle(int nPriority /* =0 */) : CObject3D(nPriority)
{
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CParticle::~CParticle()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CParticle::Init()
{
	/*m_Main = CObject3D::Create(GetPos(),
	D3DXVECTOR3(100.0f, 100.0f, 0.0f), PRIORITY_BG);
*/
	switch (m_Mode)
	{
	case CParticle::MOVE_DEF:
		for (int i = 0; i < 100; i++)
		{
			float R = br::FloatRandam(1.0f, 0.5f);
			float G = br::FloatRandam(1.0f, 0.5f);
			float B = br::FloatRandam(1.0f, 0.5f);
			float X = br::FloatRandam(50.0f, -50.0f);
			float Y = br::FloatRandam(50.0f, -50.0f);
			m_Move.x = X;
			m_Move.y = Y;
			D3DXVECTOR3 pos = GetPos();
			CEffect::Create(pos, m_Move, D3DXVECTOR3(10.0f, 10.0f, 0), m_Mode, PRIORITY_BG)->SetCol(D3DXCOLOR(R, G, B, 1.0f));
		}
		break;
	case CParticle::MOVE_DAMAGE:
	{
		D3DXVECTOR3 pos = GetPos();
		CEffect::Create(pos, m_Move, D3DXVECTOR3(50.0f, 50.0f, 0), m_Mode, PRIORITY_BG);
	}
		break;
	case CParticle::MOVE_HANABI:
	{
		D3DXVECTOR3 pos = GetPos();
		D3DXVECTOR3 vec = m_Move;
		
		vec.x *= -1;
		// ジョイスティックの方向角度を計算する
		float joystick_angle = atan2(vec.y, vec.x);
		// ショットガンを生成する
		constexpr int num_bullets = 10;  // ショットガンの数
		constexpr float spread = D3DX_PI / 8;  // ショットガンの全角度（この例では、ショットガンは45度の角度で発射されます）
		for (int i = 0; i < num_bullets; i++)
		{
			float Rand = br::FloatRandam(2, 1);

			float R = br::FloatRandam(1.0f, 0.5f);
			float G = br::FloatRandam(1.0f, 0.5f);
			float B = br::FloatRandam(1.0f, 0.5f);
			
			// このショットガンのオフセット角度を計算する
			float bullet_angle_offset = spread * ((float)i / (num_bullets - 1) - 0.5f);
			// このショットガンの最終的な方向を計算する
			float bullet_angle = joystick_angle + bullet_angle_offset;
			// 新しい方向ベクトルを計算する
			D3DXVECTOR3 bullet_vec(cos(bullet_angle), -sin(bullet_angle), 0.0f);
			// 新しい方向ベクトルを使用してショットガンの弾を作成する
			CEffect*effect = CEffect::Create(pos, bullet_vec * (5.0f + (2 * Rand)), D3DXVECTOR3(15.0f, 15.0f, 0.0f), m_Mode, PRIORITY_BG);
			effect->SetTexture(CTexture::TEXTURE_EFFECT4);
			effect->SetCol(D3DXCOLOR(R,G,B,1.0f));
		}	
		
	}
		break;
	
	break;
	default:
		break;
	}
	
	return S_OK;
}

//--------------------------------------------------
// 終了
//--------------------------------------------------
void CParticle::Uninit()
{
	CObject3D::Uninit();
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CParticle::Update()
{
	m_Life--;
	if (m_Life <= 0)
	{
		Uninit();
	}
	
}
	

//--------------------------------------------------
// 描画
//--------------------------------------------------
void CParticle::Draw(DRAW_MODE /*drawMode*/)
{
	
}

//--------------------------------------------------
// 生成関数
//--------------------------------------------------
CParticle * CParticle::Create(D3DXVECTOR3 pos, D3DXVECTOR3 move, int Life,MOVE mode,int nPriority)
{
	CParticle *pObject;
	pObject = new CParticle(nPriority);

	if (pObject != nullptr)
	{
		pObject->SetPos(pos);
		pObject->SetMove(move);
		pObject->SetLife(Life);
		pObject->SetMode(mode);

		pObject->Init();
	}
	else
	{
		assert(false);
	}

	return pObject;
}

void CParticle::SetItem(CTexture::TEXTURE tex)
{

	D3DXVECTOR3 pos = GetPos();
	D3DXVECTOR3 vec = m_Move;
	int MAX_PARTICLE = 10;
	float PARTICLE_MOVE = 5.0f;
	for (int i = 0; i < MAX_PARTICLE; i++)
	{
		float rot = (D3DX_PI * 2.0f) / MAX_PARTICLE * i;

		// 角度の正規化
		br::NormalizeAngle(&rot);

		D3DXVECTOR3 randomPos = D3DXVECTOR3(sinf(rot), cosf(rot), 0.0f) * br::FloatRandam(100.0f, 50.0f);

		float random = br::FloatRandam(PARTICLE_MOVE, PARTICLE_MOVE * 0.1f);
		D3DXVECTOR3 move = { 0.0f,0.0f,0.0f };

		move.x = sinf(rot) * random;
		move.y = cosf(rot) * random;

		CEffect::Create(pos, move, D3DXVECTOR3(15.0f, 15.0f, 0), m_Mode, PRIORITY_BG)->SetEffect(tex);
		
	}
}

//=========================================
// 軌跡の色変更
//=========================================
D3DXCOLOR CParticle::SetPlayerNumColor(int Is)
{
	int num = Is;
	switch (num)
	{
	case 0://P1
		   //水色
		return D3DXCOLOR(0.7f, 1.0f, 1.0f, 1.0f);
		break;
	case 1://P2
		   //あか
		return D3DXCOLOR(1.0f, 0.4f, 0.4f, 1.0f);
		break;
	case 2://P3
		   //黄色
		return D3DXCOLOR(1.0f, 1.0f, 0.4f, 1.0f);
		break;
	case 3://P4
		   //緑
		return D3DXCOLOR(0.78f, 1.0f, 0.7f, 1.0f);
		break;
	case 99://NPC
		return D3DXCOLOR(0.0f, 0.0f, 0.0f, 1.0f);
		break;
	default:
		break;
	}
}
