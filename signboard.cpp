//==================================================
// signboard.cpp
// Author: Buriya Kota
//==================================================
//**************************************************
// include
//**************************************************
#include "signboard.h"
#include "manager.h"
#include "input.h"
#include "utility.h"

#include "object2D.h"

#include "player.h"
#include "tutorial.h"
#include "text.h"

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CSignBoard::CSignBoard(int nPriority) : CBlock(nPriority), m_isPlayerHit(false)
{
	m_isHIt = false;
	m_isDisp = false;
	m_pText = nullptr;
	m_pObject2D = nullptr;
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CSignBoard::~CSignBoard()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CSignBoard::Init()
{
	CObjectX::Init();

	CObjectX::SetModelData(CModelData::MODEL_BOARD);

	return S_OK;
}

//--------------------------------------------------
// 終了
//--------------------------------------------------
void CSignBoard::Uninit()
{
	CObjectX::Uninit();
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CSignBoard::Update()
{
	CObjectX::Update();

	Display_();
}

//--------------------------------------------------
// 描画
//--------------------------------------------------
void CSignBoard::Draw(DRAW_MODE drawMode)
{
	CObjectX::Draw(drawMode);
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CSignBoard *CSignBoard::Create(const D3DXVECTOR3& pos, SIGN_BOARD_TYPE type)
{
	CSignBoard *pSignBoard;
	pSignBoard = new CSignBoard;

	if (pSignBoard != nullptr)
	{
		pSignBoard->Init();
		pSignBoard->SetPos(pos);
		pSignBoard->SetType(type);
	}
	else
	{
		assert(false);
	}

	return pSignBoard;
}

//--------------------------------------------------
// ヒット
//--------------------------------------------------
bool CSignBoard::hitevent(CPlayer* player)
{
	if (player->GetPlayerNumber() == 99)
	{
		return false;
	}

	CModelData* Model = CManager::GetManager()->GetModelData();

	D3DXVECTOR3 Size = Model->GetModel(GetModelData()).size;
	D3DXVECTOR3 Pos = GetPos();
	D3DXVECTOR3 PlayerSize = D3DXVECTOR3(20.0f, 20.0f, 50.0f);
	D3DXVECTOR3 PlayerPos = player->GetPos();

	if (((Pos.y - Size.y) <= (PlayerPos.y + PlayerSize.y)) &&
		((Pos.y + Size.y) >= (PlayerPos.y - PlayerSize.y)) &&
		((Pos.x - Size.x) <= (PlayerPos.x + PlayerSize.x)) &&
		((Pos.x + Size.x) >= (PlayerPos.x - PlayerSize.x)))
	{
		player->SetIsHitSignBoard(true);
	}
	else
	{
		if (m_isDisp)
		{
			m_pObject2D->Uninit();
			m_isDisp = false;
			m_isHIt = false;
			player->SetIsHitSignBoard(false);
		}
	}

	return false;
}

//--------------------------------------------------
// 文字の表示
//--------------------------------------------------
void CSignBoard::Display_()
{
	if (m_isHIt)
	{
		if (m_isDisp)
		{

		}
		else
		{
			D3DXVECTOR3 pos = br::GetWorldToScreenPos(GetPos());
			switch (m_signBoard)
			{
			case SIGN_BOARD_TYPE_MOVE:
				m_pObject2D = CObject2D::Create(D3DXVECTOR3(pos.x, pos.y - 200.0f, 0.0f), D3DXVECTOR3(300.0f, 120.0f, 0.0f));
				m_pObject2D->SetTexture(CTexture::TEXTURE_TUTORIAL3);

				break;

			case SIGN_BOARD_TYPE_JUNP:
				m_pObject2D = CObject2D::Create(D3DXVECTOR3(pos.x, pos.y - 200.0f, 0.0f), D3DXVECTOR3(300.0f, 120.0f, 0.0f));
				m_pObject2D->SetTexture(CTexture::TEXTURE_TUTORIAL2);

				break;

			case SIGN_BOARD_TYPE_SHOT:
				m_pObject2D = CObject2D::Create(D3DXVECTOR3(pos.x, pos.y - 200.0f, 0.0f), D3DXVECTOR3(300.0f, 120.0f, 0.0f));
				m_pObject2D->SetTexture(CTexture::TEXTURE_TUTORIAL_MOVE);

				break;
			default:
				break;
			}

			m_isDisp = true;
		}
	}
}
