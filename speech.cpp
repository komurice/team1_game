//==================================================
// bullet.cpp
// Author: Buriya Kota
//==================================================

//**************************************************
// include
//**************************************************
#include "speech.h"
#include "player.h"
#include "block.h"
#include "debug_proc.h"
#include "effect.h"
#include "particle.h"
#include "utility.h"

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CSpeech::CSpeech(int nPriority) : CObject3D(nPriority)
{
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CSpeech::~CSpeech()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CSpeech::Init()
{
	CObject3D::Init();
	SetBillboard(true);
	m_timer = 1.0f;
	m_life = 50;
	m_up = true;
	m_Size = 0.0f;
	return S_OK;
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CSpeech::Update()
{
	m_Size += 1.0f;

	D3DXVECTOR3 size{ m_Size,m_Size,0.0f };
	SetSize(size);
	
	D3DXVECTOR3 move{ -5.0f,0.0f,0.0f };
	

	D3DXVECTOR3 pos = GetPos();
	SetPos(pos + move);

	CObject3D::Update();

	
	if (m_up)
	{
		float color = 0.01f;
		m_timer -= color;
		
	}
	SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, m_timer));
	if (m_timer <= 0.0f)
	{
		Uninit();
	}

}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CSpeech* CSpeech::Create(const D3DXVECTOR3 pos, const D3DXVECTOR3 size,int type)
{
	CSpeech* pSpeech;
	pSpeech = new CSpeech(PRIORITY_PLAYER);

	if (pSpeech != nullptr)
	{
		pSpeech->Init();
		pSpeech->SetPos(pos);
		pSpeech->SetSize(size);
		pSpeech->SetTex(type);
	}
	else
	{
		assert(false);
	}

	return pSpeech;
}

//--------------------------------------------------
// テクスチャ
//--------------------------------------------------
void CSpeech::SetTex(int type)
{
	int cometype = br::IntRandom(7, 0);
	switch (cometype)
	{
	case 0:
		SetTexture(CTexture::TEXTURE_FIGHT);
		break;
	case 1:
		SetTexture(CTexture::TEXTURE_FOOL);
		break;
	case 2:
		SetTexture(CTexture::TEXTURE_MESUGAKI1);
		break;
	case 3:
		SetTexture(CTexture::TEXTURE_GAYA1);
		break;
	case 4:
		SetTexture(CTexture::TEXTURE_GAYA2);
		break;
	case 5:
		SetTexture(CTexture::TEXTURE_GAYA3);
		break;
	case 6:
		SetTexture(CTexture::TEXTURE_GAYA4);
		break;
	case 7:
		SetTexture(CTexture::TEXTURE_GAYA5);
		break;
	default:
		SetCol(D3DXCOLOR(0.0f, 0.0f, 0.0f, 0.0f));
		break;
	}

}
