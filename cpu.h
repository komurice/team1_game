//==================================================
// CPU.h
// Author: Buriya Kota
//==================================================
#ifndef _CPU_H_
#define _CPU_H_

//**************************************************
// インクルード
//**************************************************
#include "object.h"
#include "motion_model3D.h"
#include "bullet.h"
#include "player.h"

//**************************************************
// 前方前言　実態はNG　ポインタだけならOK
//**************************************************
class CLockOnCursor;
class CModel;
class CBlock;
class CStage_Ui;
class CObject3D;

//**************************************************
// 定数定義
//**************************************************
#define MAX_PARTS		(13)
#define MAX_KEYDATA		(2)
#define MAX_MOTION		(5)

//**************************************************
// 構造体定義
//**************************************************

//**************************************************
// クラス
//**************************************************
class CCpu : public CPlayer
{
public:
	
public:
	static const float PLAYER_SPEED;



private:
	enum EState
	{
		STATE_IDLE = 0,
		STATE_LAND,
		STATE_FLY,
		STATE_MAX,
		STATE_INVALID = -1,
	};
	enum MOVE_VEC
	{
		VEC_UP = 0,
		VEC_LEFT,
		VEC_RIGHT,
		VEC_SLOW_LEFT,
		VEC_SLOW_RIGHT,
		VEC_UPLEFT,
		VEC_UPRIGHT,

		VEC_MAX,
	};

private:
	static const UPDATE_FUNC mUpdateFunc[];

public:
	explicit CCpu(int nPriority = PRIORITY_PLAYER);
	~CCpu();

	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw(DRAW_MODE drawMode) override;

	//　プレイヤーのステート関数
	void Update_Idle();
	void Update_Land();
	void Update_Fly();

	static CCpu* Create(D3DXVECTOR3 pos, int number, int playerNumber);

	bool CCpu::Collision(const D3DXVECTOR3 PlayerPos, const D3DXVECTOR3 PlayerSize, CBlock* Block);

	void Plantarea();
private:
	void Outarea();
	void MoveRandom();
	void Control_();
	void InScreen_();
	void Recall_();
	void KnockBack_();
	void Jump_();
	void Stun_();

	void Shoot(const D3DXVECTOR3& pos, const D3DXVECTOR3& EnemyPos);

	void HitEnemy();
private:

	// ステート
	EState m_state;

	// 動き系変数
	int m_moveFrame;
	MOVE_VEC m_moveVec;
	bool m_isJump;
	float m_Speed;

	int m_attackTimer;
	bool m_isAttack;
	int m_delay;
};

#endif	// _CPU_H_#pragma once
