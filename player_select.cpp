//==================================================
// player_select.cpp
// Author: Buriya Kota
//==================================================

//**************************************************
// include
//**************************************************
#include "manager.h"
#include "input.h"
#include "fade.h"
#include "sound.h"
#include "bg.h"
#include "player_select.h"
#include "player.h"
#include "utility.h"
#include "camera.h"
#include "object3D.h"
#include "model_data.h"

//**************************************************
// 静的メンバ変数
//**************************************************
CPlayerSelect *CPlayerSelect::m_pPlayerSelect = nullptr;
const std::string fileName[5] = { "data/TEXT/motion_apple.txt","data/TEXT/motion_grape.txt","data/TEXT/motion_Orange.txt","data/TEXT/motion_strawberry.txt","data/TEXT/motion_cherry.txt" };

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CPlayerSelect::CPlayerSelect()
{
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CPlayerSelect::~CPlayerSelect()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CPlayerSelect::Init()
{
	// サウンド
	CManager::GetManager()->GetSound()->Play(CSound::LABEL_BGM_CHARSELECT);

	CBg* Bg = CBg::Create(D3DXVECTOR3(CManager::SCREEN_WIDTH * 0.5f, 50.0f, 10.0f), D3DXVECTOR3(50.0f, 100.0f, 0.0f), PRIORITY_BG);
	//Bg->SetModel(CModelData::MODEL_SUN, CModelData::MODEL_SUN, CBg::MOVE_WORLD);
	Bg->AddTex(CTexture::TEXTURE_BG1);
	Bg->AddTex(CTexture::TEXTURE_BG3);

	//(CTexture::TEXTURE_SELECTBG2, CTexture::TEXTURE_SELECTBG2, CTexture::TEXTURE_SELECTBG3);
	float Pos = CManager::SCREEN_WIDTH * 0.25f;
	D3DXVECTOR3 Size = D3DXVECTOR3(200.0f, 400.0f, 0.0f);
	m_IsPlayerselect = false;
	m_Change = false;

	/*BG = CObject2D::Create(D3DXVECTOR3((float)CM
	
	anager::SCREEN_WIDTH / 2, 370.0f, 0.0f),
		D3DXVECTOR3((float)CManager::SCREEN_WIDTH, (float)CManager::SCREEN_HEIGHT, 0.0f), PRIORITY_UI);
	BG->SetTexture(CTexture::TEXTURE_SELECTBG1);*/

	m_Stage = CObject2D::Create(D3DXVECTOR3((float)CManager::SCREEN_WIDTH / 2, 100.0f, 0.0f),
		D3DXVECTOR3(300.0f, 100.0f, 0.0f), PRIORITY_UI2D);
	m_Stage->SetTexture(CTexture::TEXTURE_STAGE);
	m_Stage->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.0f));

	m_List.IsApple = false;
	m_List.IsGrape = false;
	m_List.IsOrange = false;
	m_List.IsStrawberry = false;
	m_List.IsCherry = false;
	for (int i = 0; i < PlayerMax; i++)
	{
		m_SleepTime[i] = 0;
		m_Player[i] = nullptr;
		m_CharacterSelectUi[i] = nullptr;
		m_ResultUi[i] = nullptr;

		m_PlaySelectUi[i] = CObject2D::Create(D3DXVECTOR3((Pos*i)+ (Pos/2), CManager::SCREEN_HEIGHT * 0.5f, 0.0f),
			Size, PRIORITY_UI2D);
		m_PlaySelectUi[i]->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.5f));

		m_IsOK[i] =false;
		if (CManager::GetManager()->GetListSize() <= PlayerMax)
		{
			CManager::GetManager()->AddList(i);
		}
		else
		{
			CManager::GetManager()->SetList(i,i);
		}
		
	}
	m_Top = 0;
	m_Last = 1;
	m_Move = 0.0f;
	m_BgMove = 0.0f;

	CInput *pInput = CInput::GetKey();
	int playerNumber = pInput->GetIsConnectedNumMax();
	CManager::GetManager()->SetPlayerNumber(playerNumber);

	for (int i = 0; i < playerNumber; i++)
	{
		m_PlaySelectUi[i]->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.5f));
	}

	m_PlaySelectUi[0]->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
	m_PlaySelectUi[0]->SetTexture(CTexture::TEXTURE_PLAYERNNUMBER);
	m_PlaySelectUi[1]->SetTexture(CTexture::TEXTURE_PLAYERNNUMBER1);
	m_PlaySelectUi[2]->SetTexture(CTexture::TEXTURE_PLAYERNNUMBER2);
	m_PlaySelectUi[3]->SetTexture(CTexture::TEXTURE_PLAYERNNUMBER3);

	m_ReadyCheck = 0;
	m_PlayPlayer = 0;

	m_CharacterList.push_back(0);
	m_CharacterList.push_back(1);
	m_CharacterList.push_back(2);
	m_CharacterList.push_back(3);


	return S_OK;
}

//--------------------------------------------------
// 終了
//--------------------------------------------------
void CPlayerSelect::Uninit()
{
	//サウンドストップ
	CManager::GetManager()->GetSound()->Stop();

	CObject::DeletedObj();
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CPlayerSelect::Update()
{
	//BG->SetTex(D3DXCOLOR(0.0f + m_BgMove, 1.0f + m_BgMove, 0.0f, 1.0f));
	if (!m_IsPlayerselect)
	{
		CPlayerSelect::NumberSelect();
	}
	else
	{
		CPlayerSelect::CharacterSelect();
	}

}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CPlayerSelect *CPlayerSelect::Create()
{
	m_pPlayerSelect = new CPlayerSelect;

	if (m_pPlayerSelect != nullptr)
	{
		m_pPlayerSelect->Init();
	}
	else
	{
		assert(false);
	}

	return m_pPlayerSelect;
}

//--------------------------------------------------
// キャラクター選択
//--------------------------------------------------
void CPlayerSelect::CharacterSelect()
{
	if (!m_Change)
	{//UIの変更
		m_Move += 0.5f;
		for (int i = 0; i < PlayerMax; i++)
		{//今までのやつを下に送る
			D3DXVECTOR3 PlayPos = m_PlaySelectUi[i]->GetPos();
			
			
			if (PlayPos.y >= CManager::SCREEN_HEIGHT + 200.0f)
			{
				m_Change = true;
				m_Select = false;
				m_Move = 0.0f;
			}
			PlayPos.y += m_Move;
			m_PlaySelectUi[i]->SetPos(PlayPos);
		
		}
		for (int i = 0; i < m_PlayPlayer + 1; i++)
		{//Playerのやつを召喚
			D3DXVECTOR3 CharacterPos = m_CharacterSelectUi[i]->GetPos();
			CharacterPos.y += m_Move;
			m_CharacterSelectUi[i]->SetPos(CharacterPos);

			D3DXVECTOR3 worldPos = br::WorldCastScreen(&CharacterPos,			// スクリーン座標
				D3DXVECTOR3((float)CManager::SCREEN_WIDTH, (float)CManager::SCREEN_HEIGHT, 0.0f),									// スクリーンサイズ
				CManager::GetManager()->GetCamera()->GetViewMatrix(),									// ビューマトリックス
				CManager::GetManager()->GetCamera()->GetProjMatrix());	// プロジェクションマトリックス


			m_CharacterSelectBg[i]->SetPos(D3DXVECTOR3(worldPos.x, worldPos.y,0.0f));
			m_CharacterSelectBg[i]->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
			m_Player[i]->SetPos(D3DXVECTOR3(worldPos.x, worldPos.y, 0.0f));
		}
	}
	else
	{
		for (int i = 0; i < m_PlayPlayer + 1; i++)
		{//Playerの位置固定めんどいしね
			D3DXVECTOR3 CharacterPos = m_CharacterSelectUi[i]->GetPos();
			D3DXVECTOR3 worldPos = br::WorldCastScreen(&CharacterPos,			// スクリーン座標
				D3DXVECTOR3((float)CManager::SCREEN_WIDTH, (float)CManager::SCREEN_HEIGHT, 0.0f),									// スクリーンサイズ
				CManager::GetManager()->GetCamera()->GetViewMatrix(),									// ビューマトリックス
				CManager::GetManager()->GetCamera()->GetProjMatrix());								// プロジェクションマトリックス
			
			m_Player[i]->SetPos(D3DXVECTOR3(worldPos.x, worldPos.y,0.0f));
		}

		for (int i = 0; i < m_PlayPlayer + 1; i++)
		{
			CInput *pInput = CInput::GetKey();

			
			if (pInput->Trigger(KEY_LEFT, i) && !m_IsOK[i])
			{// 左
			 //プレイヤーのモデルを変更する
				m_SleepTime[i] = 0;
				m_CharacterList[i]++;
				m_CharacterList[i] = SelectSort(m_CharacterList[i],true);
			
				if (m_CharacterList[i] > FRUIT_MAX - 1)
				{//最大数超えたら
					m_CharacterList[i] =  m_Top;
				}
				CManager::GetManager()->SetList(i, m_CharacterList[i]);
				m_Player[i]->SetMotion(fileName[m_CharacterList[i]].c_str());

			}

			if (pInput->Trigger(KEY_RIGHT, i) && !m_IsOK[i])
			{// 右
			 //プレイヤーのモデルを変更する
				m_CharacterList[i]--;
				if (m_CharacterList[i] < 0)
				{//最大数超えたら
					m_CharacterList[i] = FRUIT_MAX  - m_Last;
				}
				m_CharacterList[i] = SelectSort(m_CharacterList[i], false);
			
				CManager::GetManager()->SetList(i, m_CharacterList[i]);
				m_Player[i]->SetMotion(fileName[m_CharacterList[i]].c_str());


			}

			if ((pInput->Trigger(KEY_DECISION, i) || pInput->Trigger(MOUSE_INPUT_LEFT)) && !m_IsOK[i] && m_Change)
			{// 決定音
			 //決定した時かぶらないようにする処理
				switch (m_CharacterList[i])
				{
				case FRUIT_APPLE:
					if (!m_List.IsApple)
					{
						m_IsOK[i] = true;
					}
					m_List.IsApple = true;
					break;
				case FRUIT_GRAPE:
					if (!m_List.IsGrape)
					{
						m_IsOK[i] = true;
					}
					m_List.IsGrape = true;
					break;
				case FRUIT_ORANGE:
					if (!m_List.IsOrange)
					{
						m_IsOK[i] = true;
					}

					m_List.IsOrange = true;

					break;
				case FRUIT_STRAWBERRY:
					if (!m_List.IsStrawberry)
					{
						m_IsOK[i] = true;
					}

					m_List.IsStrawberry = true;

					break;
				case FRUIT_CHERRY:
					if (!m_List.IsCherry)
					{
						m_IsOK[i] = true;
					}
					m_List.IsCherry = true;
					break;
				default:
					break;
				}

				if (m_IsOK[i])
				{
					CManager::GetManager()->GetSound()->Play(CSound::LABEL_SE_ENTER);
					SetList();
					m_ReadyCheck++;

					D3DXVECTOR3 Pos = m_Player[i]->GetPos();

					m_ResultUi[i]->SetPos(D3DXVECTOR3(Pos.x + CManager::SCREEN_WIDTH / 2.0f, Pos.y + 500.0f, 0.0f));
					m_ResultUi[i]->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
				}

			}
			if ((pInput->Trigger(KEY_BACK, i) || pInput->Trigger(MOUSE_INPUT_RIGHT)) && !m_IsOK[0]&& !m_IsOK[1] &&!m_IsOK[2] && !m_IsOK[3])
			{//人数の画面に選択戻す
			
				m_Select = false;
				m_Move = 0.0f;
				m_IsPlayerselect = false;
			}
			if ((pInput->Trigger(KEY_BACK, i) || pInput->Trigger(MOUSE_INPUT_RIGHT)) &&m_IsOK[i])
			{// キャンセルモデル決定した時

				switch (m_CharacterList[i])
				{
				case FRUIT_APPLE:
					m_List.IsApple = false;
					break;
				case FRUIT_GRAPE:
					m_List.IsGrape = false;

					break;
				case FRUIT_ORANGE:
					m_List.IsOrange = false;

					break;
				case FRUIT_STRAWBERRY:
					m_List.IsStrawberry = false;
					break;
				case FRUIT_CHERRY:
					m_List.IsCherry = false;
					break;
				default:
					break;
				}

				SetList();
				m_IsOK[i] = false;
				m_ReadyCheck--;
				m_ResultUi[i]->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.0f));
			}

			if (m_ReadyCheck >= m_PlayPlayer + 1)
			{
				m_Stage->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
			}
			else
			{
				m_Stage->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.0f));
			}
			
			// スタート
			if (m_ReadyCheck >= m_PlayPlayer + 1 && (pInput->Trigger(KEY_PAUSE, i) || pInput->Trigger(MOUSE_INPUT_LEFT)))
			{//ゲームを開始したとき選択したプレイヤーの人数を超えてた時
				CManager::GetManager()->GetSound()->Play(CSound::LABEL_SE_ENTER2);
				CFade::GetInstance()->SetFade(CManager::MODE_MAP_SELECT);
			}
		}
	}
}

//--------------------------------------------------
// にんずう選択
//--------------------------------------------------
void CPlayerSelect::NumberSelect()
{
	CInput *pInput = CInput::GetKey();

	int playerNumber = pInput->GetIsConnectedNumMax();
	CManager::GetManager()->SetPlayerNumber(m_PlayPlayer + 1);


	if (!m_Select)
	{//にんずう選択の変更
		m_Move += 0.5f;
		for (int i = 0; i < PlayerMax; i++)
		{//今までのやつを下に送る
			D3DXVECTOR3 PlayPos = m_PlaySelectUi[i]->GetPos();
			
			
			if (PlayPos.y <= CManager::SCREEN_HEIGHT * 0.5f)
			{
				m_Select = true;
				m_Change = false;
				m_Move = 0.0f;
			}
			PlayPos.y -= m_Move;
			m_PlaySelectUi[i]->SetPos(PlayPos);
		
		}
		for (int i = 0; i < m_PlayPlayer + 1; i++)
		{//Playerのやつを召喚
			if (m_CharacterSelectUi[i] != nullptr)
			{
				D3DXVECTOR3 CharacterPos = m_CharacterSelectUi[i]->GetPos();

				CharacterPos.y -= m_Move;
				m_CharacterSelectUi[i]->SetPos(CharacterPos);
				D3DXVECTOR3 worldPos = br::WorldCastScreen(&CharacterPos,			// スクリーン座標
					D3DXVECTOR3((float)CManager::SCREEN_WIDTH, (float)CManager::SCREEN_HEIGHT, 0.0f),									// スクリーンサイズ
					CManager::GetManager()->GetCamera()->GetViewMatrix(),									// ビューマトリックス
					CManager::GetManager()->GetCamera()->GetProjMatrix());								// プロジェクションマトリックス

				m_CharacterSelectBg[i]->SetPos(D3DXVECTOR3(worldPos.x, worldPos.y, 0.0f));
				m_Player[i]->SetPos(D3DXVECTOR3(worldPos.x, worldPos.y, 0.0f));
			}
		}
	}

	if (pInput->Trigger(KEY_LEFT) && m_Select)
	{//プレイヤーの人数の変更
		m_PlaySelectUi[m_PlayPlayer]->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.5f));
		m_PlayPlayer--;
		if (m_PlayPlayer < 0)
		{
			m_PlayPlayer = playerNumber - 1;
			if (playerNumber == 0)
			{
				m_PlayPlayer = 0;
			}
			
		}

		m_PlaySelectUi[m_PlayPlayer]->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));;
	}

	if (pInput->Trigger(KEY_RIGHT) && m_Select)
	{//プレイヤーの人数の変更
		m_PlaySelectUi[m_PlayPlayer]->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.5f));
		m_PlayPlayer++;
		if (m_PlayPlayer >= playerNumber)
		{
			m_PlayPlayer = 0;
		}

		m_PlaySelectUi[m_PlayPlayer]->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));;
	}

	if ((pInput->Trigger(KEY_DECISION) || pInput->Trigger(MOUSE_INPUT_LEFT)) && m_Select)
	{
		// サウンド
		CManager::GetManager()->GetSound()->Play(CSound::LABEL_SE_ENTER);
		//決定時
		m_IsPlayerselect = true;
		
		D3DXVECTOR3 Size = D3DXVECTOR3(200.0f, 0.0f, 0.0f);
		D3DXVECTOR3 readySize = D3DXVECTOR3(150.0f, 75.0f, 0.0f);
		for (int i = 0; i < m_PlayPlayer+1; i++)
		{
			float Pos = (float)CManager::SCREEN_WIDTH / (m_PlayPlayer+1);

			D3DXVECTOR3 screenPos = D3DXVECTOR3((Pos*i) + (Pos / 2), 0.0f - 50.0f, 0.0f);

			D3DXVECTOR3 worldPos = br::WorldCastScreen(&screenPos,			// スクリーン座標
				D3DXVECTOR3((float)CManager::SCREEN_WIDTH, (float)CManager::SCREEN_HEIGHT, 0.0f),		// スクリーンサイズ
				CManager::GetManager()->GetCamera()->GetViewMatrix(),									// ビューマトリックス
				CManager::GetManager()->GetCamera()->GetProjMatrix());									// プロジェクションマトリックス


			if (m_Player[i] == nullptr)
			{//プレイヤー作ってないとき
				m_CharacterSelectUi[i] = CObject2D::Create(screenPos, Size, PRIORITY_UI2D);
				m_CharacterSelectUi[i]->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));

				m_ResultUi[i] = CObject2D::Create(D3DXVECTOR3((Pos*i) + (Pos / 2), CManager::SCREEN_HEIGHT * 0.5f, 0.0f),
					readySize, PRIORITY_UI2D);

				m_ResultUi[i]->SetTexture(CTexture::TEXTURE_READY);
				m_ResultUi[i]->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.0f));

				m_Player[i] = CPlayer::Create(worldPos, i, i);

				//プレイヤーの後ろのやつ
				m_CharacterSelectBg[i] = CObject3D::Create(worldPos, D3DXVECTOR3(100.0f, 150.0f, 0.0f), PRIORITY_BULLET);
				m_CharacterSelectBg[i]->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.0f));
				//テクスチャ
				switch ((CManager::PLAYERID)i)
				{
				case CManager::PLAYER000:
					m_CharacterSelectBg[i]->SetTexture(CTexture::TEXTURE_SELECTPLAYER);
					break;
				case CManager::PLAYER001:
					m_CharacterSelectBg[i]->SetTexture(CTexture::TEXTURE_SELECTPLAYER1);

					break;
				case CManager::PLAYER002:
					m_CharacterSelectBg[i]->SetTexture(CTexture::TEXTURE_SELECTPLAYER2);

					break;
				case CManager::PLAYER003:
					m_CharacterSelectBg[i]->SetTexture(CTexture::TEXTURE_SELECTPLAYER3);

					break;
				default:
					break;
				}
			}
			else
			{	
				//作られてたとき
				m_CharacterSelectUi[i]->SetPos(screenPos);
				m_CharacterSelectBg[i]->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.0f));
				m_ResultUi[i]->SetPos(screenPos);
				m_Player[i]->SetPos(worldPos);
				m_CharacterSelectBg[i]->SetPos(worldPos);
			}

		}
	}
	m_BgMove += 0.001f;
	
}

//--------------------------------------------------
//整列関数
//--------------------------------------------------
void CPlayerSelect::SetList()
{
	if (m_List.IsApple)
	{
		m_Top = FRUIT_GRAPE;
		if (m_List.IsGrape)
		{
			m_Top = FRUIT_ORANGE;
			if (m_List.IsOrange)
			{
				m_Top = FRUIT_STRAWBERRY;
				if (m_List.IsStrawberry)
				{
					m_Top = FRUIT_CHERRY;
				}
			}
		}
	}
	else
	{
		m_Top = 0;
	}

	if (m_List.IsCherry)
	{
		m_Last = 2;
		if (m_List.IsOrange)
		{
			m_Last = 3;
			if (m_List.IsStrawberry)
			{
				m_Last = 4;
				if (m_List.IsGrape)
				{
					m_Last = 5;
				}
			}
		}
	}
	else
	{
		m_Last = 1;
	}
}
//--------------------------------------------------
//整列関数
//--------------------------------------------------
int CPlayerSelect::SelectSort(const int Isnumber ,const bool isUp)
{
	int List = Isnumber;
	bool IsChange = true;
	while (IsChange)
	{
		IsChange = false;
		if (isUp)
		{
			switch ((FRUIT)List)
			{
			case CPlayerSelect::FRUIT_APPLE:
				if (m_List.IsApple)
				{
					IsChange = true;
					List = FRUIT_CHERRY;
				}
				break;
			case CPlayerSelect::FRUIT_GRAPE:
		
				if (m_List.IsGrape)
				{
					IsChange = true;
					List = FRUIT_ORANGE;
				}
				break;
			case CPlayerSelect::FRUIT_ORANGE:
				if (m_List.IsOrange)
				{
					IsChange = true;
					List = FRUIT_STRAWBERRY;
				}
				break;
			case CPlayerSelect::FRUIT_STRAWBERRY:
				if (m_List.IsStrawberry)
				{
					IsChange = true;
					List = FRUIT_CHERRY;
				}
			case CPlayerSelect::FRUIT_CHERRY:
				if (m_List.IsCherry)
				{
					IsChange = true;
					List = FRUIT_APPLE;
				}
				break;
			case CPlayerSelect::FRUIT_MAX:
				break;
			default:

				break;
			}
		}
		else
		{
			switch ((FRUIT)List)
			{
			case CPlayerSelect::FRUIT_APPLE:
				if (m_List.IsApple)
				{
					IsChange = true;
					List = FRUIT_STRAWBERRY;
				}
				break;
			case CPlayerSelect::FRUIT_GRAPE:
				if (m_List.IsGrape)
				{
					IsChange = true;
					List = FRUIT_APPLE;
				}
				break;
			case CPlayerSelect::FRUIT_ORANGE:
				if (m_List.IsOrange)
				{
					IsChange = true;
					List = FRUIT_STRAWBERRY;
				}
				break;
			case CPlayerSelect::FRUIT_STRAWBERRY:
				if (m_List.IsStrawberry)
				{
					IsChange = true;
					List = FRUIT_ORANGE;
				}
				break;
			case CPlayerSelect::FRUIT_CHERRY:
				if (m_List.IsCherry)
				{
					IsChange = true;
					List = FRUIT_STRAWBERRY;
				}
				break;
			case CPlayerSelect::FRUIT_MAX:
				break;
			default:

				break;
			}
		}
	}
	return List;
}