//============================
//
// エフェクト
// Author:hamada ryuuga
//
//============================

//**************************************************
// include
//**************************************************
#include <assert.h>
#include "manager.h"
#include "ring.h"
#include "objectX.h"
#include "object2D.h"
#include "fade.h"
#include "player.h"
#include "sound.h"

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CRing::CRing(int nPriority /* =0 */) : CObject(nPriority)
{
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CRing::~CRing()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CRing::Init()
{
	m_Bgblender = CObject3D::Create(m_pos,
		D3DXVECTOR3(CManager::SCREEN_WIDTH*0.5f, CManager::SCREEN_HEIGHT*0.6f, 0.0f), PRIORITY_EFFECT);
	m_Bgblender->SetTexture(CTexture::TEXTURE_EFFECT5);
	m_Bgblender->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
	m_Bgblender->SetSize(D3DXVECTOR3(0.0f, 0.0f, 0.0f));
	m_size = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	m_count = 0;
	
	

	return S_OK;
}

//--------------------------------------------------
// 終了
//--------------------------------------------------
void CRing::Uninit()
{

	CObject::DeletedObj();
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CRing::Update()
{

	if (m_is)
	{
		m_size.x += 5.0f;
		m_size.y += 5.0f;
		m_Bgblender->SetSize(m_size);
		m_count++;
		if (m_count >= 20)
		{
			m_is = false;
		}
	}
	else
	{
		m_Bgblender->Uninit();
		CRing::Uninit();
	}


}

//--------------------------------------------------
// 描画
//--------------------------------------------------
void CRing::Draw(DRAW_MODE drawMode)
{

}

//--------------------------------------------------
// 生成関数
//--------------------------------------------------
CRing * CRing::Create(D3DXVECTOR3 Pos, int Is)
{
	CRing *pObject;
	pObject = new CRing(1);

	if (pObject != nullptr)
	{
		pObject->SetPos(Pos);
		pObject->Init();
		pObject->SetPlayerNum(Is);
	}
	else
	{
		assert(false);
	}

	return pObject;
}


//=========================================
// 軌跡の色変更
//=========================================
void CRing::SetPlayerNum(int Is)
{
	int num = Is;
	switch (num)
	{
	case 0://P1
		   //水色
		m_Bgblender->SetCol(D3DXCOLOR(0.7f, 1.0f, 1.0f, 1.0f));
		break;
	case 1://P2
		   //あか
		m_Bgblender->SetCol(D3DXCOLOR(1.0f, 0.4f, 0.4f, 1.0f));
		break;
	case 2://P3
		   //黄色
		m_Bgblender->SetCol(D3DXCOLOR(1.0f, 1.0f, 0.4f, 1.0f));
		break;
	case 3://P4
		   //緑
		m_Bgblender->SetCol(D3DXCOLOR(0.78f, 1.0f, 0.7f, 1.0f));
		break;
	case 99://NPC
		m_Bgblender->SetCol(D3DXCOLOR(0.0f, 0.0f, 0.0f, 1.0f));
		break;
	default:
		break;
	}
}