//==================================================
// signboard.h
// Author: Buriya Kota
//==================================================
#ifndef _SIGNBOARD_H_
#define _SIGNBOARD_H_

//**************************************************
// インクルード
//**************************************************
#include "block.h"

//**************************************************
// 前方宣言
//**************************************************
class CPlayer;
class CText;
class CObject2D;

//**************************************************
// クラス
//**************************************************
class CSignBoard :public CBlock
{
public:
	enum SIGN_BOARD_TYPE
	{
		SIGN_BOARD_TYPE_MOVE = 0,
		SIGN_BOARD_TYPE_JUNP,
		SIGN_BOARD_TYPE_SHOT,
		BLOCKTYPE_MAX
	};

	explicit CSignBoard(int nPriority = PRIORITY_BG_OBJECT);
	~CSignBoard();

	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw(DRAW_MODE drawMode) override;

	static CSignBoard *Create(const D3DXVECTOR3& pos, SIGN_BOARD_TYPE type);

	void SetType(SIGN_BOARD_TYPE type) { m_signBoard = type; }

	bool hitevent(CPlayer* player) override;

private:
	void Display_();

private:
	bool m_isHIt;
	bool m_isPlayerHit;
	bool m_isDisp;
	CText *m_pText;
	CObject2D *m_pObject2D;
	SIGN_BOARD_TYPE m_signBoard;
};

#endif	// _SIGNBOARD_H_
