//==================================================
// game.cpp
// Author: Buriya Kota
//==================================================

//**************************************************
// include
//**************************************************
#include "manager.h"
#include "camera.h"
#include "input.h"
#include "sound.h"
#include "pause.h"
#include "game.h"
#include "ranking.h"
#include "player_select.h"
#include "utility.h"

#include "fade.h"
#include "score.h"
#include "timer.h"
#include "silhouette.h"
#include "object3D.h"

#include "model.h"
#include "meshfield.h"
#include "mesh_sky.h"
#include "player.h"
#include "map.h"
#include "bg.h"
#include "particle.h"
#include "cpu.h"
#include "enemy_count.h"
#include "bgmodel.h"
#include "effect.h"
#include "audience.h"
#include "count.h"
#include "aubience_list.h"

namespace
{// マップの数
	const std::string fileName[] =
	{
		"data/MAP/STAGE_TUTORIAL.json",
		"data/MAP/STAGE001.json",
		"data/MAP/STAGE002.json",
		"data/MAP/STAGE003.json",
		"data/MAP/STAGE004.json",
	};
} // namespace



//**************************************************
// 静的メンバ変数
//**************************************************
CGame *CGame::m_pGame = nullptr;

//**************************************************
// マクロ定義
//**************************************************

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CGame::CGame()
{
	m_pPlayer.clear();
	m_pScore = nullptr;
	m_pPause = nullptr;
	m_pTimer = nullptr;
	m_Map = nullptr;
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CGame::~CGame()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CGame::Init()
{
	CManager::GetManager()->GetCamera()->SetParallel(false);
	CManager::GetManager()->ClearRanking();
	
	m_Map = CMap::Create();
	m_Map->Load(CManager::GetManager()->GetMap());
	
	SetSound(CManager::GetManager()->GetMap());

	CAubience_list::Create();
	//m_Map->LoadfileEnemy("data\\test.json");
	CManager::GetManager()->GetSound()->Play(CSound::LABEL_SE_GAYA);

	//CManager::GetManager()->GetCamera()->SetPosV(D3DXVECTOR3(0.0f, 200.0f, -400.0f));
	//CManager::GetManager()->GetCamera()->SetPosR(D3DXVECTOR3(0.0f, 0.0f, 0.0f));

	m_time = 0;
	m_popEnemy = 0;
	
	if (CManager::GetManager()->GetPlayerNumber() == 1)
	{
		m_enemy = CEnemyCount::Create(D3DXVECTOR3(CManager::SCREEN_WIDTH * 0.5f-200.0f, 100.0f, 0.0f), D3DXVECTOR3(25.0f, 50.0f, 0.0f));
		m_enemy->SetTimer(10);

		m_pScore = CScore::Create(D3DXVECTOR3(60.0f, 50.0f, 0.0f), D3DXVECTOR3(40.0f, 100.0f, 0.0f));
		m_pScore->SetScore(18000);
	}

	

	m_pPause = CPause::Create();

	//m_Bg->SetSpeed(0.01f);


	int nPlayerNumber = CManager::GetManager()->GetPlayerNumber();
	
	for (int nCnt = 0; nCnt < nPlayerNumber; nCnt++)
	{
		CPlayer* pPlayer = CPlayer::Create(D3DXVECTOR3(0.0f + nCnt * 100.0f, 500.0f, 0.0f),nCnt, CManager::GetManager()->GetList(nCnt));
		m_pPlayer.push_back(pPlayer);
	}


	m_count = CCount::Create(D3DXVECTOR3(CManager::SCREEN_WIDTH * 0.5f, 50.0f, -100.0f), D3DXVECTOR3(50.0f, 100.0f, 0.0f), PRIORITY_EFFECT);
	m_count->AddTex(CTexture::TEXTURE_COUNT3);
	m_count->AddTex(CTexture::TEXTURE_COUNT2);
	m_count->AddTex(CTexture::TEXTURE_COUNT1);
	m_count->AddTex(CTexture::TEXTURE_COUNT0);

	m_stagr = false;
	m_Map->SetFast(nPlayerNumber);
	return S_OK;
}

//--------------------------------------------------
// 終了
//--------------------------------------------------
void CGame::Uninit()
{
	if (CManager::GetManager()->GetPlayerNumber() == 0&& m_pScore!=nullptr)
	{	// スコアの設定
		CManager::SetNowScore(m_pScore->GetScore());
	}

	m_pPlayer.clear();
	m_pTimer = nullptr;
	m_pPause = nullptr;
	m_pGame = nullptr;

	CObject::DeletedObj();
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CGame::Update()
{

	m_time++;
	if (!m_stagr && m_time >= 180)
	{
		if (m_time >= 160)
		{//Startのサウンド
			CManager::GetManager()->GetSound()->Play(CSound::LABEL_SE_START);
		}
		m_stagr = true;
		m_time = 0;
		m_count->Uninit();
		
	}
	else if(m_stagr)
	{
		if (CManager::GetManager()->GetPlayerNumber() == 1)
		{
			m_pScore->AddScore(-1);
			if (m_pScore->GetScore() <= 0)
			{
				CFade::GetInstance()->SetFade(CManager::MODE_RESULT);
			}
		}
	

		if (m_time >= 100)
		{
			switch ((MODEL)m_popEnemy)
			{
			case CGame::MODEL_UP:
				CBgModel::Create(CModelData::MODEL_BACK1, false, true, CBgModel::LIGHT);	//1
				break;
			case CGame::MODEL_DOWN:

				CBgModel::Create(CModelData::MODEL_BACK2, false, true, CBgModel::LIGHT);	//2
				break;
			case CGame::MODEL_LEFT:

				CBgModel::Create(CModelData::MODEL_BACK3, true, true, CBgModel::DOWN);		//3
				break;
			case CGame::MODEL_RIGHT:

				CBgModel::Create(CModelData::MODEL_BACK4, true, false, CBgModel::LEFT);		//4
				break;
			case CGame::MODEL_MAX:
				break;
			default:
				break;
			}
			m_popEnemy++;
			m_time = 0;
			if (m_popEnemy >= CGame::MODEL_MAX)
			{
				m_popEnemy = 0;
			}
		}
	}
	
#ifdef _DEBUG
	CInput *pInput = CInput::GetKey();
	
	if (pInput->Trigger(DIK_RETURN) || pInput->Trigger(JOYPAD_B, 0))
	{
		//CAubience_list::Create();
		//m_pPlayer[0]->AddStun(10);
		//CEffect::Create(D3DXVECTOR3(0.0f, 0.0f, 0.0f), D3DXVECTOR3(0.0f, 0.0f, 0.0f), D3DXVECTOR3(100.0f, 100.0f, 0.0f), CParticle::MOVE_DAMAGE, PRIORITY_EFFECT);
		//m_Bg->ChangeTex();
		//CParticle::Create(D3DXVECTOR3(0.0f, 0.0f, 0.0f), D3DXVECTOR3(1.0f, 0.0f, 0.0f), 10, CParticle::MOVE_HANABI, PRIORITY_EFFECT);
		//CCpu::Create(D3DXVECTOR3 (0.0f,0.0f,0.0f), 1, 1);
		// 遷移
		//CFade::GetInstance()->SetFade(CManager::MODE_RANKING);
	}
#endif // _DEBUG
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CGame* CGame::Create()
{
	m_pGame = new CGame;

	if (m_pGame != nullptr)
	{
		m_pGame->Init();
	}
	else
	{
		assert(false);
	}

	return m_pGame;
}

//--------------------------------------------------
// サウンドと背景をマップごとに分ける
//--------------------------------------------------
void CGame::SetSound(std::string pass)
{
	if (pass == fileName[0])
	{

	}
	else if (pass == fileName[1])
	{//ステージ1
		CManager::GetManager()->GetSound()->Play(CSound::LABEL_BGM_GAME6);			//サウンド

		//背景
		m_Bg = CBg::Create(D3DXVECTOR3(CManager::SCREEN_WIDTH * 0.5f, 50.0f, 0.0f), D3DXVECTOR3(50.0f, 100.0f, 0.0f), PRIORITY_BG);
		m_Bg->AddTex(CTexture::TEXTURE_BGsakura0);
		m_Bg->AddTex(CTexture::TEXTURE_BGnatu1);
		m_Bg->AddTex(CTexture::TEXTURE_BGaki2);
		m_Bg->AddTex(CTexture::TEXTURE_BGhuyu3);

		m_Bg->SetPos(D3DXVECTOR3(0.0f, 0.0f, 500.0f));
		m_Bg->SetSize(D3DXVECTOR3(CManager::SCREEN_WIDTH*1.1f, CManager::SCREEN_HEIGHT*1.1f, 0.0f));

		//木の幹
		CObject3D*Bg3 = CObject3D::Create(D3DXVECTOR3(0.0f, 0.0f, 100.0f),
			D3DXVECTOR3(CManager::SCREEN_WIDTH*0.5f, CManager::SCREEN_HEIGHT*0.6f, 0.0f), PRIORITY_BULLET);
		Bg3->SetTexture(CTexture::TEXTURE_WOOD_PARTS_4);
		Bg3->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));

		//木の葉
		CBg*Bg2 = CBg::Create(D3DXVECTOR3(CManager::SCREEN_WIDTH * 0.5f, 50.0f, 0.0f), D3DXVECTOR3(50.0f, 100.0f, 0.0f), PRIORITY_BULLET);
		Bg2->AddTex(CTexture::TEXTURE_WOOD_PARTS_0);
		Bg2->AddTex(CTexture::TEXTURE_WOOD_PARTS_1);
		Bg2->AddTex(CTexture::TEXTURE_WOOD_PARTS_2);
		Bg2->AddTex(CTexture::TEXTURE_WOOD_PARTS_3);
	}
	else if (pass == fileName[2])
	{//ステージ2
		CManager::GetManager()->GetSound()->Play(CSound::LABEL_BGM_GAME2);		//サウンド

																				//背景
		m_Bg = CBg::Create(D3DXVECTOR3(CManager::SCREEN_WIDTH * 0.5f, 50.0f, 0.0f), D3DXVECTOR3(50.0f, 100.0f, 0.0f), PRIORITY_BG);
		m_Bg->AddTex(CTexture::TEXTURE_BGsakuraFUCK0);
		m_Bg->AddTex(CTexture::TEXTURE_BGnatuFUCK1);
		m_Bg->AddTex(CTexture::TEXTURE_BGakiFUCK2);
		m_Bg->AddTex(CTexture::TEXTURE_BGhuyuFUCK3);

		m_Bg->SetPos(D3DXVECTOR3(0.0f, 0.0f, 500.0f));
		m_Bg->SetSize(D3DXVECTOR3(CManager::SCREEN_WIDTH*1.1f, CManager::SCREEN_HEIGHT*1.1f, 0.0f));

		//木の幹
		CObject3D*Bg3 = CObject3D::Create(D3DXVECTOR3(0.0f, 0.0f, 100.0f),
			D3DXVECTOR3(CManager::SCREEN_WIDTH*0.5f, CManager::SCREEN_HEIGHT*0.6f, 0.0f), PRIORITY_BULLET);
		Bg3->SetTexture(CTexture::TEXTURE_WOOD_PARTS_FUCK2);
		Bg3->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));

		//木の葉
		CBg*Bg2 = CBg::Create(D3DXVECTOR3(CManager::SCREEN_WIDTH * 0.5f, 50.0f, 0.0f), D3DXVECTOR3(50.0f, 100.0f, 0.0f), PRIORITY_BULLET);
		Bg2->AddTex(CTexture::TEXTURE_WOOD_PARTS_FUCK0);
		Bg2->AddTex(CTexture::TEXTURE_WOOD_PARTS_FUCK0);
		Bg2->AddTex(CTexture::TEXTURE_WOOD_PARTS_FUCK0);
		Bg2->AddTex(CTexture::TEXTURE_WOOD_PARTS_FUCK0);
	}
	else if (pass == fileName[3])
	{//ステージ3
		CManager::GetManager()->GetSound()->Play(CSound::LABEL_BGM_GAME5);

		//背景
		m_Bg = CBg::Create(D3DXVECTOR3(CManager::SCREEN_WIDTH * 0.5f, 50.0f, 0.0f), D3DXVECTOR3(50.0f, 100.0f, 0.0f), PRIORITY_BG);
		m_Bg->AddTex(CTexture::TEXTURE_BGsakura_NIGHT0);
		m_Bg->AddTex(CTexture::TEXTURE_BGnatuNIGHT1);
		m_Bg->AddTex(CTexture::TEXTURE_BGakiNIGHT2);
		m_Bg->AddTex(CTexture::TEXTURE_BGhuyuNIGHT3);

		m_Bg->SetPos(D3DXVECTOR3(0.0f, 0.0f, 500.0f));
		m_Bg->SetSize(D3DXVECTOR3(CManager::SCREEN_WIDTH*1.1f, CManager::SCREEN_HEIGHT*1.1f, 0.0f));

		//木の幹
		CObject3D*Bg3 = CObject3D::Create(D3DXVECTOR3(0.0f, 0.0f, 100.0f),
			D3DXVECTOR3(CManager::SCREEN_WIDTH*0.5f, CManager::SCREEN_HEIGHT*0.6f, 0.0f), PRIORITY_BULLET);
		Bg3->SetTexture(CTexture::TEXTURE_WOOD_PARTS_NIGHT4);
		Bg3->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));

		//木の葉
		CBg*Bg2 = CBg::Create(D3DXVECTOR3(CManager::SCREEN_WIDTH * 0.5f, 50.0f, 0.0f), D3DXVECTOR3(50.0f, 100.0f, 0.0f), PRIORITY_BULLET);
		Bg2->AddTex(CTexture::TEXTURE_WOOD_PARTS_NIGHT0);
		Bg2->AddTex(CTexture::TEXTURE_WOOD_PARTS_NIGHT1);
		Bg2->AddTex(CTexture::TEXTURE_WOOD_PARTS_NIGHT2);
		Bg2->AddTex(CTexture::TEXTURE_WOOD_PARTS_NIGHT3);
	}
	else if (pass == fileName[4])
	{//ステージ4
		CManager::GetManager()->GetSound()->Play(CSound::LABEL_BGM_GAME4);

		//背景
		m_Bg = CBg::Create(D3DXVECTOR3(CManager::SCREEN_WIDTH * 0.5f, 50.0f, 0.0f), D3DXVECTOR3(50.0f, 100.0f, 0.0f), PRIORITY_BG);
		m_Bg->AddTex(CTexture::TEXTURE_BGsakuraFUCK0);
		m_Bg->AddTex(CTexture::TEXTURE_BGnatuFUCK1);
		m_Bg->AddTex(CTexture::TEXTURE_BGakiFUCK2);
		m_Bg->AddTex(CTexture::TEXTURE_BGhuyuFUCK3);

		m_Bg->SetPos(D3DXVECTOR3(0.0f, 0.0f, 500.0f));
		m_Bg->SetSize(D3DXVECTOR3(CManager::SCREEN_WIDTH*1.1f, CManager::SCREEN_HEIGHT*1.1f, 0.0f));

		//木の幹
		CObject3D*Bg3 = CObject3D::Create(D3DXVECTOR3(0.0f, 0.0f, 100.0f),
			D3DXVECTOR3(CManager::SCREEN_WIDTH*0.5f, CManager::SCREEN_HEIGHT*0.6f, 0.0f), PRIORITY_BULLET);
		Bg3->SetTexture(CTexture::TEXTURE_WOOD_PARTS_NIGHT4);
		Bg3->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));

		//木の葉
		CBg*Bg2 = CBg::Create(D3DXVECTOR3(CManager::SCREEN_WIDTH * 0.5f, 50.0f, 0.0f), D3DXVECTOR3(50.0f, 100.0f, 0.0f), PRIORITY_BULLET);
		Bg2->AddTex(CTexture::TEXTURE_WOOD_PARTS_NIGHT0);
		Bg2->AddTex(CTexture::TEXTURE_WOOD_PARTS_NIGHT1);
		Bg2->AddTex(CTexture::TEXTURE_WOOD_PARTS_NIGHT2);
		Bg2->AddTex(CTexture::TEXTURE_WOOD_PARTS_NIGHT3);
	}
}
