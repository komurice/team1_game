//==================================================
// title.cpp
// Author: Buriya Kota
//==================================================

//**************************************************
// include
//**************************************************
#include <assert.h>

#include "manager.h"
#include "camera.h"
#include "light.h"
#include "input.h"

#include "title.h"
#include "object2D.h"
#include "fade.h"
#include "sound.h"

#include "meshfield.h"
#include "mesh_sky.h"
#include "text.h"
#include "bg.h"
#include "bgmodel.h"
//**************************************************
// マクロ定義
//**************************************************
#define CHOICE_POS		(350.0f)
#define TITLE_HEIGHT	(200.0f)
#define MOVE_TITLE		(3.0f)

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CTitle::CTitle()
{
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CTitle::~CTitle()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CTitle::Init()
{
	CManager::GetManager()->ClearRanking();
	CManager::GetManager()->ClearList();
	
	// サウンド
	CManager::GetManager()->GetSound()->Play(CSound::LABEL_BGM_TITLE);
	CManager::GetManager()->GetCamera()->SetParallel(true);

	CManager::GetManager()->GetCamera()->SetPosV(D3DXVECTOR3(0.0f, 200.0f, -1000.0f));
	CManager::GetManager()->GetCamera()->SetPosR(D3DXVECTOR3(0.0f, 0.0f, 0.0f));

	CBgModel::Create(CModelData::MODEL_BACK1, false, true, CBgModel::LIGHT);	//1
	
	m_Bg = CBg::Create(D3DXVECTOR3(CManager::SCREEN_WIDTH * 0.5f, 50.0f, 0.0f), D3DXVECTOR3(50.0f, 100.0f, 0.0f), PRIORITY_BG);
	m_Bg->BlenderTex(CTexture::TEXTURE_BG1, CTexture::TEXTURE_BG2, CTexture::TEXTURE_BG3);

	// タイトル
	m_pObject2D[0] = CObject2D::Create(
		D3DXVECTOR3(CManager::SCREEN_WIDTH * 0.5f, -300.0f, 0.0f),
		D3DXVECTOR3(550.0f, 300.0f, 0.0f),
		PRIORITY_UI2D);
	m_pObject2D[0]->SetTexture(CTexture::TEXTURE_TITEL);


	// スタート
	m_pMenu[0] = CObject2D::Create(
		D3DXVECTOR3(CManager::SCREEN_WIDTH * 0.5f, CManager::SCREEN_HEIGHT - 150.0f, 0.0f),
		D3DXVECTOR3(200.0f * 2.0f, 50.0f * 2.0f, 0.0f),
		PRIORITY_UI2D);
	m_pMenu[0]->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
	m_pMenu[0]->SetTexture(CTexture::TEXTURE_START);


	D3DXVECTOR3 menuPos = m_pMenu[0]->GetPos();

	//// 矢印
	//m_pChoice = CObject2D::Create(
	//	D3DXVECTOR3(D3DXVECTOR3(menuPos.x - CHOICE_POS, menuPos.y, menuPos.z)),
	//	D3DXVECTOR3(50.0f, 70.0f, 0.0f),
	//	PRIORITY_UI2D);
	//m_pChoice->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));

	//m_pChoice->SetTexture(CTexture::TEXTURE_CHOICE_R);

	m_nSelect = SELECT_GAMEMODE_START;

	return S_OK;
}

//--------------------------------------------------
// 終了
//--------------------------------------------------
void CTitle::Uninit()
{
	CObject::DeletedObj();
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CTitle::Update()
{
	MoveTitle_();

	CInput *pInput = CInput::GetKey();

	if (pInput->Trigger(KEY_UP))
	{
		// サウンド
		CManager::GetManager()->GetSound()->Play(CSound::LABEL_SE_SELECT);

		m_pMenu[m_nSelect]->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.5f));

		m_nSelect--;
		if (m_nSelect < 0)
		{
			m_nSelect = 0;
		}

		D3DXVECTOR3 menuPos = m_pMenu[m_nSelect]->GetPos();
		
		m_pMenu[m_nSelect]->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
	}
	else if (pInput->Trigger(KEY_DOWN))
	{
		// サウンド
		CManager::GetManager()->GetSound()->Play(CSound::LABEL_SE_SELECT);

		m_pMenu[m_nSelect]->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.5f));

		m_nSelect++;
		if (m_nSelect >= 1)
		{
			m_nSelect = 0;
		}

		D3DXVECTOR3 menuPos = m_pMenu[m_nSelect]->GetPos();
		
		m_pMenu[m_nSelect]->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
	}

	if (pInput->Trigger(KEY_DECISION) || pInput->Trigger(MOUSE_INPUT_LEFT))
	{
		// サウンド
		CManager::GetManager()->GetSound()->Play(CSound::LABEL_SE_ENTER2);

		switch (m_nSelect)
		{
		case CTitle::SELECT_GAMEMODE_START:
			// 遷移
			CFade::GetInstance()->SetFade(CManager::MODE_PLAYER_SELECT);
			break;

		case CTitle::SELECT_GAMEMODE_TUTRIAL:
			// 遷移
			CFade::GetInstance()->SetFade(CManager::MODE_TUTORIAL);
			break;

		default:
			assert(false);

			break;
		}
	}
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CTitle *CTitle::Create()
{
	CTitle *pTitle;
	pTitle = new CTitle;

	if (pTitle != nullptr)
	{
		pTitle->Init();
	}
	else
	{
		assert(false);
	}

	return pTitle;
}

//--------------------------------------------------
// タイトルの動き
//--------------------------------------------------
void CTitle::MoveTitle_()
{
	for (int i = 0; i < PARTS; i++)
	{
		D3DXVECTOR3 pos = m_pObject2D[i]->GetPos();

		if (pos.y >= TITLE_HEIGHT)
		{
			pos.y = TITLE_HEIGHT;
		}
		else
		{
			pos.y += MOVE_TITLE;
		}

		m_pObject2D[i]->SetPos(pos);
	}
}