//============================
//
// ブロック
// Author:hamada ryuuga
//
//============================
#ifndef _BLOCK_H_
#define _BLOCK_H_

//**************************************************
// インクルード
//**************************************************
#include "objectX.h"


class CPlayer;
//**************************************************
// クラス
//**************************************************
class CBlock :public CObjectX
{
public:
	static const int REVIVAL_TIME = 300;
public:
	enum BLOCKTYPE
	{
		BLOCKTYPE_NORMAL = 0,
		BLOCKTYPE_LEFT,
		BLOCKTYPE_RIGHT,
		BLOCKTYPE_BROKEN,
		BLOCKTYPE_PODIUM1,
		BLOCKTYPE_CLEENNESS,
		BLOCKTYPE_MAX
	};
	const float BLOCKSIZE = 5.0f;

	explicit CBlock(int nPriority = PRIORITY_OBJECT);
	~CBlock();

	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw(DRAW_MODE drawMode) override;
	virtual bool hitevent(CPlayer* player);
	static CBlock* CBlock::Create(D3DXVECTOR3 pos, int ModelType, BLOCKTYPE SetModel);

	D3DXVECTOR3 GetPosOriginPos() { return m_PosOrigin; }
	void SetPosOriginPos(D3DXVECTOR3 Pos) { m_PosOrigin = Pos; }

	D3DXVECTOR3 GetPosOld() { return m_PosOld; }
	void SetPosOld(D3DXVECTOR3 Pos) { m_PosOld = Pos; }

	D3DXVECTOR3 GetNouPos() { return GetPos() - m_MovePos; }
	D3DXVECTOR3 GetMovePos() { return m_MovePos; }

	int GetModelType() { return m_ModelType; }
	void SetModelType(int Model) { m_ModelType = Model; }

	bool Collision(const D3DXVECTOR3 PlayerPos, const D3DXVECTOR3 PlayerSize, CPlayer* player);
	void SetBlockType(BLOCKTYPE SetModel) { m_Model = SetModel; };
	void SetCollisio(bool Collision) { m_IsCollision = Collision; }

protected:
	BLOCKTYPE m_Model;
	D3DXVECTOR3 m_MovePos;
	D3DXVECTOR3 m_PosOrigin;
	D3DXVECTOR3 m_PosOld;
	int m_ModelType;
	bool m_IsCollision;

	bool m_isBroken;	// 壊れているかどうか
	int m_brokenTimer;	// 壊れている時間
};

#endif	// _FADE_H_
