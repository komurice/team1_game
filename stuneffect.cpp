//==================================================
// エフェクト
// Author: HAMADA
//==================================================

//**************************************************
// include
//**************************************************
#include "stuneffect.h"
#include "player.h"
#include "block.h"
#include "debug_proc.h"
#include "effect.h"
#include "particle.h"
#include "utility.h"

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CStunEffect::CStunEffect(int nPriority) : CObject3D(nPriority)
{
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CStunEffect::~CStunEffect()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CStunEffect::Init()
{
	CObject3D::Init();
	SetBillboard(true);
	SetTexture(CTexture::TEXTURE_STUN);
	m_timer = 0.5f;
	m_life = 50;
	m_up = false;
	return S_OK;
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CStunEffect::Update()
{

	//ランダムな値を設定
	D3DXVECTOR3 move{0.0f,0.0f,0.0f};
	move.x = br::IntRandom(5.0f, -5.0f);
	move.y = br::IntRandom(5.0f, -5.0f);

	D3DXVECTOR3 pos = GetPos();
	SetPos(pos + move);

	CObject3D::Update();

	float color = 0.01f;
	if (m_up)
	{
		m_timer += color;
	}
	else
	{
		m_timer -= color;
	}
	if (m_timer >= 1.0f)
	{
		m_up = false;
	}
	if (m_timer <= 0.5f)
	{
		m_up = true;
	}

	
	SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, m_timer));



}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CStunEffect* CStunEffect::Create(const D3DXVECTOR3 pos, const D3DXVECTOR3 size)
{
	CStunEffect* pBullet;
	pBullet = new CStunEffect(PRIORITY_PLAYER);

	if (pBullet != nullptr)
	{
		pBullet->Init();
		pBullet->SetPos(pos);
		pBullet->SetSize(size);

	}
	else
	{
		assert(false);
	}

	return pBullet;
}
