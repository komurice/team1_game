//============================
//
// 背景
// Author:hamada ryuuga
//
//============================
#ifndef _BG_H_
#define _BG_H_

//**************************************************
// インクルード
//**************************************************
#include "object3D.h"
#include "model_data.h"
class CObjectX;
class CModelData;
//**************************************************
// クラス
//**************************************************
class CBg : public CObject3D
{
public:

	enum MOVE_STATE
	{
		MOVE_NONE = 0,
		MOVE_WORLD,
		MOVE_SKY,
		MOVE_STATE_MAX
	};
	const float Timer = 0.001f;
	const float VecMove = 0.1f;
	explicit CBg(int nPriority = PRIORITY_BG);
	~CBg();

	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw(DRAW_MODE drawMode) override;
	void Move(MOVE_STATE Move);

	static CBg *CBg::Create(D3DXVECTOR3 pos, D3DXVECTOR3 size, int nPriority);

	void SetSize(D3DXVECTOR3 size);
	void SetPos(D3DXVECTOR3 pos);

	void SetSpeed(float Speed) { m_speed = Speed; }
	int GetTex() { return m_priority; }
	void SetTex(int data) { m_priority = data; }

	void BlenderTex(CTexture::TEXTURE Fast, CTexture::TEXTURE Second, CTexture::TEXTURE Third);
	
	void SetModel(CModelData::MODEL_TYPE InData, CModelData::MODEL_TYPE InData2, MOVE_STATE Data);

	void AddTex(CTexture::TEXTURE add);
	void ChangeTex(CTexture::TEXTURE Fast, CTexture::TEXTURE Second, CTexture::TEXTURE Third);
	void ChangeTex();
private:
	CObject3D *m_Bgblender[3];

	std::vector<CTexture::TEXTURE> m_List;
	std::vector<CTexture::TEXTURE> m_Log;
	CObjectX* m_sun;
	CObjectX* m_moon;
	float m_blender;
	int m_Noudraw;
	int m_Nextdraw;
	MOVE_STATE m_moveType;
	bool m_MoveVec;
	bool m_bgChange;
	bool m_EventEnd;
	bool m_EventEnd2;
	int m_priority;
	float m_speed;
};

#endif
