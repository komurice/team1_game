//**************************************************
// texture.h
// Author  : katsuki mizuki
//**************************************************
#ifndef _TEXTURE_H_
#define _TEXTURE_H_

//==================================================
// インクルード
//==================================================
#include <d3dx9.h>

//==================================================
// 定義
//==================================================
class CTexture
{
public: /* 定義 */
	enum TEXTURE
	{
		TEXTURE_PLAYER = 0,						// プレイヤー
		TEXTURE_GROUND,							// 地面
		TEXTURE_SHADOW,							// 影
		TEXTURE_TITLE_BG,						// TITLE_BG
		TEXTURE_TITLE,							// TITLE
		TEXTURE_TUTORIAL,						// TUTORIAL
		TEXTURE_START,							// START
		TEXTURE_RESULT,							// RESULT
		TEXTURE_NUMBER,							// タイム
		TEXTURE_SNOW,							// 雪
		TEXTURE_SNOW_GROUND,					// 雪の地面
		TEXTURE_SKY,							// 空
		TEXTURE_COIN,							// コイン取得時のパーティクル
		TEXTURE_CHOICE_R,						// 選択アイコンR
		TEXTURE_CHOICE_L,						// 選択アイコンL
		TEXTURE_RANKING_NUMBER,					// 順位
		TEXTURE_PAUSE_BACK_TITLE,				// タイトルへ
		TEXTURE_PAUSE_RESTART,					// はじめから
		TEXTURE_PAUSE_CLOSE,					// とじる
		TEXTURE_PAUSE,							// ポーズ
		TEXTURE_PRESS_ENTER_OR_A,				// 押してほしいボタン
		TEXTURE_RANKING,						// ランキング
		TEXTURE_TUTORIAL_OPERATION_METHOD,		// 操作方法
		TEXTURE_TUTORIAL_MAP,					// マップ
		TEXTURE_TUTORIAL_MANUAL,				// チュートリアル
		TEXTURE_GOAL,
		TEXTURE_BG1,//ゲームの時の背景
		TEXTURE_BG2,
		TEXTURE_BG3,
		TEXTURE_UI1,//プレイヤーのUI
		TEXTURE_UI2,
		TEXTURE_UI3,
		TEXTURE_UI4,
		TEXTURE_READY,
		TEXTURE_SELECTBG,//selectのやつ
		TEXTURE_SELECTBG1,
		TEXTURE_SELECTBG2,
		TEXTURE_SELECTBG3,
		TEXTURE_STAGE,
		TEXTURE_SELECTPLAYER,//プレイヤーの後ろにある箱
		TEXTURE_SELECTPLAYER1,
		TEXTURE_SELECTPLAYER2,
		TEXTURE_SELECTPLAYER3,
		TEXTURE_ARROW,							// 矢印
		TEXTURE_PLAYERNNUMBER,//プレイにんずうのやつ
		TEXTURE_PLAYERNNUMBER1,
		TEXTURE_PLAYERNNUMBER2,
		TEXTURE_PLAYERNNUMBER3,
		TEXTURE_STUN_UI_1,
		TEXTURE_STUN_UI_2,
		TEXTURE_STUN_UI_3,
		TEXTURE_BULLET,
		TEXTURE_EXPLOSION,
		TEXTURE_ITEM_SHOTGUN,
		TEXTURE_ITEM_LONGRANGE,
		TEXTURE_ITEM_SPEED,
		TEXTURE_TITEL,
		TEXTURE_WOOD_PARTS_0,
		TEXTURE_WOOD_PARTS_1,
		TEXTURE_WOOD_PARTS_2,
		TEXTURE_WOOD_PARTS_3,
		TEXTURE_WOOD_PARTS_4,
		TEXTURE_BOM,
		TEXTURE_RESULT_BG,						// RESULT_BG
		TEXTURE_HUKI,						//hukidasi
		TEXTURE_BOKO,						// BOKO効果音
		TEXTURE_BAKI,						//  BAKI効果音
		TEXTURE_DOKO,						//  DOKO効果音
		TEXTURE_KIKEN,						//  kiken
		TEXTURE_SUTA,						//	SUTA効果音
		TEXTURE_DA,						//  DA効果音
		TEXTURE_ZA,						//  ZA効果音
		TEXTURE_EFFECT1,					// エフェクト1
		TEXTURE_EFFECT2,					// エフェクト2
		TEXTURE_EFFECT3,					// エフェクト3
		TEXTURE_KUSA,					// エフェクト3
		TEXTURE_STUN,					// エフェクト3
		TEXTURE_COUNT0,					// エフェクト1
		TEXTURE_COUNT1,					// エフェクト2
		TEXTURE_COUNT2,					// エフェクト3
		TEXTURE_COUNT3,					// エフェクト1
		TEXTURE_FIN,					// エフェクト1
		TEXTURE_CLOWN,					// エフェクト1
		TEXTURE_FIGHT,					// ふきだし
		TEXTURE_FOOL,					// ふきだし２
		TEXTURE_TUTORIAL_MOVE,			// 看板（イドウ）
		TEXTURE_TUTORIAL_JUMP,			// 看板（ジャンプ）
		TEXTURE_TUTORIAL_SHOT,			// 看板（撃つ）
		TEXTURE_MESUGAKI1,					// ふきだし２
		TEXTURE_BGsakura0,					// ふきだし２
		TEXTURE_BGnatu1,					// ふきだし２
		TEXTURE_BGaki2,						// ふきだし２
		TEXTURE_BGhuyu3,					// ふきだし２
		TEXTURE_EFFECT4,					// エフェクト4
		TEXTURE_GAYA1,							// 吹き出し
		TEXTURE_GAYA2,							// 吹き出し
		TEXTURE_GAYA3,							// 吹き出し
		TEXTURE_GAYA4,							// 吹き出し
		TEXTURE_GAYA5,							// 吹き出し
		TEXTURE_EFFECT5,						// ring
		TEXTURE_WOOD_PARTS_NIGHT0,				//背景パーツ夜
		TEXTURE_WOOD_PARTS_NIGHT1,
		TEXTURE_WOOD_PARTS_NIGHT2,
		TEXTURE_WOOD_PARTS_NIGHT3,
		TEXTURE_WOOD_PARTS_NIGHT4,
		TEXTURE_BGsakura_NIGHT0,					// ふきだし２
		TEXTURE_BGnatuNIGHT1,					// ふきだし２
		TEXTURE_BGakiNIGHT2,						// ふきだし２
		TEXTURE_BGhuyuNIGHT3,					// ふきだし２
		TEXTURE_BGNIGHTLEAF,					// 床の草夜
		TEXTURE_BGSNOW,							// 雪
		TEXTURE_RANKING_NUMBER_1,						// ring
		TEXTURE_RANKING_NUMBER_2,						// ring
		TEXTURE_RANKING_NUMBER_3,						// ring
		TEXTURE_RANKING_NUMBER_4,						// ring
		TEXTURE_RANKING_NUMBER_5,						// ring
		TEXTURE_WOOD_PARTS_FUCK0,				//背景パーツモノクロ
		TEXTURE_WOOD_PARTS_FUCK2,				//幹モノクロ
		TEXTURE_BGsakuraFUCK0,					// 桜モノ
		TEXTURE_BGnatuFUCK1,					// 夏モノ
		TEXTURE_BGakiFUCK2,						// 紅葉モノ
		TEXTURE_BGhuyuFUCK3,					// 冬モノ
		TEXTURE_STAGE1,							// ステージセレクト1
		TEXTURE_STAGE2,							// ステージセレクト1
		TEXTURE_STAGE3,							// ステージセレクト1
		TEXTURE_STAGE4,							// ステージセレクト1
		TEXTURE_TUTORIAL2,						// チュートリアル2
		TEXTURE_TUTORIAL3,						// チュートリアル3
		TEXTURE_MAX,
		TEXTURE_NONE,							// 使用しない
	};

	static const char* s_FileName[];	// ファイルパス

public:
	CTexture();		// デフォルトコンストラクタ
	~CTexture();	// デストラクタ

public: /* メンバ関数 */
	void LoadAll();										// 全ての読み込み
	void Load(TEXTURE inTexture);						// 指定の読み込み
	void ReleaseAll();									// 全ての破棄
	void Release(TEXTURE inTexture);					// 指定の破棄
	LPDIRECT3DTEXTURE9 GetTexture(TEXTURE inTexture);	// 情報の取得

private: /* メンバ変数 */
	LPDIRECT3DTEXTURE9 s_pTexture[TEXTURE_MAX];	// テクスチャの情報
};

#endif // !_TEXTURE_H_
