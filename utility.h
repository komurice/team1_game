//==================================================
// utility.h
// Author: Buriya Kota
//==================================================
#ifndef _UTILITY_H_
#define _UTILITY_H_

//**************************************************
// インクルード
//**************************************************
#include "object2D.h"

//**************************************************
// 前方前言　実態はNG　ポインタだけならOK
//**************************************************

//**************************************************
// 定数定義
//**************************************************

//**************************************************
// 構造体定義
//**************************************************

//**************************************************
// グローバル関数
//**************************************************
namespace br
{
	// 座標変換
	D3DXVECTOR3 GetWorldToScreenPos(const D3DXVECTOR3& pos);
	float Vec2Cross(D3DXVECTOR3* v1, D3DXVECTOR3* v2);
	float D3DXVec2Dot(D3DXVECTOR3* v1, D3DXVECTOR3* v2);
	float FloatRandam(float fMax, float fMin);
	int IntRandom(int nMax, int nMin);
	float NormalizeAngle(float *pAngle);
	void NormalizeAngleDest(float *pAngle, float *pAngleDest);
	float easeInOutQuint(float x);
	float easeOutQuint(float x);

	D3DXVECTOR3 WorldCastScreen(D3DXVECTOR3 *screenPos,			// スクリーン座標
		const D3DXVECTOR3& screenSize,									// スクリーンサイズ
		const D3DXMATRIX& mtxView,									// ビューマトリックス
		const D3DXMATRIX& mtxProjection);								// プロジェクションマトリックス

	D3DXVECTOR3 ScreenCastWorld(D3DXVECTOR3 *screenPos,			// スクリーン座標
		D3DXVECTOR3 screenSize									// スクリーンサイズ
	);

	const D3DXVECTOR3& GetScreenSize();
}

namespace hmd
{
	bool CollisionCircle(const D3DXVECTOR3& pos1, float radius1, const D3DXVECTOR3& pos2, float radius2);
	D3DXMATRIX *giftmtx(D3DXMATRIX *pOut, D3DXVECTOR3 pos, D3DXVECTOR3 rot, bool Billboard = false);
	D3DXMATRIX *giftmtxQuat(D3DXMATRIX *pOut, D3DXVECTOR3 pos, D3DXQUATERNION Quat, bool Billboard = false);
	float easeInSine(float X);
	float easeInQuad(float X);
	bool is_sjis_lead_byte(int c);
}

#endif	// _UTILITY_H_