//==================================================
// stun_gauge.h
// Author: Buriya Kota
//==================================================
#ifndef _BGMODEL_H_
#define _BGMODEL_H_

//**************************************************
// インクルード
//**************************************************
#include "object.h"
#include "model_data.h"


//**************************************************
// 前方前言　実態はNG　ポインタだけならOK
//**************************************************
class CObjectX;

//**************************************************
// クラス
//**************************************************
class CBgModel : public CObject
{
public:
	static const int MAX_PLAYER = 4;

	enum POPPOINT
	{
		LEFT = 0,
		LIGHT,
		UP,
		DOWN,
		MAX,
	};
public:
	explicit CBgModel(int nPriority = PRIORITY_UI3D);
	~CBgModel() override;

	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw(DRAW_MODE /*drawMode*/) override {}

	static CBgModel *Create(CModelData::MODEL_TYPE modelData, bool IsSine, bool IsEase, POPPOINT point);

	void SetData(CModelData::MODEL_TYPE modelData);
	void SetPoint(POPPOINT IsPoint);
	void SetIsSine(bool IsSine) { m_IsSine = IsSine; }
	void SetIsEase(bool IsEase) { m_IsEase = IsEase; }
	void SetPos(D3DXVECTOR3 Pos);
	void SetMagnification(float magnification) { m_magnification = magnification; }
private:
	

private:
	int m_Sin;

	bool m_IsSine;
	bool m_IsEase;

	CModelData::MODEL_TYPE m_modelData;

	D3DXVECTOR3 m_Move;
	CObjectX* m_Model;

	float m_frame;
	float m_magnification;

	void moveSine();
	void moveEase();
	
};

#endif // _STUN_GAUGE_H_
