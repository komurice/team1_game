//============================
//
// UI
// Author:hamada ryuuga
//
//============================

//**************************************************
// include
//**************************************************
#include <assert.h>
#include "manager.h"
#include "stage_ui.h"
#include "player.h"
#include "stun_gauge.h"
#include "particle.h"
//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CStage_Ui::CStage_Ui(int nPriority /* =0 */) : CObject3D(nPriority)
{
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CStage_Ui::~CStage_Ui()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CStage_Ui::Init()
{
	m_blender = 0.0f;
	m_alpha = 0.0f;
	m_Noudraw = 0;
	m_Nextdraw = 1;
	m_BoostTime = 0;
	m_ShotTime = 0;
	int PlayPlayer = CManager::GetManager()->GetPlayerNumber();
	float Pos = (float)CManager::SCREEN_WIDTH / (PlayPlayer);

	D3DXVECTOR3 screenPos = D3DXVECTOR3((Pos*m_MyNumber) + (Pos / 2), 70.0f, 0.0f);

	m_UiBox = CObject2D::Create(D3DXVECTOR3(screenPos),
		D3DXVECTOR3(300.0f, 130.0f, 0.0f), PRIORITY_UI2D);

	switch (m_MyNumber)
	{
	case 0:
		m_UiBox->SetTexture(CTexture::TEXTURE_UI1);
		break;
	case 1:
		m_UiBox->SetTexture(CTexture::TEXTURE_UI2);
		break;
	case 2:
		m_UiBox->SetTexture(CTexture::TEXTURE_UI3);
		break;
	case 3:
		m_UiBox->SetTexture(CTexture::TEXTURE_UI4);
		break;
	default:
		break;
	}

	m_UiBox->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));

	for (int i = 0; i < 5; i++)
	{
		
		m_Bullet[i] = CObject2D::Create(D3DXVECTOR3((screenPos.x+(i*20.0f)-50.0f), screenPos.y, 0.0f),
			D3DXVECTOR3(30.0f, 30.0f, 0.0f), PRIORITY_UI2D);
	
		//m_Bullet[i]->SetTexture(CTexture::TEXTURE_NONE);
		m_Bullet[i]->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f,0.0f));
	}
	m_Boost = CObject2D::Create(D3DXVECTOR3((screenPos.x + (20.0f) - 50.0f), screenPos.y, 0.0f),
		D3DXVECTOR3(70.0f, 70.0f, 0.0f), PRIORITY_UI2D);
	m_Boost->SetTexture(CTexture::TEXTURE_ITEM_SPEED);
	m_Boost->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.0f));

	return S_OK;
}

//--------------------------------------------------
// 終了
//--------------------------------------------------
void CStage_Ui::Uninit()
{
	CObject3D::Uninit();
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CStage_Ui::Update()
{
	if (m_BoostTime >= 0)
	{//ブーストアイテムの消滅時間
		m_BoostTime--;
		
		if (m_BoostTime <= 20)
		{
			m_alpha -= 0.05f;
			m_Boost->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, m_alpha));
			
		}
		if (m_BoostTime <= 0)
		{
			m_Boost->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.0f));
		}
	}
	
	if (m_ShotTime >= 0)
	{
		m_ShotTime--;
		for (int i = 0; i < 3; i++)
		{
			if (m_ShotTime <= 20)
			{
				m_alpha -= 0.05f;
				m_Bullet[i]->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, m_alpha));

			}
			if (m_ShotTime <= 0)
			{
				m_Bullet[i]->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.0f));
			}
		}
	}
}

//--------------------------------------------------
// 描画
//--------------------------------------------------
void CStage_Ui::Draw(DRAW_MODE /*drawMode*/)
{
	
}

//--------------------------------------------------
// 生成関数
//--------------------------------------------------
CStage_Ui * CStage_Ui::Create(int Type, int nPriority)
{
	CStage_Ui *pObject2D;
	pObject2D = new CStage_Ui(nPriority);

	if (pObject2D != nullptr)
	{
		pObject2D->SetMyNumber(Type);
		pObject2D->Init();
	}
	else
	{
		assert(false);
	}

	return pObject2D;
}

//--------------------------------------------------
// Set関数
//--------------------------------------------------
void CStage_Ui::SetType(CPlayer::BULLET_TYPE EVENY)
{
	for (int i = 0; i < 5; i++)
	{
		m_Bullet[i]->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.0f));
	}
	m_Type = EVENY;
	switch (m_Type)
	{
	case CPlayer::DEFAULT:

		break;
	case CPlayer::RANGE_EXTENSION:
		for (int i = 0; i < 3; i++)
		{
			m_Bullet[i]->SetTexture(CTexture::TEXTURE_ITEM_LONGRANGE);
			m_Bullet[i]->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
		}
		m_Max = 3;
		break;
	case CPlayer::SHOTGUN:
		for (int i = 0; i < 3; i++)
		{
			m_Bullet[i]->SetTexture(CTexture::TEXTURE_ITEM_SHOTGUN);
			m_Bullet[i]->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
		}
		m_Max = 3;
		break;
	case CPlayer::STUN_BULLET:
		m_alpha = 1.0f; 
		m_BoostTime = 180;
		m_Boost->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
		break;
	default:
		break;
	}

}

//--------------------------------------------------
// 玉を減らす関数
//--------------------------------------------------
void CStage_Ui::SetBullet()
{
	//if (m_Max > 0)
	//{
	//	m_Max--;
	//	m_Bullet[m_Max]->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.0f));
	//}
}

void CStage_Ui::PopEffect(D3DXVECTOR3 pos)
{
	
	CParticle*data = CParticle::Create(pos, D3DXVECTOR3(70.0f, 70.0f, 0.0f), 10, CParticle::MOVE_ITEM, PRIORITY_EFFECT);
	switch (m_Type)
	{
	case CPlayer::DEFAULT:

		break;
	case CPlayer::RANGE_EXTENSION:
		data->SetItem(CTexture::TEXTURE_EFFECT3);
		break;
	case CPlayer::SHOTGUN:
		data->SetItem(CTexture::TEXTURE_EFFECT2);
		break;
	case CPlayer::STUN_BULLET:
		data->SetItem(CTexture::TEXTURE_EFFECT1);
		break;
	default:
		break;
	}
	
}





