//==================================================
// game_mode.cpp
// Author: Buriya Kota
//==================================================

//**************************************************
// include
//**************************************************
#include "aubience_list.h"
#include "audience.h"
#include "manager.h"
#include "utility.h"
#include "objectX.h"
#include "speech.h"

const std::string fileName[] =
{
	"data\\TEXT\\hamadaEnemy\\snake.txt",
	"data\\TEXT\\hamadaEnemy\\Kuma.txt",
};
// namespace

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CAubience_list::CAubience_list(int nPriority) : CObject(nPriority)
{

}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CAubience_list::~CAubience_list()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CAubience_list::Init()
{
	m_delay = br::IntRandom(90, 30);
	m_beesY = br::FloatRandam(90, -90);
	m_timer = 0;

	float LiftX = 70.0f;

	int upperRow = 5;
	int downperRow = 2;

	D3DXVECTOR3 worldPos = D3DXVECTOR3((float)CManager::SCREEN_WIDTH, m_beesY, 400.0f);
	D3DXVECTOR3 Pos = worldPos;
	int Pass = br::IntRandom(2, 0);

	for (int i = 0; i < upperRow; i++)
	{
		m_Adience[i] = CAdience::Create(Pos, D3DXVECTOR3(1.0f, 1.0f, 1.0f), fileName[Pass]);
		Pos.x += LiftX;
		Pass = br::IntRandom(2, 0);

	}
	
	m_Point = Pos;
	Pos = worldPos;
	//Pos.x += 100;

	//for (int i = upperRow; i < downperRow+ upperRow; i++)
	//{
	//	
	//	Pass = br::IntRandom(2, 0);
	//	m_Adience[i] = CAdience::Create(Pos, D3DXVECTOR3(1.0f, 1.0f, 1.0f), fileName[Pass]);
	//	Pos.x += LiftX;
	//}
	
	m_Model = CObjectX::Create(PRIORITY_PLAYER);
	m_Model->SetPos(D3DXVECTOR3(Pos.x + 320.0f, Pos.y-30.0f, 500.0f));
	m_Model->SetModelData(CModelData::MODEL_DANDAI);
	m_Model->SetRot(D3DXVECTOR3(0.0f, 0.0f, 0.0f));
	return E_NOTIMPL;
}

//--------------------------------------------------
// 破棄
//--------------------------------------------------
void CAubience_list::Uninit()
{
	if (!IsDeleted())
	{
		for (int i = 0; i < aubience_list; i++)
		{
			m_Adience[i]->Uninit();
		}
		//m_Model->Uninit();
		CObject::DeletedObj();
	}
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CAubience_list::Update()
{
	m_timer++;
	if (m_timer >= m_delay)
	{
		//ランダムでコメント出す機構
		m_timer = 0;
		m_delay = br::IntRandom(90, 30);
		int select = br::IntRandom(4, 0);
		D3DXVECTOR3 pos = m_Adience[select]->GetPos();
		pos.y+= 150;
		int Type = br::IntRandom(3, 0);
		CSpeech::Create(pos, D3DXVECTOR3(0.0f, 0.0f, 0.0f), Type);
	}
	float move = -5.0f;
	
	for (int i = 0; i < aubience_list; i++)
	{//出てるモデル移動

		D3DXVECTOR3 Adience = m_Adience[i]->GetPos();
		Adience.x += move+0.6f;
		m_Adience[i]->SetPos(Adience);
		//m_Adience[i]->SetMove(D3DXVECTOR3(move, 0.0f, 0.0f));
	}
	D3DXVECTOR3 ModelPos = m_Model->GetPos();
	ModelPos.x += move;
	m_Model->SetPos(ModelPos);
	//判定する基準の移動
	m_Point.x += move;

	if (m_Point.x <= -1500)
	{//画面外削除

		CAubience_list::Uninit();
		CAubience_list::Create();
	}

}

//--------------------------------------------------
// 描画
//--------------------------------------------------
void CAubience_list::Draw(DRAW_MODE)
{

}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CAubience_list *CAubience_list::Create()
{
	CAubience_list *pObjectX;
	pObjectX = new CAubience_list(0);

	if (pObjectX != nullptr)
	{
		pObjectX->Init();
	}
	else
	{
		assert(false);
	}

	return pObjectX;
}
