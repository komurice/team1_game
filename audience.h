//==================================================
// player3D.h
// Author: Buriya Kota
//==================================================
#ifndef _ADIENCE_H_
#define _ADIENCE_H_

//**************************************************
// インクルード
//**************************************************
#include "object.h"
#include "motion_model3D.h"
#include "bullet.h"


//**************************************************
// 前方前言　実態はNG　ポインタだけならOK
//**************************************************


//**************************************************
// 定数定義
//**************************************************
#define MAX_PARTS		(13)
#define MAX_KEYDATA		(2)
#define MAX_MOTION		(5)

//**************************************************
// 構造体定義
//**************************************************

//**************************************************
// クラス
//**************************************************
class CAdience : public CMotionModel3D
{
public:
	static const int MAX_PLAYER = 4;
	static const int MAX_STUN = 5;
	static const int STUN_FRAME = 60;

public:
	

	enum BULLET_TYPE
	{
		DEFAULT = 0,
		RANGE_EXTENSION,
		SHOTGUN,
		STUN_BULLET,
		MAX_BULLET
	};

public:
	static const float PLAYER_SPEED;

	enum ACTION_TYPE
	{
		TYPE_NEUTRAL = 0,		// ニュートラル
		TYPE_MOVE,				// 移動
		TYPE_ATTACK,			// 攻撃
		TYPE_JUMP,				// JUMP
		TYPE_JUMPOUT,			// 着地
		TYPE_MAX,				// 最大数
	};

	enum PLAYER_STATE
	{
		PLAYER_NONE = 0,
		PLAYER_GROUND,
		PLAYER_SKY,
		PLAYER_STATE_MAX
	};

private:
	enum EState
	{
		STATE_IDLE = 0,
		STATE_LAND,
		STATE_FLY,
		STATE_MAX,
		STATE_INVALID = -1,
	};

private:
	static const UPDATE_FUNC mUpdateFunc[];

public:
	explicit CAdience(int nPriority = PRIORITY_PLAYER);
	~CAdience();

	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw(DRAW_MODE drawMode) override;

	//　プレイヤーのステート関数
	void Update_Idle();
	void Update_Land();
	void Update_Fly();

	static CAdience* Create(D3DXVECTOR3 pos, D3DXVECTOR3 scale, std::string Model);

	// セッター
	void SetPos(const D3DXVECTOR3& pos) { m_pos = pos; CMotionModel3D::SetPos(pos); }
	void SetPosOld(const D3DXVECTOR3& posOld) { m_posOld = posOld; }
	void SetMove(const D3DXVECTOR3& move) { m_move = move; }
	void SetRot(const D3DXVECTOR3& rot) { m_rot = rot;   CMotionModel3D::SetRot(rot); }
	void SetRotDest(const D3DXVECTOR3& rotDest) { m_rotDest = rotDest; }
	void SetSize(const D3DXVECTOR3& size) { m_size = size; }
	void SetWorldMtx(const D3DXMATRIX& mtx) { m_mtx = mtx; }
	void SetScale(const D3DXVECTOR3& scale) { m_scale =  scale;}
	// ゲッター
	const D3DXVECTOR3& GetPos() const { return m_pos; }
	const D3DXVECTOR3& GetPosOld() const { return m_posOld; }
	const D3DXVECTOR3& GetMove() const { return m_move; }
	const D3DXVECTOR3& GetRot() const { return m_rot; }
	const D3DXVECTOR3& GetRotDest() const { return m_rotDest; }
	const D3DXVECTOR3& GetSize() const { return m_size; }


	// 角度の正規化
	D3DXVECTOR3 RotNormalization(D3DXVECTOR3 rot);
	D3DXVECTOR3 RotDestNormalization(D3DXVECTOR3 rot, D3DXVECTOR3 rotDest);

	// 移動量代入
	void MovePos(const D3DXVECTOR3& move) { m_pos += move; }


private:
	


private:
	// モデルの情報

	D3DXMATRIX m_mtx;
	D3DXVECTOR3 m_pos;
	D3DXVECTOR3 m_posOld;
	D3DXVECTOR3 m_move;
	D3DXVECTOR3 m_size;
	D3DXVECTOR3 m_rot;
	D3DXVECTOR3 m_rotDest;
	D3DXVECTOR3 m_scale;

	ACTION_TYPE m_action;

	int m_nSetModel;
	int m_nSetCurrentMotion;

	// ステート
	EState m_state;
};

#endif	// _PLAYER3D_H_