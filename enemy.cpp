//**************************************************
// include
//**************************************************
#include "enemy.h"

#include <assert.h>

#include "utility.h"
#include "debug_proc.h"

#include "game.h"
#include "tutorial.h"

#include "manager.h"
#include "camera.h"
#include "sound.h"
#include "input.h"
#include "model.h"

#include "object3D.h"
#include "player.h"
#include "bullet.h"
#include "map.h"
#include "motion.h"
#include "particle.h"
#include "fade.h"
#include "stage_ui.h"

#include "player.h"

//**************************************************
// マクロ定義
//**************************************************
#define MAX_SPEED			(10.0f)
#define FRICTION			(0.07f)
#define GRAVITY				(0.5f)
#define MAX_ACCEL			(1.0f)
#define MIN_ACCEL			(0.0f)
#define COIN_PARTICLE		(5)
#define NUM_GOAL			(2)
#define KNOCKBACK			(20.0f)
#define BULLET_TIME			(12)
#define JUMP				(15.0f)
#define X_MOVE_LIMIT		(100.0f)
#define Y_MOVE_LIMIT		(6.0f)
#define KNOCK_BACK_POWER_X	(0.7f)
#define KNOCK_BACK_POWER_Y	(8.0f)
#define CHARGE_TIME			(120)

namespace
{
	const std::string fileName[5] =
	{
		"data/TEXT/motion_sai.txt",
		"data/TEXT/motion_fox.txt",
		"data/TEXT/motion_araiguma.txt",
		"data/TEXT/motion_kyouryu.txt",
	};
} // namespace

//**************************************************
// 静的メンバ変数
//**************************************************
const float CEnemy::PLAYER_SPEED = 5.0f;

// 
const CObject::UPDATE_FUNC CEnemy::mUpdateFunc[] =
{
	UPDATE_FUNC_CAST(Update_Idle),
	UPDATE_FUNC_CAST(Update_Land),
	UPDATE_FUNC_CAST(Update_Fly),
};

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CEnemy::CEnemy(int nPriority) : CMotionModel3D(nPriority)
{
	// 数が一致しなかったらエラー
	static_assert(std::size(mUpdateFunc) == STATE_MAX, "baka");
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CEnemy::~CEnemy()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CEnemy::Init()
{
	CMotionModel3D::Init();

	m_nCurrentKey = 0;

	m_pos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	m_posOld = D3DXVECTOR3(1.0f, 0.0f, 0.0f);
	m_rot = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	SetRot(m_rot);
	m_rotDest = D3DXVECTOR3(0.0f, D3DX_PI, 0.0f);
	m_move = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	m_size = D3DXVECTOR3(10.0f, 25.0f, 50.0f);

	InitStateFunc(mUpdateFunc, STATE_MAX);

	// プレイヤーの初期状態
	SetState(STATE_IDLE);

	m_Life = 100;
	m_bIsLife = true;
	return S_OK;
}

//--------------------------------------------------
// 終了
//--------------------------------------------------
void CEnemy::Uninit()
{
	// 現在のモーション番号の保管
	CMotionModel3D::Uninit();

	// deleteフラグを立てる
	CObject::DeletedObj();

}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CEnemy::Update()
{
	CObject::Update();
	CMotion* pMotion = CMotionModel3D::GetMotion();
	// posOld の更新
	D3DXVECTOR3 posOld = CMotionModel3D::GetPos();
	SetPosOld(posOld);

	if ((CManager::GetGameMode() == CManager::MODE_GAME) ||
		(CManager::GetGameMode() == CManager::MODE_TUTORIAL))
	{
		if (pMotion != nullptr && m_action != TYPE_ATTACK)
		{//アニメーション設定
			m_action = TYPE_ATTACK;
			pMotion->SetNumMotion(m_action);

		}
		//// 重力
	/*	m_move.y += -GRAVITY;
		m_move.x += (0.0f - m_move.x) * (0.025f); */   //（目的の値-現在の値）＊減衰係数

		Motion_();

		D3DXVECTOR3 pos = CMotionModel3D::GetPos();
		//ブースト
		m_move.x += (0.0f - m_move.x) * (0.025f);    //（目的の値-現在の値）＊減衰係数

		pos += m_move;
		SetPos(pos);

		CMotionModel3D::Update();
		m_Life--;
		if (m_Life <= 0)
		{
			m_Life = 0;
			m_bIsLife = false;
			//Uninit();
		}
	}

#ifdef _DEBUG
#endif // DEBUG
}

//--------------------------------------------------
// 描画
//--------------------------------------------------
void CEnemy::Draw(DRAW_MODE drawMode)
{
	CMotionModel3D::Draw(drawMode);
}


//--------------------------------------------------
// 生成
//--------------------------------------------------
CEnemy* CEnemy::Create(D3DXVECTOR3 pos, ENEMY_TYPE type)
{
	CEnemy* enemy = nullptr;
	enemy = new CEnemy;

	if (enemy != nullptr)
	{
		enemy->SetType(TYPE_ENEMY);
		enemy->SetEnemyType(type);
		enemy->Init();
		enemy->SetPos(pos);
		enemy->CMotionModel3D::SetPos(pos);
		enemy->SetMotion(fileName[type].c_str());
	}
	else
	{
		assert(false);
	}

	return enemy;
}


void CEnemy::SetEnemyType(ENEMY_TYPE type)
{
	m_enemy_type = type;
}

void CEnemy::Update_Idle()
{
	SetState(STATE_LAND);
}

void CEnemy::Update_Land()
{
	SetState(STATE_FLY);
}

void CEnemy::Update_Fly()
{
	SetState(STATE_IDLE);
}

//--------------------------------------------------
// 現在の角度の正規化
//--------------------------------------------------
D3DXVECTOR3 CEnemy::RotNormalization(D3DXVECTOR3 rot)
{
	m_rot = rot;

	// 現在の角度の正規化
	if (m_rot.y > D3DX_PI)
	{
		m_rot.y -= D3DX_PI * 2.0f;
	}
	else if (m_rot.y < -D3DX_PI)
	{
		m_rot.y += D3DX_PI * 2.0f;
	}

	return m_rot;
}

//--------------------------------------------------
// 目的の角度の正規化
//--------------------------------------------------
D3DXVECTOR3 CEnemy::RotDestNormalization(D3DXVECTOR3 rot, D3DXVECTOR3 rotDest)
{
	m_rot = rot;
	m_rotDest = rotDest;

	// 目的の角度の正規化
	if (m_rotDest.y - m_rot.y > D3DX_PI)
	{
		m_rotDest.y -= D3DX_PI * 2.0f;
	}
	else if (m_rotDest.y - m_rot.y < -D3DX_PI)
	{
		m_rotDest.y += D3DX_PI * 2.0f;
	}

	return m_rotDest;
}


//---------------------------------------------------------------------------
// ファイル読み込み処理
//---------------------------------------------------------------------------
void CEnemy::LoadSetFile(char* Filename)
{
	m_nSetCurrentMotion = 0;
	char modelFile[256];
	char String[256];
	// ファイルポインタの宣言
	FILE* pFile;

	// ファイルを開く
	pFile = fopen(Filename, "r");

	if (pFile != NULL)
	{// ファイルが開いた場合
		fscanf(pFile, "%s", &String);

		while (strncmp(&String[0], "SCRIPT", 6) != 0)
		{// スタート来るまで空白読み込む
			String[0] = {};
			fscanf(pFile, "%s", &String[0]);
		}
		D3DXVECTOR3	s_modelMainpos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
		while (strncmp(&String[0], "END_SCRIPT", 10) != 0)
		{// 文字列の初期化と読み込み// 文字列の初期化と読み込み
			fscanf(pFile, "%s", &String[0]);

			if (strcmp(&String[0], "MODEL_FILENAME") == 0)
			{// 文字列が一致した場合
				fscanf(pFile, "%s", &modelFile);
			}
			if (strcmp(&String[0], "MAINPOS") == 0)
			{// 文字列が一致した場合
				fscanf(pFile, "%s", &String[0]);		// ＝読み込むやつ
				fscanf(pFile, "%f", &s_modelMainpos.x);
				fscanf(pFile, "%f", &s_modelMainpos.y);
				fscanf(pFile, "%f", &s_modelMainpos.z);
			}
			else if (strcmp(&String[0], "CHARACTERSET") == 0)
			{
				while (1)
				{
					int Index = 0;
					fscanf(pFile, "%s", &String[0]);

					if (strcmp(&String[0], "PARTSSET") == 0)
					{
						while (1)
						{// 文字列の初期化と読み込み// 文字列の初期化と読み込み
							fscanf(pFile, "%s", &String[0]);
							if (strcmp(&String[0], "INDEX") == 0)
							{// 文字列が一致した場合
								fscanf(pFile, "%s", &String[0]);
								fscanf(pFile, "%d", &Index);
							}
							if (strcmp(&String[0], "POS") == 0)
							{
								D3DXVECTOR3 Pos = { 0.0f,0.0f,0.0f };
								fscanf(pFile, "%s", &String[0]);
								fscanf(pFile, "%f", &Pos.x);
								fscanf(pFile, "%f", &Pos.y);
								fscanf(pFile, "%f", &Pos.z);
								m_pModel[Index]->SetPos(Pos);
							}
							if (strcmp(&String[0], "ROT") == 0)
							{
								D3DXVECTOR3 Rot = { 0.0f,0.0f,0.0f };
								fscanf(pFile, "%s", &String[0]);
								fscanf(pFile, "%f", &Rot.x);
								fscanf(pFile, "%f", &Rot.y);
								fscanf(pFile, "%f", &Rot.z);
								m_pModel[Index]->SetRot(Rot);
							}
							if (strcmp(&String[0], "END_PARTSSET") == 0)
							{
								break;
							}
						}
					}
					if (strcmp(&String[0], "END_CHARACTERSET") == 0)
					{
						break;
					}
				}

			}
			else if ((strcmp(&String[0], "MODELSET") == 0) || (strcmp(&String[0], "MOTIONSET") == 0))
			{// 文字列が一致した場合
				D3DXVECTOR3	s_modelpos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
				D3DXVECTOR3	s_modelrot = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

				while (1)
				{
					// 文字列の初期化と読み込み
					String[0] = {};
					fscanf(pFile, "%s", &String[0]);

					if (strncmp(&String[0], "#", 1) == 0)
					{// これのあとコメント
						fgets(&String[0], sizeof(String), pFile);
						continue;
					}

					if (strcmp(&String[0], "POS") == 0)
					{// 文字列が一致した場合
						fscanf(pFile, "%s", &String[0]);		// ＝読み込むやつ
						fscanf(pFile, "%f", &s_modelpos.x);
						fscanf(pFile, "%f", &s_modelpos.y);
						fscanf(pFile, "%f", &s_modelpos.z);
					}

					if (strcmp(&String[0], "ROT") == 0)
					{// 文字列が一致した場合
						fscanf(pFile, "%s", &String[0]);		// ＝読み込むやつ
						fscanf(pFile, "%f", &s_modelrot.x);
						fscanf(pFile, "%f", &s_modelrot.y);
						fscanf(pFile, "%f", &s_modelrot.z);
					}

					if (strcmp(&String[0], "LOOP") == 0)
					{// 文字列が一致した場合//ループするかしないか1する０しない
						fscanf(pFile, "%s", &String[0]);		// ＝読み込むやつ
						fscanf(pFile, "%d", &m_ModelData[m_nSetCurrentMotion].LOOP);
					}

					if (strcmp(&String[0], "NUM_KEY") == 0)
					{// 文字列が一致した場合//キーの最大数
						fscanf(pFile, "%s", &String[0]);		// ＝読み込むやつ
						fscanf(pFile, "%d", &m_ModelData[m_nSetCurrentMotion].NUM_KEY);
					}

					if (strcmp(&String[0], "KEYSET") == 0)
					{// 文字列が一致した場合//アニメーションのファイルだったとき
						LoadKeySetFile(pFile);
					}

					if (strcmp(&String[0], "END_MOTIONSET") == 0)
					{// 一回のmotion読み込み切ったら次のmotionのセットに行くためにカウント初期化してデータを加算する
						m_nSetModel = 0;
						m_nSetCurrentMotion++;
					}

					if (strcmp(&String[0], "END_SCRIPT") == 0)
					{// 文字列が一致した場合
						break;
					}
				}
			}
		}

		//ファイルを閉じる
		fclose(pFile);
	}
}

//---------------------------------------------------------------------------
// ファイル読み込み処理
//---------------------------------------------------------------------------
void CEnemy::LoadKeySetFile(FILE* pFile)
{
	char String[256];
	int nSetKey = 0;

	while (1)
	{
		// 文字列の初期化と読み込み
		fscanf(pFile, "%s", &String[0]);

		if (strncmp(&String[0], "#", 1) == 0)
		{//コメント対策
			fgets(&String[0], sizeof(String), pFile);
			continue;
		}

		if (strcmp(&String[0], "FRAME") == 0)
		{// 文字列が一致した場合
			fscanf(pFile, "%s", &String[0]);	// イコール読み込む
			fscanf(pFile, "%d", &m_ModelData[m_nSetCurrentMotion].KeySet[m_nSetModel].nFrame);
		}

		if (strcmp(&String[0], "KEY") == 0)
		{
			while (1)
			{// 文字列の初期化と読み込み
				String[0] = {};
				fscanf(pFile, "%s", &String[0]);
				if (strncmp(&String[0], "#", 1) == 0)
				{
					fgets(&String[0], sizeof(String), pFile);		// コメント読み込む
					continue;
				}

				if (strcmp(&String[0], "POS") == 0)
				{// 文字列が一致した場合
					fscanf(pFile, "%s", &String[0]);	//イコール読み込む
					fscanf(pFile, "%f", &m_ModelData[m_nSetCurrentMotion].KeySet[m_nSetModel].aKey[nSetKey].pos.x);
					fscanf(pFile, "%f", &m_ModelData[m_nSetCurrentMotion].KeySet[m_nSetModel].aKey[nSetKey].pos.y);
					fscanf(pFile, "%f", &m_ModelData[m_nSetCurrentMotion].KeySet[m_nSetModel].aKey[nSetKey].pos.z);
				}

				if (strcmp(&String[0], "ROT") == 0)
				{// 文字列が一致した場合
					fscanf(pFile, "%s", &String[0]);	//イコール読み込む
					fscanf(pFile, "%f", &m_ModelData[m_nSetCurrentMotion].KeySet[m_nSetModel].aKey[nSetKey].rot.x);
					fscanf(pFile, "%f", &m_ModelData[m_nSetCurrentMotion].KeySet[m_nSetModel].aKey[nSetKey].rot.y);
					fscanf(pFile, "%f", &m_ModelData[m_nSetCurrentMotion].KeySet[m_nSetModel].aKey[nSetKey].rot.z);
				}

				if (strcmp(&String[0], "END_KEY") == 0)
				{
					nSetKey++;//パーツの数がどんどん増える
					break;
				}
			}
		}

		if (strcmp(&String[0], "END_KEYSET") == 0)
		{// 文字列が一致した場合
			m_nSetModel++;		// 現在のセットしてる番号の更新
			nSetKey = 0;		// パーツの数初期化
			break;
		}
	}
}

void CEnemy::Motion_()
{
}

void CEnemy::Jump_()
{
}

//=============================================================================
// 当たり判定
//=============================================================================
bool CEnemy::Collision(const D3DXVECTOR3 /*InPos*/, const D3DXVECTOR3 /*InSize*/, CPlayer* player, bool isHit)
{
	D3DXVECTOR3 Size = GetSize();
	D3DXVECTOR3 Pos = GetPos();
	if (!IsDeleted())
	{
		bool bisHit = hitevent(player);
		if (bisHit && isHit)
		{
			CManager::GetManager()->GetSound()->Play(CSound::LABEL_SE_ENEMYDAMAGE);	//hit音
			CManager::GetManager()->GetCamera()->ShakeCamera(60, 5.0f, D3DXVECTOR3(5.0f, 5.0f, 0.0f));
		}
	}


	return false;
}

//=============================================================================
// ヒット
//=============================================================================
bool CEnemy::hitevent(CPlayer* player)
{

	D3DXVECTOR3 Size = D3DXVECTOR3(50.0f, 80.0f, 0.0f);
	D3DXVECTOR3 Pos = GetPos();
	D3DXVECTOR3 PlayerSize = D3DXVECTOR3(0.0f, 20.0f, 50.0f);
	D3DXVECTOR3 PlayerPos = player->GetPos();
	D3DXVECTOR3 PlayerPosOld = player->GetPosOld();
	D3DXVECTOR3 PlayerMove = player->GetMove();

	bool IsHit = false;
	//左右の壁
	if (PlayerPos.y + PlayerSize.y > Pos.y - Size.y
		&& PlayerPosOld.y - PlayerSize.y < Pos.y + Size.y)
	{
		if (PlayerPos.x + PlayerSize.x > Pos.x - Size.x
			&& PlayerPosOld.x + PlayerSize.x < Pos.x - Size.x)
		{//ブロックの座標と座標が重なり合ったら//通常モード//左
			D3DXVECTOR3 move = -PlayerMove;
			player->SetKnockBack(move, true);
			player->AddStun(1);
			IsHit = true;
		}
		if (PlayerPos.x - PlayerSize.x  < Pos.x + Size.x
			&& PlayerPosOld.x - PlayerSize.x  > Pos.x + Size.x)
		{//ブロックの座標と座標が重なり合ったら//通常モード//右

			D3DXVECTOR3 move = -PlayerMove;
			player->SetKnockBack(move, true);
			player->AddStun(1);
			IsHit = true;
		}
	}

	//上と下のカベ処理
	if (PlayerPos.x + PlayerSize.x > Pos.x - Size.x
		&& PlayerPos.x - PlayerSize.x < Pos.x + Size.x)
	{
		if (PlayerPos.y - PlayerSize.y < Pos.y + Size.y
			&& PlayerPosOld.y - PlayerSize.y >= Pos.y + Size.y)
		{//ブロックの座標と座標が重なり合ったら//通常モード//上

			D3DXVECTOR3 move = -PlayerMove;
			player->SetKnockBack(move, true);
			player->AddStun(1);
			IsHit = true;
		}
		if (PlayerPos.y + PlayerSize.y > Pos.y - Size.y
			&& PlayerPosOld.y + PlayerSize.y <= Pos.y - Size.y)
		{//ブロックの座標と座標が重なり合ったら//通常モード//下

			D3DXVECTOR3 move = -PlayerMove;
			player->SetKnockBack(move, true);
			player->AddStun(1);
			IsHit = true;
		}
	}

	player->SetPos(PlayerPos);
	player->SetMove(PlayerMove);
	return IsHit;
}


