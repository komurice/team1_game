//============================
//
// 背景
// Author:hamada ryuuga
//
//============================
#ifndef _RING_H_
#define _RING_H_

//**************************************************
// インクルード
//**************************************************
#include "object3D.h"
#include "model_data.h"
class CObjectX;
class CModelData;
//**************************************************
// クラス
//**************************************************
class CRing : public CObject
{
public:

	enum MOVE_STATE
	{
		MOVE_NONE = 0,
		MOVE_WORLD,
		MOVE_SKY,
		MOVE_STATE_MAX
	};
	const float Timer = 0.001f;
	const float VecMove = 0.1f;
	explicit CRing(int nPriority = PRIORITY_EFFECT);
	~CRing();

	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw(DRAW_MODE drawMode) override;

	static CRing *CRing::Create(D3DXVECTOR3 Pos, int Is);
	
	void SetPos(D3DXVECTOR3 Pos) { m_pos = Pos; }
	void SetPlayerNum(int Is);
private:
	CObject3D *m_clown;
	CObject3D *m_Bgblender;
	D3DXVECTOR3 m_size;
	int m_count;
	bool m_is;
	bool m_IsResult;
	D3DXVECTOR3 m_pos;
};

#endif

