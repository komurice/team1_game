//============================
//
// マップチップ
// Author:hamada ryuuga
//
//============================

//**************************************************
// include
//**************************************************
#include <assert.h>
#include "manager.h"
#include "bg.h"
#include "objectX.h"
#include "object2D.h"


//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CBg::CBg(int nPriority /* =0 */) : CObject3D(nPriority)
{
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CBg::~CBg()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CBg::Init()
{
	CObject3D::Init();

	CObject3D::SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.0f));
	CObject3D::SetSize(D3DXVECTOR3(0.0f, 0.0f, 0.0f));

	m_Bgblender[0] = CObject3D::Create(D3DXVECTOR3(0.0f, 0.0f, 100.0f),
		D3DXVECTOR3(CManager::SCREEN_WIDTH*0.5f, CManager::SCREEN_HEIGHT*0.6f, 0.0f), GetTex());
	m_Bgblender[0]->SetTexture(CTexture::TEXTURE_BG1);
	m_Bgblender[0]->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f,1.0f));

	m_Bgblender[1] = CObject3D::Create(D3DXVECTOR3(0.0f, 0.0f,100.0f),
		D3DXVECTOR3(CManager::SCREEN_WIDTH*0.5f, CManager::SCREEN_HEIGHT*0.6f, 0.0f), GetTex());
	m_Bgblender[1]->SetTexture(CTexture::TEXTURE_BG2);
	m_Bgblender[1]->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f,0.0f));

	m_Bgblender[2] = CObject3D::Create(D3DXVECTOR3(0.0f, 0.0f, 100.0f),
		D3DXVECTOR3(CManager::SCREEN_WIDTH*0.5f, CManager::SCREEN_HEIGHT*0.6f, 0.0f), PRIORITY_UI3D);
	m_Bgblender[2]->SetTexture(CTexture::TEXTURE_NONE);
	m_Bgblender[2]->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.0f));
	m_blender = 0.0f;
	m_Noudraw = 0;
	m_Nextdraw = 1;
	m_MoveVec = false;
	m_bgChange = false;
	m_EventEnd = false;
	m_sun = nullptr;
	m_moon = nullptr;
	//BlenderTex(CTexture::TEXTURE_BG1, CTexture::TEXTURE_BG2, CTexture::TEXTURE_BG3);
	m_speed = 0.001f;
	return S_OK;
}

//--------------------------------------------------
// 終了
//--------------------------------------------------
void CBg::Uninit()
{
	CObject3D::Uninit();
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CBg::Update()
{
	CObject3D::Update();
	if (m_bgChange)
	{
		m_blender += m_speed;

		if (m_blender >= 1.0f && !m_EventEnd)
		{

			m_Noudraw = 0;
			m_Nextdraw = m_Noudraw + 1;
			
			m_Bgblender[0]->SetTexture(m_List[0]);
			m_Bgblender[1]->SetTexture(m_List[1]);

		}
		if (m_blender >= 1.0f)
		{
			m_EventEnd = true;
			m_Bgblender[2]->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 2.0f - m_blender));
		}
		else
		{
			
			m_Bgblender[2]->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, m_blender));
		}
		
		if (m_EventEnd&&m_blender >= 2.0f)
		{
		
			m_blender = 0.0f;
			m_Bgblender[2]->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.0f));

			m_bgChange = false;
		}
		
	}
	else
	{
		if (m_List.size() == 0)
		{
			return;
		}
		m_blender += m_speed;
		if (m_blender >= 1.0f)
		{
		m_blender = 0.0f;
			m_Noudraw++;
			m_Nextdraw++;
			int Size = m_List.size();
			if (m_Nextdraw >= Size)
			{
				m_Nextdraw = 0;
			}
			if (m_Noudraw >= Size)
			{
				m_MoveVec = !m_MoveVec;
				m_Noudraw = 0;
			}

		}
		else
		{
			m_Bgblender[0]->SetTexture(m_List[m_Noudraw]);
			m_Bgblender[1]->SetTexture(m_List[m_Nextdraw]);
			m_Bgblender[0]->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f - m_blender));
			m_Bgblender[1]->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, m_blender));
		}

		Move(m_moveType);
	}
	

}

//--------------------------------------------------
// 描画
//--------------------------------------------------
void CBg::Draw(DRAW_MODE drawMode)
{
	CObject3D::Draw(drawMode);
}

//--------------------------------------------------
// 生成関数
//--------------------------------------------------
CBg * CBg::Create(D3DXVECTOR3 /*pos*/, D3DXVECTOR3 /*size*/, int nPriority)
{
	CBg *pObject2D;
	pObject2D = new CBg(nPriority);

	if (pObject2D != nullptr)
	{
		pObject2D->SetTex(nPriority);
		pObject2D->Init();
		
	}
	else
	{
		assert(false);
	}

	return pObject2D;
}

//--------------------------------------------------
// 設定
//--------------------------------------------------
void CBg::SetSize(D3DXVECTOR3 size)
{
	m_Bgblender[0]->SetSize(size);
	m_Bgblender[1]->SetSize(size);
	m_Bgblender[2]->SetSize(size);
}

//--------------------------------------------------
// 設定
//--------------------------------------------------
void CBg::SetPos(D3DXVECTOR3 pos)
{
	m_Bgblender[0]->SetPos(pos);
	m_Bgblender[1]->SetPos(pos);
}

//--------------------------------------------------
// ストリートビュー設定
//--------------------------------------------------
void CBg::BlenderTex(CTexture::TEXTURE Fast, CTexture::TEXTURE Second, CTexture::TEXTURE Third)
{

	m_List.clear();
	m_List.push_back(Fast);
	m_List.push_back(Second);
	m_List.push_back(Third);
	m_Bgblender[0]->SetTexture(Fast);
	m_Bgblender[1]->SetTexture(Second);

	//m_Bgblender[2]->SetTexture(Third);
}

void CBg::SetModel(CModelData::MODEL_TYPE  InData, CModelData::MODEL_TYPE  InData2, MOVE_STATE Data)
{
	
	if (m_sun != nullptr)
	{
		m_sun->Uninit();
	}
	if (m_moon != nullptr)
	{
		m_moon->Uninit();
	}
	m_sun = CObjectX::Create();
	m_moon = CObjectX::Create();
	m_sun->SetModelData(InData);
	m_sun->SetPos(D3DXVECTOR3(200.0f, 200.0f, 0.0f));
	m_moon->SetModelData(InData2);
	m_moon->SetPos(D3DXVECTOR3(-200.0f, -200.0f, 0.0f));
	m_moveType = Data;

}

//--------------------------------------------------
// 設定
//--------------------------------------------------
void CBg::Move(MOVE_STATE Move)
{
	
	switch (Move)
	{
	case CBg::MOVE_NONE:
		
		break;
	case CBg::MOVE_WORLD:
	{
		D3DXVECTOR3 SunPos = m_sun->GetPos();
		D3DXVECTOR3 MoonPos = m_moon->GetPos();

		if (m_MoveVec)
		{
			SunPos.y += VecMove;
			m_sun->SetPos(SunPos);
			MoonPos.y -= VecMove;
			m_moon->SetPos(MoonPos);

		}
		else
		{
			SunPos.y -= VecMove;
			m_sun->SetPos(SunPos);
			MoonPos.y += VecMove;
			m_moon->SetPos(MoonPos);

		}
	}
		break;
	case CBg::MOVE_SKY:

		break;
	case CBg::MOVE_STATE_MAX:
		break;
	default:
		break;
	}


}

//--------------------------------------------------
// ストリートビュー設定
//--------------------------------------------------
void CBg::AddTex(CTexture::TEXTURE add)
{
	m_List.push_back(add);
}

//--------------------------------------------------
// ストリートビュー設定
//--------------------------------------------------
void CBg::ChangeTex(CTexture::TEXTURE Fast, CTexture::TEXTURE Second, CTexture::TEXTURE Third)
{
	//CBg::Init();
	m_Log.clear();
	m_bgChange = true;
	m_Log.push_back(Fast);
	m_Log.push_back(Second);
	m_Log.push_back(Third);
	m_List = m_Log;
}

//--------------------------------------------------
// ストリートビュー設定
//--------------------------------------------------
void CBg::ChangeTex()
{
	//CBg::Init();
	//m_Log.clear();
	m_bgChange = true;
	m_EventEnd = false;
	m_blender = 0.0f;
	

}

