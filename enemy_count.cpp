//==================================================
// timer.cpp
// Author: hamada
//==================================================

//**************************************************
// include
//**************************************************
#include "manager.h"
#include "game.h"
#include "fade.h"

#include "enemy_count.h"
#include "score.h"
#include "number.h"
#include "cpu.h"
#include "utility.h"

//**************************************************
// 静的メンバ変数
//**************************************************

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CEnemyCount::CEnemyCount(int nPriority) : CObject(nPriority)
{
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CEnemyCount::~CEnemyCount()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CEnemyCount::Init()
{
	return S_OK;
}

//--------------------------------------------------
// 初期化　オーバーロード
//--------------------------------------------------
HRESULT CEnemyCount::Init(D3DXVECTOR3 pos, D3DXVECTOR3 size)
{
	m_nTime = 0;
	m_bIsStop = false;
	m_nFrame = 0;
	m_nSceneFrame = 0;
	m_popEnemy = 0;
	m_popMax = 0;
	for (int nCnt = 0; nCnt < NUM_TIME; nCnt++)
	{
		m_pNumber[nCnt] = nullptr;
		m_pNumber[nCnt] = CNumber::Create(D3DXVECTOR3(size.x * nCnt + pos.x, pos.y, 0.0f), size);
		m_pNumber[nCnt]->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
		m_pNumber[nCnt]->SetTexture(CTexture::TEXTURE_NUMBER);
	}

	return S_OK;
}

//--------------------------------------------------
// 終了
//--------------------------------------------------
void CEnemyCount::Uninit()
{
	CObject::DeletedObj();
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CEnemyCount::Update()
{
	m_nFrame++;

	if (m_nFrame >= 150)
	{
		m_nFrame = 0;
		
		if (m_popEnemy <= 3&& m_popMax <= 9)
		{
		
			m_popMax++;
			m_popEnemy++;
			int Pop = br::IntRandom(3, -3);
			D3DXVECTOR3 Pos = D3DXVECTOR3(150.0f*Pop, 500.0f, 0.0f);
			CCpu::Create(Pos, 99, 1);


		}

	}
}

//--------------------------------------------------
// 位置の設定と大きさの設定
//--------------------------------------------------
void CEnemyCount::SetPos(D3DXVECTOR3 pos, D3DXVECTOR3 size)
{
	for (int nCnt = 0; nCnt < NUM_TIME; nCnt++)
	{
		m_pNumber[nCnt]->SetPos(D3DXVECTOR3(size.x * nCnt + pos.x, pos.y, 0.0f));
	}
}

//--------------------------------------------------
// タイムの設定
//--------------------------------------------------
void CEnemyCount::SetTimer(int nTime)
{
	m_nTime = nTime;

	int aPosTexU[3];		//各桁の数字を格納

	{
		int timer = m_nTime;
		for (int i = 2; i >= 0; --i)
		{
			aPosTexU[i] = timer % 10;
			timer /= 10;
		}
	}

	// テクスチャ座標の設定
	for (int nCnt = 0; nCnt < NUM_TIME; nCnt++)
	{
		m_pNumber[nCnt]->AnimTexture(aPosTexU[nCnt], 10);
	}
	if (m_nTime <= 0)
	{
		if (CManager::GetGameMode() == CManager::MODE_GAME)
		{
			// 遷移
			CFade::GetInstance()->SetFade(CManager::MODE_RANKING);
		}
		else
		{
			// 遷移
			CFade::GetInstance()->SetFade(CManager::MODE_MAP_SELECT);
		}
		

	}

}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CEnemyCount *CEnemyCount::Create(D3DXVECTOR3 pos, D3DXVECTOR3 size)
{
	CEnemyCount *pTimer;
	pTimer = new CEnemyCount;

	if (pTimer != nullptr)
	{
		pTimer->Init(pos, size);
	}
	else
	{
		assert(false);
	}

	return pTimer;

}

//--------------------------------------------------
// 減らす
//--------------------------------------------------
void CEnemyCount::SudEnemy()
{
	if (m_nTime > 0)
	{
		SubTimer(1);
		m_popEnemy-- ;
	}
}