//============================
//
// ブロック
// Author:hamada ryuuga
//
//============================

//**************************************************
// include
//**************************************************
#include <assert.h>
#include "manager.h"
#include "tutorial.h"
#include "block.h"
#include "model_data.h"
#include "input.h"
#include "utility.h"
#include "camera.h"
#include "debug_proc.h"
#include "game.h"
#include "player.h"
#include "map.h"
#include "fade.h"
#include "player_select.h"
#include "model_data.h"
#include "sound.h"

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CBlock::CBlock(int nPriority /* =6 */) : CObjectX(nPriority)
{
	m_Model = BLOCKTYPE_MAX;
	m_MovePos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	m_isBroken = false;
	m_brokenTimer = 0;
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CBlock::~CBlock()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CBlock::Init()
{
	m_IsCollision = false;
	CObjectX::Init();


	switch (m_Model)
	{
	case CBlock::BLOCKTYPE_NORMAL:
		CObjectX::SetModelData(CModelData::MODEL_BOX);
		break;
	case CBlock::BLOCKTYPE_LEFT:
		CObjectX::SetModelData(CModelData::MODEL_BOXLEFT);
		break;
	case CBlock::BLOCKTYPE_RIGHT:
		CObjectX::SetModelData(CModelData::MODEL_BOXRIGHT);
		break;
	case CBlock::BLOCKTYPE_BROKEN:
		CObjectX::SetModelData(CModelData::MODEL_BROKEN);
		break;

	case CBlock::BLOCKTYPE_PODIUM1:
		CObjectX::SetModelData(CModelData::MODEL_CAKE);
		break;
	case CBlock::BLOCKTYPE_CLEENNESS:
		CObjectX::SetModelData(CModelData::MODEL_CLEENNESS_OBJECT);
		break;

	case CBlock::BLOCKTYPE_MAX:
		break;
	default:
		break;
	}
	SetType(TYPE_BLOCK);

	D3DXVECTOR3 blockSize = D3DXVECTOR3(25.0f, 25.0f, 25.0f);
	CModelData* Model = CManager::GetManager()->GetModelData();
	SetSize(blockSize);

	return S_OK;
}

//--------------------------------------------------
// 終了
//--------------------------------------------------
void CBlock::Uninit()
{
	CObjectX::Uninit();
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CBlock::Update()
{

	CModelData* Model = CManager::GetManager()->GetModelData();
	D3DXVECTOR3 Size = Model->GetModel(GetModelData()).size;
	CObjectX::Update();
	D3DXVECTOR3 Pos = GetPos();
	//Pos.y += BLOCKSIZE;

	if (m_isBroken)
	{
		m_brokenTimer++;
	}

	if (m_brokenTimer > REVIVAL_TIME)
	{
		m_isBroken = false;
		m_brokenTimer = 0;
	}

	SetPos(D3DXVECTOR3(Pos));
}

//--------------------------------------------------
// 描画
//--------------------------------------------------
void CBlock::Draw(DRAW_MODE drawMode)
{
	if (!m_isBroken)
	{
		CObjectX::Draw(drawMode);
	}
}

//--------------------------------------------------
// 生成関数
//--------------------------------------------------
CBlock* CBlock::Create(D3DXVECTOR3 pos, int Model, BLOCKTYPE SetModel)
{
	CBlock* pObjectX;
	pObjectX = new CBlock;

	if (pObjectX != nullptr)
	{
		pObjectX->SetBlockType(SetModel);
		pObjectX->Init();
		pObjectX->SetPos(pos);
		pObjectX->SetPosOriginPos(pos);
		pObjectX->SetModelType(Model);
	}
	else
	{
		assert(false);
	}

	return pObjectX;

}

//=============================================================================
// 当たり判定
//=============================================================================
bool CBlock::Collision(const D3DXVECTOR3 /*PlayerPos*/, const D3DXVECTOR3 /*PlayerSize*/, CPlayer* player)
{
	CModelData* Model = CManager::GetManager()->GetModelData();

	D3DXVECTOR3 Size = Model->GetModel(GetModelData()).size;
	D3DXVECTOR3 Pos = GetPos();
	bool IsHit = false;

	if (!m_isBroken)
	{
		IsHit = hitevent(player);
	}

	return IsHit;
}

//=============================================================================
// ヒット
//=============================================================================
bool CBlock::hitevent(CPlayer* player)
{
	D3DXVECTOR3 Size = m_size;
	D3DXVECTOR3 Pos = GetPos();
	D3DXVECTOR3 PlayerSize = D3DXVECTOR3(20.0f, 40.0f, 50.0f);
	D3DXVECTOR3 PlayerPos = player->GetPos();
	D3DXVECTOR3 PlayerPosOld = player->GetPosOld();
	D3DXVECTOR3 PlayerMove = player->GetMove();
	bool IsHit = false;
	//左右の壁
	if (PlayerPos.y + PlayerSize.y > Pos.y - Size.y
		&& PlayerPosOld.y - PlayerSize.y < Pos.y + Size.y)
	{
		if (PlayerPos.x + PlayerSize.x > Pos.x - Size.x
			&& PlayerPosOld.x + PlayerSize.x <= Pos.x - Size.x)
		{//ブロックの座標と座標が重なり合ったら//通常モード//左
			PlayerPos.x = Pos.x - Size.x - PlayerSize.x - 3.0f;
			PlayerMove.x = -3.0f;
		}
		if (PlayerPos.x - PlayerSize.x < Pos.x + Size.x
			&& PlayerPosOld.x - PlayerSize.x >= Pos.x + Size.x)
		{//ブロックの座標と座標が重なり合ったら//通常モード//右

			PlayerPos.x = Pos.x + Size.x + PlayerSize.x + 3.0f;
			PlayerMove.x = +3.0f;
		}
	}

	//上と下のカベ処理
	if (PlayerPos.x + PlayerSize.x > Pos.x - Size.x
		&& PlayerPos.x - PlayerSize.x + 5.0f < Pos.x + Size.x)
	{
		if (PlayerPos.y - PlayerSize.y < Pos.y + Size.y
			&& PlayerPosOld.y - PlayerSize.y >= Pos.y + Size.y)
		{//ブロックの座標と座標が重なり合ったら//通常モード//上

			PlayerPos.y = Pos.y + Size.y + PlayerSize.y + 0.1f;
			PlayerMove.y = 0.0f;
			IsHit = true;

		}
		if (PlayerPos.y + PlayerSize.y > Pos.y - Size.y
			&& PlayerPosOld.y + PlayerSize.y <= Pos.y - Size.y)
		{//ブロックの座標と座標が重なり合ったら//通常モード//下

			PlayerPos.y = Pos.y - Size.y - PlayerSize.y - 1.0f;
			PlayerMove.y = 0.0f;

			if (m_Model == BLOCKTYPE_BROKEN)
			{
				m_isBroken = true;
				CManager::GetManager()->GetSound()->Play(CSound::LABEL_SE_BROKEN);					//壊れた時の音
			}
		}
	}

	player->SetPos(PlayerPos);
	player->SetMove(PlayerMove);
	return IsHit;
}

