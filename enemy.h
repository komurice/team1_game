#ifndef _ENEMY_H_
#define _ENEMY_H_

//**************************************************
// インクルード
//**************************************************
#include "object.h"
#include "motion_model3D.h"

//**************************************************
// 前方前言　実態はNG　ポインタだけならOK
//**************************************************
class CLockOnCursor;
class CModel;
class CPlayer;
class CStage_Ui;
class CObject3D;

//**************************************************
// 定数定義
//**************************************************
#define MAX_PARTS		(13)
#define MAX_KEYDATA		(2)
#define MAX_MOTION		(5)

//**************************************************
// 構造体定義
//**************************************************

//**************************************************
// クラス
//**************************************************
class CEnemy : public CMotionModel3D
{
public:
	struct KEY
	{
		D3DXVECTOR3 pos;
		D3DXVECTOR3 rot;
	};

	struct KEY_SET
	{// std::vector map を勉強したら動的に配列の値を変えれる
		int nFrame;
		KEY aKey[MAX_PARTS];
	};

	//modelデータの構造体//
	struct MODELDATA
	{
		int  LOOP;		// ループするかどうか[0:ループしない / 1 : ループする]
		int NUM_KEY;  	// キー数
		KEY_SET KeySet[MAX_KEYDATA];
	};


public:
	static const float PLAYER_SPEED;

	enum ACTION_TYPE
	{
		TYPE_NEUTRAL = 0,		// ニュートラル
		TYPE_MOVE,				// 移動
		TYPE_ATTACK,			// 攻撃
		TYPE_JUMP,				// JUMP
		TYPE_JUMPOUT,			// 着地
		TYPE_MAX,				// 最大数
	};

	enum ENEMY_TYPE
	{
		ENEMY_SAI = 0,
		ENEMY_FOX,
		ENEMY_ARAIGUMA,
		ENEMY_KYOURYU,
		max
	};

private:
	enum EState
	{
		STATE_IDLE = 0,
		STATE_LAND,
		STATE_FLY,
		STATE_MAX,
		STATE_INVALID = -1,
	};

private:
	static const UPDATE_FUNC mUpdateFunc[];

public:
	explicit CEnemy(int nPriority = PRIORITY_PLAYER);
	~CEnemy();

	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw(DRAW_MODE drawMode) override;

	static CEnemy* Create(D3DXVECTOR3 pos, ENEMY_TYPE type);

	// セッター
	void SetPos(const D3DXVECTOR3& pos) { m_pos = pos; CMotionModel3D::SetPos(pos); }
	void SetPosOld(const D3DXVECTOR3& posOld) { m_posOld = posOld; }
	void SetMove(const D3DXVECTOR3& move) { m_move = move; }
	void SetRot(const D3DXVECTOR3& rot) { m_rot = rot;   CMotionModel3D::SetRot(rot); }
	void SetRotDest(const D3DXVECTOR3& rotDest) { m_rotDest = rotDest; }
	void SetSize(const D3DXVECTOR3& size) { m_size = size; }
	void SetWorldMtx(const D3DXMATRIX& mtx) { m_mtx = mtx; }
	void SetEnemyType(ENEMY_TYPE type);

	// ゲッター
	const D3DXVECTOR3& GetPos() const { return m_pos; }
	const D3DXVECTOR3& GetPosOld() const { return m_posOld; }
	const D3DXVECTOR3& GetMove() const { return m_move; }
	const D3DXVECTOR3& GetRot() const { return m_rot; }
	const D3DXVECTOR3& GetRotDest() const { return m_rotDest; }
	const D3DXVECTOR3& GetSize() const { return m_size; }
	bool GetIsLive() { return m_bIsLife; }


	void Update_Idle();
	void Update_Land();
	void Update_Fly();


	// 角度の正規化
	D3DXVECTOR3 RotNormalization(D3DXVECTOR3 rot);
	D3DXVECTOR3 RotDestNormalization(D3DXVECTOR3 rot, D3DXVECTOR3 rotDest);

	// 移動量代入
	void MovePos(const D3DXVECTOR3& move) { m_pos += move; }

	bool CEnemy::Collision(const D3DXVECTOR3 PlayerPos, const D3DXVECTOR3 PlayerSize, CPlayer* player, bool isHit);
	bool CEnemy::hitevent(CPlayer* player);

private:
	void LoadSetFile(char* Filename);
	void LoadKeySetFile(FILE* pFile);
	void Motion_();
	void Jump_();


private:
	// モデルの情報
	CModel* m_pModel[MAX_PARTS];
	D3DXMATRIX m_mtx;
	D3DXVECTOR3 m_pos;
	D3DXVECTOR3 m_posOld;
	D3DXVECTOR3 m_move;
	D3DXVECTOR3 m_size;
	D3DXVECTOR3 m_rot;
	D3DXVECTOR3 m_rotDest;
	CStage_Ui* m_Ui;
	// キーの総数
	int m_nNumKey;
	// 現在のキー
	int m_nCurrentKey;
	// モーションカウンター
	int m_nCountMotion;
	//
	MODELDATA m_ModelData[MAX_MOTION];
	//
	int m_nSetModel;
	int m_nSetCurrentMotion;

	ENEMY_TYPE m_enemy_type;
	EState m_enemy_state;

	int m_Life;

	ACTION_TYPE m_action;

	bool m_bIsLife;
};

#endif	// _ENEMY_H_