//============================
//
// 演出
// Author:hamada ryuuga
//
//============================
#ifndef _EFFECT_H_
#define _EFFECT_H_

//**************************************************
// インクルード
//**************************************************
#include "object3D.h"
#include "particle.h"
//**************************************************
// クラス
//**************************************************
class CEffect : public CObject3D
{
public:
	enum OTO
	{// 状態
		OTO_BOKO = 0,
		OTO_BAKI,
		OTO_DOKO,
		OTO_SUTA,
		OTO_DA,
		OTO_ZA,
		OTO_MAX
	};
	const float Timer = 0.001f;

	explicit CEffect(int nPriority = PRIORITY_BG);
	~CEffect();

	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw(DRAW_MODE drawMode) override;

	void SetMove(D3DXVECTOR3 Move) { m_Move = Move; }
	void SetLife(int Life) { m_Life = Life; }
	static CEffect* CEffect::Create(D3DXVECTOR3 pos, D3DXVECTOR3 move, D3DXVECTOR3 Size, CParticle::MOVE mode, int nPriority, OTO oto = OTO_MAX, int life = 100);

	void SetDef();
	void SetDamage();
	void SetEffect(CParticle::MOVE Mode);
	void SetEffect(CTexture::TEXTURE tex);
	void setOto(OTO oto) { m_oto = oto; }

private:\
	CObject3D* m_Main;
	   float m_blender;
	   int m_Noudraw;
	   int m_Nextdraw;
	   int m_Life;
	   bool m_isal;
	   D3DXVECTOR3 m_Move;
	   CParticle::MOVE m_moveType;
	   OTO m_oto;
};

#endif

