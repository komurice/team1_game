//==================================================
// result.h
// Author: Buriya Kota
//==================================================
#ifndef _RESULT_H_
#define _RESULT_H_

//**************************************************
// インクルード
//**************************************************
#include "block.h"
#include "enemy.h"
#include "game_mode.h"
#include "texture.h"

//**************************************************
// 前方前言　実態はNG　ポインタだけならOK
//**************************************************
class CObject2D;
class CObject3D;
//**************************************************
// クラス
//**************************************************
class CResult : public CGameMode
{
public:
	CResult();
	~CResult() override;

	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw(DRAW_MODE /*drawMode*/) override {}

	bool blockHit(const D3DXVECTOR3 pos, const D3DXVECTOR3 Size, CPlayer* player);

	static CResult* GetResult() { return m_pResult; }

	static CResult* Create();

private:
	// オブジェクト2Dの箱
	CObject2D* m_pObject2D[2];
	// 位置
	D3DXVECTOR3 m_pos;
	// 大きさ
	D3DXVECTOR3 m_size;
	//プレイヤー
	std::vector<CPlayer*> m_pPlayer;

	std::vector<CEnemy*> m_enemy;
	std::vector<CBlock*> m_block;

	static CResult* m_pResult;
	// オブジェクト2Dの箱
	CObject3D* m_pObject[4];
};

#endif	// _RESULT_H_