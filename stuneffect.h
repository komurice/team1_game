//==================================================
// bullet.h
// Author: Buriya Kota
//==================================================
#ifndef _STUNEFFECT_H_
#define _STUNEFFECT_H_

//**************************************************
// インクルード
//**************************************************
#include "object3d.h"

//**************************************************
// 前方前言　実態はNG　ポインタだけならOK
//**************************************************

//**************************************************
// 構造体定義
//**************************************************

//**************************************************
// クラス
//**************************************************


class CStunEffect : public CObject3D
{
public:

	enum class BULLET_TYPE
	{
		DEFAULT,
		RANGE_EXTENSION,
		SHOTGUN,
		STUN_BULLET
	};

	explicit CStunEffect(int nPriority = PRIORITY_BULLET);
	~CStunEffect();

	HRESULT Init() override;
	void Update() override;

	static CStunEffect* Create(const D3DXVECTOR3 pos, const D3DXVECTOR3 size);



private:

private:

	float m_timer;
	bool m_up;
	int m_life;

};

#endif
