//==================================================
// stun_gauge.cpp
// Author: Buriya Kota
//==================================================

//**************************************************
// include
//**************************************************
#include "bgmodel.h"

#include "manager.h"
#include "objectX.h"
#include "utility.h"
//**************************************************
// マクロ定義
//**************************************************
#define STUN_GAUGE_POS_X			(50.0f)
#define STUN_GAUGE_POS_Y			(60.0f)

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CBgModel::CBgModel(int nPriority) : CObject(nPriority)
{

}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CBgModel::~CBgModel()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CBgModel::Init()
{
	m_Sin = 0;
	m_IsSine = false;
	m_IsEase = false;
	m_Move = { 0.0f,0.0f,0.0f };
	m_frame = 0.0f;
	m_Model = CObjectX::Create();
	m_Model->SetPos(D3DXVECTOR3(200.0f, 200.0f, 0.0f));
	m_magnification = 5.0f;
	return S_OK;
}

//--------------------------------------------------
// 終了
//--------------------------------------------------
void CBgModel::Uninit()
{
	CObject::DeletedObj();
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CBgModel::Update()
{
	if (m_IsSine)
	{
		moveSine();
	}
	if (m_IsEase)
	{
		moveEase();
	}
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CBgModel *CBgModel::Create(CModelData::MODEL_TYPE modelData,bool IsSine, bool IsEase, POPPOINT point)
{
	CBgModel *pBgModel;
	pBgModel = new CBgModel;

	if (pBgModel != nullptr)
	{
		pBgModel->Init();
		pBgModel->SetData(modelData);
		pBgModel->SetIsSine(IsSine);
		pBgModel->SetIsEase(IsEase);
		pBgModel->SetPoint(point);
	}
	else
	{
		assert(false);
	}

	return pBgModel;
}

void CBgModel::SetData(CModelData::MODEL_TYPE modelData)
{
	m_modelData = modelData;
	m_Model->SetModelData(m_modelData);
}

//--------------------------------------------------
// 座標などの設定
//--------------------------------------------------
void CBgModel::SetPoint(POPPOINT IsPoint)
{
	switch (IsPoint)
	{
	case CBgModel::LEFT:	//4(左からくるよー)
		m_Move.x = 1.0f;
		m_Model->SetPos(D3DXVECTOR3(-640.0f, 200.0f, 0.0f));

		break;
	case CBgModel::LIGHT:	//2(右からくるよー)
		m_Move.x = -3.0f;
		m_Model->SetPos(D3DXVECTOR3(700.0f, 200.0f, 0.0f));
		break;
	case CBgModel::UP:		//1(上からくるよー)
		m_Move.y = -1.0f;
		m_Model->SetPos(D3DXVECTOR3(0.0f, 720.0f, 0.0f));
		break;
	case CBgModel::DOWN:
		m_Move.y = 0.5f;	//3(下からくるよー)
		m_Model->SetPos(D3DXVECTOR3(0.0f, -720.0f, 0.0f));
		break;
	case CBgModel::MAX:
		break;
	default:
		break;
	}
}

void CBgModel::SetPos(D3DXVECTOR3 Pos)
{
	m_Model->SetPos(Pos);
}


//--------------------------------------------------
// サインカーブ
//--------------------------------------------------
void CBgModel::moveSine()
{
	m_Sin++;
	D3DXVECTOR3 Move = { m_Move.x*m_magnification,m_Move.y*m_magnification,0.0f };
	D3DXVECTOR3 Pos = m_Model->GetPos();

	if (m_Move.x != 0.0f)
	{
		Move.y = cosf((D3DX_PI*2.0f) * 0.01f * (m_Sin)) * 3;
	}
	if (m_Move.y != 0.0f)
	{
		Move.x = cosf((D3DX_PI*2.0f) * 0.01f * (m_Sin)) * 3;
	}
	
	m_Model->SetPos(Pos + Move);
}

//--------------------------------------------------
// いーじんぐ
//--------------------------------------------------
void CBgModel::moveEase()
{
	m_frame += 0.01f;
	float ease = 0.0f;
	ease = hmd::easeInSine(m_frame);
	D3DXVECTOR3 Move={0.0f,0.0f,0.0f};
	D3DXVECTOR3 Pos = m_Model->GetPos();

	if (m_Move.x != 0.0f)
	{
		Move.x = (m_Move.x*m_magnification)*ease;
	}
	if (m_Move.y != 0.0f)
	{
		Move.y = (m_Move.y*m_magnification)*ease;
	}
	
	m_Model->SetPos(Pos + Move);
}

