//==================================================
// item.h
// Author: Huang Qiyue
//==================================================
#ifndef _ITEM_H_
#define _ITEM_H_

//**************************************************
// インクルード
//**************************************************
#include "block.h"

//**************************************************
// クラス
//**************************************************
class CItem :public CBlock
{
public:
	enum ITEMTYPE
	{
		ITEMTYPE_RANGE,
		ITEMTYPE_SHOTGUN,
		ITEMTYPE_STUN,
		ITEMTYPE_METAL,
		ITEMTYPE_SONICSHOT,
	};

	explicit CItem(int nPriority = PRIORITY_OBJECT);
	~CItem();

	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw(DRAW_MODE drawMode) override;
	static CItem* Create(D3DXVECTOR3 pos, ITEMTYPE type);
	bool hitevent(CPlayer* player)override;

	void UpdateMovementState();

	bool getIsRemove() const { return m_isRemove; }

protected:
	void SetItemType(ITEMTYPE type) { m_itemType = type; }

	bool m_isRemove;           // 削除フラグ

	float m_rotationSpeed;     // 回転速度
	float m_amplitude;         // 振幅
	float m_frequency;         // 周波数
	float m_phaseOffset;       // 位相オフセット
	float m_elapsed;
	float m_fixedDelta;
	ITEMTYPE m_itemType;

private:

};

#endif	// _GOAL_H_#pragma once
